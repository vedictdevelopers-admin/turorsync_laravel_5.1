// Custom jScript
jQuery(document).ready(function(){
	//Admin Panel Drop down
	jQuery(".admin-greeting").click(function(){

		jQuery(".admin-panel-dropdown").stop().fadeToggle();
	});

	// Datepicker
	jQuery( "#sessionDate" ).datepicker();
});