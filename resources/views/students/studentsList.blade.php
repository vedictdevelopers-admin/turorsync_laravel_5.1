@extends('layouts.master')

@section('content')

    {{--pagination scripts--}}

    <link href="{{ URL::asset('assets/tbl/footable.core.css?v=2-0-1') }}" rel="stylesheet" type="text/css"/>
    <script>
        if (!window.jQuery) { document.write('<script src="js/jquery-1.9.1.min.js"><\/script>'); }
    </script>
    <script src="{{ URL::asset('assets/tbl/footable.js?v=2-0-1') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/tbl/footable.paginate.js?v=2-0-1') }} " type="text/javascript"></script>
    <script src="{{ URL::asset('assets/tbl/bootstrap-tab.js') }}" type="text/javascript"></script>

    {{--end pagination scripts--}}

    <section class="body-content container">
        <!-- Filter -->
        <section class="filter-holder">

            {!! Form::open(array('action' => 'StudentsController@filter_all_students','id' => 'filter_all_students','class' => 'form-inline','role'=>'form')) !!}

                <div class="form-group">
                    <div class="top-block">
                        {!! Form::label('studentName', 'Student Name', array('for' => 'studentName'))!!}
                        {!! Form::text('studentName', Input::get("studentName"), array('class' => 'form-control', 'id'=>'studentName','placeholder'=>''))!!}
                    </div>
                </div>
                <div class="form-group">
                <div class="top-block filter-location">
                    {!! Form::label('location', 'Location', array('for' => 'location'))!!}
                    {!! Form::text('location', Input::get("location"), array('class' => 'form-control', 'id'=>'location','placeholder'=>''))!!}
                    <span class="fa fa-map-marker"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="top-block filter-location">
                    {!! Form::label('Email', 'Email', array('for' => 'email'))!!}
                    {!! Form::text('email', Input::get("email"), array('class' => 'form-control', 'id'=>'email','placeholder'=>''))!!}

                    {!! Form::hidden('sort_method', Input::get("sort_method"), array('id'=>'sort_method')) !!}
                    {!! Form::hidden('sort_value', Input::get("sort_value"), array('id'=>'sort_value')) !!}

                </div>
            </div>
                <div class="form-group">
                    <div class="top-block">
                        {!! Form::button('<span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filter',array('type'=>'submit','class'=>'btn btn-default')) !!}
{{--
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filter</button>
--}}
                    </div>
                </div>

            {!! Form::close() !!}

            {!!Form::open(array('action' => 'StudentsController@to_excel','id' => 'to_excel_form', 'class' => 'form-inline','role'=>'form'))!!}
            <div class="bottom-block submit-btn">
                <button style="float: right;" class="btn btn-default" id="toExcel">to excel</button>
            </div>
            {!!Form::close()!!}
        </section>

        <!-- Tutor Table Container -->
        <div class="msg_type_success messages success_msg">
            <!--  <p>The Status has been updated.</p> -->
        </div>
        <div class="success_msg_append "> </div>
        <div class="tutor-table table-responsive">
            <table class="table table-condensed  table demo" data-page-size="10">
                <thead>
                <tr>
                    <th style="cursor:pointer" id="sort_name" value="sort_name" class="active-sort asc">Student Name <span class="sort-icon">&#9650;</span></th>
                    <th id="sort_contact_number" value="sort_contact_number">Contact Number</th>
                    <th style="cursor:pointer"  id="sort_email" value="sort_email">Email</th>
                    <th style="cursor:pointer" id="sort_location" value="sort_location">Location</th>
                    <th id="sort_sessions" value="sort_sessions">Total Sessions</th>
                    <th  style="cursor:pointer" id="sort_date" value="sort_date">Date Created</th>
                    <th id="sort_total_payment" value="sort_total_payment">Total Payments</th>
                    <th id="sort_status" value="sort_status">Status</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($bulk_data)) { ?>
                @foreach ($bulk_data as $student)
                    <tr>
                        <td><a href="{{ url('students/'. $student['student_id']) }}">{{ $student['first_name'] }} {{ $student['last_name'] }}</a></td>
                        <td> {{ $student['phone'] }}</td>
                        <td> {{ $student['email'] }}</td>
                        <td> {{ $student['street'] }} {{ $student['city'] }} {{ $student['state'] }} </td>
                        <td> {{ $student['sessions'] }}</td>

                        <td>{{ date_format(date_create($student['created_at']),"m-d-Y") }}</td>
                        <td>{{ $student['student_payment'] }}</td>
                        <td>
                            <select name="status" id="studentStatus_{{ $student['student_id'] }}" data-token="{{ csrf_token() }}"  class="form-control tutorStatus"  onchange="return studentStatus(this);">

                                <option value="0" @if($student['respond']==0) selected @endif>Not Responded</option>
                                <option value="1" @if($student['respond']==1) selected @endif>Responded</option>

                            </select>
                        </td>
                    </tr>
                @endforeach
                <?php } else { ?>
                    <tr><td colspan="7" style="text-align: center;">No result found</td></tr>
                <?php } ?>
                </tbody>


                    <tfoot>
                    <tr>
                        <td colspan="7">
                            {!! $paginations->render() !!}
                        </td>
                    </tr>
                    </tfoot>

            </table>
        </div>
    </section>

    <style type="text/css">

        .pagination li {
            display: inline-block!important;
            margin: 0px 10px;
        }


    </style>

    <script>
        function studentStatus(sel){

            var user_id = $(sel).attr('id').split('_');
            var status_value = $(sel).val();
            var _token = $(sel).data("token");

            var msg_text = "";
            if(status_value == 0) {
                msg_text = "Are you sure you want to make value as 'Not Responded' ?";
            } else if(status_value == 1) {
                msg_text = "Are you sure you want to make value as 'Responded' ?";
            }

            if(confirm(msg_text)) {
                $.post('students/update_student_status', { user_id : user_id[1],status_value:status_value, _token : _token }, function(data) {

                    $('#tbl_row_'+data['user_id']).fadeOut(1000);

                    if(data['status_value'] ==0){

                        $('.success_msg_append').html("<div class='msg_type_success_danger messages'><p>Updated as Not Responded</p></div>");
                        $('.success_msg_append').css('display','block');
                        $('.success_msg_append').fadeOut(3000);

                    }else  if(data['status_value'] ==1){

                        $('.success_msg_append').html("<div class='msg_type_success messages'><p>Updated as Responded</p></div>");
                        $('.success_msg_append').css('display','block');
                        $('.success_msg_append').fadeOut(3000);

                    }
                });
            } else {

                if(status_value == 0) {
                    $(sel).val('1');
                } else {
                    $(sel).val('0');
                }
                return false;
            }
        }



        $(document).ready(function(){
            $( 'th' ).on( 'click', function() {

                class_name = $( this ).attr('class');

             //
                if($(this).attr('id') == 'sort_contact_number' || $(this).attr('id') == 'sort_sessions' || $(this).attr('id') == 'sort_total_payment' || $(this).attr('id') == 'sort_status'  ){
                    return false;
                }

                if(typeof class_name === 'undefined'){

                    if ( $( '.table th' ).hasClass('active-sort') ) {
                        $('#sort_method').val('asc');
                        $("#sort_value").val($(this).attr('value'));
                        $('#filter_all_students').submit();
                        return false;
                    }

                } else {

                    if ( $( '.table th' ).hasClass('asc') ) {
                        $('#sort_method').val('desc');
                        $("#sort_value").val($(this).attr('value'));
                        $('#filter_all_students').submit();
                    } else if( $( '.table th' ).hasClass('desc') ){
                        $('#sort_method').val('asc');
                        $("#sort_value").val($(this).attr('value'));
                        $('#filter_all_students').submit();
                    }

                }
            });
        });

        if($('#sort_method').val() == 'asc'){

            sort_value = $('#sort_value').val()
            $( '.table th' ).removeAttr('class');
            $( '.table th span' ).remove();
            $('#'+sort_value).attr('class', 'active-sort asc');
            $('#'+sort_value).append(' <span class="sort-icon">&#9650;</span>')

        } else if($('#sort_method').val() == 'desc'){

            sort_value = $('#sort_value').val()
            $( '.table th' ).removeAttr('class');
            $( '.table th span' ).remove();
            $('#'+sort_value).attr('class', 'active-sort desc');
            $('#'+sort_value).append(' <span class="sort-icon">&#9660;</span>');

        }


    </script>


@stop
