@extends('layouts.master')

@section('title', 'All Sessions - TutorSync')

@section('content')

    <section class="body-content container">
        <!-- Panel Holder -->
        <div class="tutor-bio-card panel col-sm-4">
            <!-- Profile -->
            <div class="profile-card">
                <div class="inner-wrapper">
                    <!-- Profile -->
                    <!-- <img src="{{ URL::asset('assets/images/tutor-prof-img.png') }}" class="img-circle" alt="Nicolas Cage" width="90" height="90"> -->
                    <h3 class="title">{{ $students['details']->first_name .' '. $students['details']->last_name }}</h3>
                    <!-- Tutor Bio -->
                    <div class="tut-bio">



                        <ul>

                            @if(isset($students['details']->email))
                                <li>
                                    <p>  Email</p>
                                    <span class="number">{{ $students['details']->email }}</span>
                                </li>
                            @endif

                            <li>
                                <p>Contact Number</p>
                                <span class="number">{{ $students['details']->phone }}</span>
                            </li>
                            <li>
                                <p>Location</p>
								<span class="locatione">
									{{ $students['details']->street }} {{ $students['details']->city }}., <br>{{$students['details']->state .','. $students['details']->zip }}
								</span>
                            </li>
                        </ul>
                    </div>
                    <!-- Edit Profile -->
                    <a href="#" class="edit-prof">Edit Profile</a>
                </div>
            </div>
            <!-- Earning Info Panel -->

        </div>
        <!-- Tutor Acadimic Info Panel -->
        <div class="tutor-profile panel col-sm-8">
            <div class="inner-wrapper">

                <div class="success_msg_append"> </div>

                <!-- Panel Section -->
                <section class="edu-info main-sub-sections">
                    <div class="sub-inner-wrapper">
                        <div class="title">
                            <h3>Plans</h3>
                            <div class="section-icons">
                                <ul>
                                    {{--<li><i class="fa fa-plus"></i></li>--}}
                                    {{--<li><i class="fa fa-edit"></i></li>--}}
                                    <li>
                                        <a href="#" class="trash_student_by_id" id="" onclick="return deleteStudentById('{{csrf_token()}}')">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="sub-section">
                            {{--@foreach($students['package_details'] as $value)--}}

                            {{--<p class="certificate tick">--}}
                            {{--<i class="fa fa-check"></i>--}}
                            {{--10 hours   {{ $value->grade }}    {{ $value->subject_name }}--}}
                            {{--</p>--}}

                            {{--@endforeach--}}

                            <div class="plans table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Grade</th>
                                        <th>Subject</th>
                                        <th>Total Hours</th>
                                        <th>Remaining Hours</th>
                                        <th>Used Hours</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($students['package_details'] as $value)
                                        <tr>
                                            <td>{{ $value['grade'] }} </td>
                                            <td>{{ $value['subject_name'] }}</td>
                                            <td>{{ $value['total_hours'] }}  hours</td>
                                            <td>{{ ($value['total_hours'] -  $value['used_hours']) }}  hours</td>
                                            <td>{{ $value['used_hours'] }}  hours</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="sub-section">
                            <!-- Tutor Table Container -->
                            <div class="plans table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                    <tr>
                                        <th>Subject</th>
                                        <th>Grade</th>
                                        <th>Total Sessions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($students['grade'] as $value)
                                        <tr>
                                            <td>{{ $value->subject_name }}</td>
                                            <td>{{ $value->grade }}</td>
                                            <td>{{ $value->total_sessions }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

    <script type="text/javascript">

        function deleteStudentById(token){

            var student_id = '{{ Route::getCurrentRoute()->getParameter('id') }}';

            if(confirm("Are you sure want to delete this Student ?")) {
                $.post('delete_student_by_student_id', { student_id : student_id, '_token': token }, function(data) {
// alert(data);
                    $('.success_msg_append').html("<div class='msg_type_success messages'><p>"+ data + " </p></div>");
                    $('.success_msg_append').css('display','block');
                    $('.success_msg_append').fadeOut(3000);
                    location.reload();
                });
            } else {
                return false;
            }

        }

    </script>

@stop
