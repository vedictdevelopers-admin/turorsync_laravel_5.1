<html>
<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>TutorSync - @yield('title')</title>

    <script src="{{ URL::asset('http://code.jquery.com/jquery-1.10.2.js') }}"></script>

    <!-- Gogle Fonts -->
    <link href="{{ URL::asset('https://fonts.googleapis.com/css?family=Montserrat') }}" rel='stylesheet' type='text/css'>
    <link href="{{ URL::asset('https://fonts.googleapis.com/css?family=Nunito') }}" rel='stylesheet' type='text/css'>
    <!-- Font awesome -->
    <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/css/global-layout.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/student-profile.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/tutor-profile.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('assets/css/sessions-page.css') }}" />
    {{--<script type="text/javascript" src="{{ URL::asset('js/custom.js') }}"></script>--}}


</head>
<body>

{{--@section('sidebar')--}}
   {{----}}
{{--@show--}}

<!-- Header -->
<header>
    <!-- Inner content holder -->
    <section class="header-innerwholder container">
        <!-- Branding -->
        <section class="branding">
            <a href="#">
                <img src="{{ URL::asset('assets/images/logo.png') }}" alt="TutorSync">
            </a>
        </section>
        <!-- Admin switch/logout panel -->
        <section class="admin-panel">
            <div class="admin-greetings pull-left">
                <div class="admin-greeting">
                    <p class="pull-left">Welcome. <span class="user-type">Admin</span></p>
                    <span class="glyphicon glyphicon-menu-down pull-right" aria-hidden="true"></span>
                </div>
                <!-- Admin Panel -->
                <div class="admin-panel-dropdown">
                    <i class="fa fa-caret-up"></i>
                    <ul>
                        <li><a href="{{URL::to('tutors') }}" class="tutor-icon">Tutor Profile</a></li>
                        <li><a href="{{URL::to('students') }}" class="student-icon">Student Profile</a></li>
                        <li><a href="{{URL::to('sessions/all_sessions') }}" class="sessions-icon">Sessions</a></li>
                        <li><a href="" class="book-icon">Book a Tutor</a></li>
                    </ul>
                </div>
            </div>
            <div class="logout pull-right"><a href="#">logout</a></div>
        </section>
    </section>
</header>


<div class="container">
    @yield('content')
</div>


<!-- Footer -->
<footer>
    <!-- Inner Content Holder -->
    <div class="container">
        <div class="pivacy">
            <p>� 2015 TutorSync. All Rights Reserved</p>
        </div>
        <div class="menu-social-media">
            <div class="footer-menu">
                <ul class="list-inline">
                    <li><a href="#">Become a Tutor</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Privacy</a></li>
                    <li><a href="#">Terms</a></li>
                </ul>
            </div>
            <div class="social-media">
                <ul class="list-inline">
                    <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#" class="Twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#" class="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/libs/site-scripts.js') }}"></script>

</body>
</html>