@extends('layouts.master')

@section('title', 'All Tutors')

@section('content')



    <!-- jQuery is used only for this example; it isn't required to use Stripe -->




    <link href="{{ URL::asset('assets/css/assign-new-tutor.css') }}" rel="stylesheet" type="text/css"/>
<?php
$profile_picture = "";
if($booking_success_data['profile_pic'] == '') {
    if($booking_success_data['tutor_gender'] == 'Male') {
        $profile_picture = URL::asset('assets/images/tutor-male.png');
    } else {
        $profile_picture = URL::asset('assets/images/tutor-female.png');
    }
} else {
    $profile_picture = $booking_success_data['profile_pic'];
}
?>

    <!-- Body Content -->
<section class="body-content container">
    <!-- Content Holder -->
    <div class="tutor-pick-success inner-wrapper col-sm-10">
        <img class="success-mark" alt="" src="{{ URL::asset('assets/images/step5.png') }}" />
        <h1 class="title">Success</h1>
        <div class="prof-cards-holder col-sm-6">
            <!-- Tutor Profile Card -->
            <div class="tutor profile">
                <div class="profile-bio">
                    <div class="profile-img-holder">
                        <img src="<?php echo  $profile_picture; ?>" alt="Tutor Profile Image" class="img-circle img-responsive">
                    </div>
                    <div class="profile-bio-info">
                        <h3><?php echo $booking_success_data['tutor_name']; ?></h3>

                        <p><i class="fa fa-book fa-fw"></i><?php echo $booking_success_data['subject_name']; ?></p>
                        <p><i class="fa fa-mars fa-fw"></i><?php echo $booking_success_data['tutor_gender']; ?></p>
                        <p><i class="fa fa-user fa-fw"></i><?php echo $booking_success_data['tutor_type']; ?> Tutor</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sessions controlers -->
        <div class="session-btns">
            <a href="{{ url('tutors/clear_booking_sessions') }}" class="reschedule-btn btn btn-default">Back to Home</a>
        </div>
    </div>
</section>










@stop