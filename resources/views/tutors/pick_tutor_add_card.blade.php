@extends('layouts.master')

@section('title', 'All Tutors')

@section('content')


    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

    <!-- jQuery is used only for this example; it isn't required to use Stripe -->


    <script type="text/javascript">
        // this identifies your website in the createToken call below
        Stripe.setPublishableKey('pk_test_4PE6mMMjONnpRCVX429l3pnB');

        function stripeResponseHandler(status, response) {
            if (response.error) {
                // re-enable the submit button
                $('.submit-button').removeAttr("disabled");
                // show the errors on the form
                $(".payment-errors").html(response.error.message);
            } else {
                var form$ = $("#payment-form");
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                // and submit
                form$.get(0).submit();
            }
        }
        $(document).ready(function() {
            $("#payment-form").submit(function(event) {
                // disable the submit button to prevent repeated clicks
                $('.submit-button').attr("disabled", "disabled");
                // createToken returns immediately - the supplied callback submits the form if there are no errors
                Stripe.createToken({
                    number: $('.card-number').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeResponseHandler);
                return false; // submit from callback
            });
        });
    </script>

    <link href="{{ URL::asset('assets/css/assign-new-tutor.css') }}" rel="stylesheet" type="text/css"/>


        <!-- Body Content -->
<section class="body-content container">
    <div class="inner-wrapper">
    <!-- Content Holder -->
    <!-- Panel Section -->
    <div class="pop-up-holder col-sm-12">
        <section class="payment-info main-sub-sections inner-wrapper">
            <div class="sub-inner-wrapper">
                <div class="title">
                    <h3>Add Card</h3>
                    <div class="section-icons">
                        <i class="fa fa-times"></i>
                    </div>
                </div>
            </div>
        </section>
        <section class="new-card-details main-sub-sections inner-wrapper">

            {!!Form::open(array('action' => 'TutorsController@pick_tutor_add_card_details','id' => 'payment-form','role'=>'form'))!!}
            {!! Form::hidden('tutor_id',$tutor['id'] ) !!}
            <div class="form-group col-sm-12">
                {{--<div class=" alert alert-danger   payment-errors">--}}
                    <span class="payment-errors"></span>
                {{--</div>--}}


            </div>

            <div class="clearfix"></div>

            <div class="visa-card-id">
                <div class="card-num form-group col-sm-6">
                    <div class="card">
                        <p class="card-id">Credit Card Number</p>
                        {!! Form::text('card-id','', array('class' => 'form-control card-number', 'id'=>'card-id','autocomplete'=>'off','placeholder'=>'4532 3134 0355 8329'))!!}
                    </div>
                </div>
                <div class="allowed-cards col-sm-6">
                    <img alt="Allowed Cards" src="{{ URL::asset('assets/images/student_card_new.png') }}">
                </div>
            </div>

            <div class="card-details">
                <div class="card-exp-date col-sm-6">
                    <p class="expire-date">Expiry Date</p>
                    <div class="dates-holder">
                        <div class="exp-dates-holder form-group">
                            {!!  Form::select('card-expiry-month',array('11' => '11', '12' => '12'),'', ['class' => 'card-expiry-month','id'=>'card-expiry-month']) !!}
                        </div>

                        <div class="exp-dates-holder form-group">
                            {!!  Form::select('card-expiry-year',array('15' => '2015', '16' => '2016'),'', ['class' => 'card-expiry-year','id'=>'card-expiry-year']) !!}
                        </div>
                    </div>
                </div>
                <div class="card-secret-code col-sm-6">
                    <p class="cvv-text">CVV</p>
                    <div class="cvv-num-holder">
                        <div class="cvv-num col-sm-6">
                            {{--<input type="text" autocomplete="off" maxlength="3" placeholder="" class="form-control" name="cardCvc">--}}
                            {!! Form::text('card-cvc','', array('class' => 'card-cvc form-control','size'=>4 ,'id'=>'card-cvc','autocomplete'=>'off'))!!}

                        </div>
							<span class="helper-note">
								The last 3 digits displayed on the back of your card
							</span>
                    </div>
                </div>
            </div>

            <div class="payment-controllers new-card">
                <div class="session-btns payment-controllers-holder">
                    <a class="cancel-btn btn btn-default" href="#">Cancel</a>
                    {{--<a class="save-btn btn btn-default" href="#">Add Card</a>--}}
                    {!! Form::button('Add Card',array('type'=>'submit','class'=>'submit-button save-btn btn btn-default'))!!}
                </div>
            </div>

            {!!Form::close()!!}

            <div class="clearfix"></div>
        </section>








    </div>
</div>
</section>










@stop