@extends('layouts.master')

@section('title', 'All Tutors')

@section('content')







    <!-- Body Content -->
    <section class="body-content container">
        <!-- Content Holder -->
        <div class="student-data-filter">
            <form class="form-inline student-email-filter inner-wrapper" role="form">
                <div class="form-group">
                    <label for="studentEmail">Student Email</label>
                    <input type="email" class="form-control" id="studentEmail" placeholder="Enter Email">
                </div>
            </form>
            <div class="summery-holder inner-wrapper">
                <div class="summery-inner-wrapper col-sm-10">
                    <div class="label-wrappe">
                        <p>Summery</p>
                    </div>
                    <div class="summmry-info col-sm-10">
                        <div class="col-sm-4">
                            <p class="summery-title">Student Name</p>
                            <p class="summery-data">Alex Dilango</p>
                        </div>
                        <div class="col-sm-4">
                            <p class="summery-title">Phone Number</p>
                            <p class="summery-data">(123) 456 6789</p>
                        </div>
                        <div class="col-sm-4">
                            <p class="summery-title">Location</p>
                            <p class="summery-data address-line">4435 Armbrester Drive</p>
                            <p class="summery-data address-line">Burbank, CA 95662</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-wrapper">
            <div class="filter-form pick-tutor col-sm-10">
                <form class="form-inline" role="form">
                    <div class="gender-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <p>Gender Preference of Tutor</p>
                        </div>
                        <div class="gender-options col-sm-10">
                            <div class="col-lg-4 col-md-4 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_gender st_selected_type">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_3_1.png') }}">
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_3_1-1_.png') }}">
                                            Any
                                        </h6>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <!-- student_step_common_wrapper -->
                            <div class="col-lg-4 col-md-4 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_gender">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_3_2.png') }}">
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_3_2-1.png') }}">
                                            male
                                        </h6>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <!-- student_step_common_wrapper -->
                            <div class="col-lg-4 col-md-4 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_gender">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_3_3.png') }}">
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/images/student_step_3_3-1.png') }}">
                                            Female
                                        </h6>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_type st_selected_type">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_5-1.png') }}">
                                            <img class="st_step_3_hiden_icon" src={{ URL::asset('assets/images/student_step_5-2.png') }}">
                                            PRO
                                        </h6>
                                        <div class="row row_clr type_of_tutor_step_3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h1 class="ng-binding">$25</h1>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h3>PER<br>
                                                    HOUR
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <!-- student_step_common_wrapper -->
                            <div class="col-lg-6 col-md-6 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_type">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="images/student_step_4-1.png">
                                            <img class="st_step_3_hiden_icon" src="images/student_step_4-2.png">
                                            Peer
                                        </h6>
                                        <div class="row row_clr type_of_tutor_step_3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h1 class="ng-binding">$15</h1>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h3>PER<br>
                                                    HOUR
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="calendar">
                            <label for="sessionDate">Session Date</label>
                            <input type="text" class="form-control" id="sessionDate" placeholder="July 20,2015">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                    <div class="form-group select-tutor-time">
                        <label for="time">Time</label>
                        <div class="time-holder">
                            <div class="select-time start-time">
                                <input type="text" class="form-control small-field" id="time" placeholder="8.00">
                                <select class="form-control small-field" id="beginTime">
                                    <option>AM</option>
                                    <option>PM</option>
                                </select>
                            </div>
                            <div class="select-time end-time">
                                <input type="text" class="form-control small-field" id="time" placeholder="9.00">
                                <select class="form-control small-field" id="beginTime">
                                    <option>AM</option>
                                    <option>PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tutorName">Tutor Name</label>
                        <input type="text" class="form-control" id="tutorName" placeholder="John Doe">
                    </div>
                    <div class="form-group submit-holder">
                        <div class="form-field">
                            <button type="submit" class="btn btn-default">Find a Tutor</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Tutor Results -->
            <div class="tutor-result-holder col-sm-12">
                <div class="row-result">
                    <div class="tutor-bio col-sm-9">
                        <div class="profile-img-holder">
                            <img class="img-circle" alt="Tutor Profile Image" src="images/tutor-prof-img.png">
                            <img class="tutor-status" src="images/tutor-available.png" alt="tutor is available">
                        </div>
                        <div class="profile-bio-info col-sm-10">
                            <h3>Mark Nicol</h3>
                            <div class="contact-block col-sm-4">
                                <p><i class="fa fa-phone fa-fw"></i> (123) 456 0789</p>
                                <p><i class="fa fa-map-marker fa-fw"></i> Hothrone Lane, New York, NY 100203</p>
                            </div>
                            <div class="education-block col-sm-6">
                                <p><i class="fa fa-graduation-cap fa-fw"></i> B.S. in Applied Mathematics</p>
                            </div>
                            <div class="tutor-type-block">
                                <p><i class="fa fa-user fa-fw"></i> PRO Tutor</p>
                            </div>
                        </div>
                    </div>
                    <div class="tutor-rating col-sm-3">
                        <div class="star-rating tutor-rating">
                            <span class="fa fa-star fa-star-active" data-rating="1"></span>
                            <span class="fa fa-star fa-star-active" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                        </div>
                        <p class="review-stats"><span class="num-of-students">13</span> Reviewes</p>
                        <a href="#" class="btn btn-default assign">Assign this Tutor</a>
                    </div>
                </div>
                <div class="row-result">
                    <div class="tutor-bio col-sm-9">
                        <div class="profile-img-holder">
                            <img class="img-circle" alt="Tutor Profile Image" src="images/tutor-prof-img.png">
                            <img class="tutor-status" src="images/tutor-scheduled.png" alt="tutor is available">
                        </div>
                        <div class="profile-bio-info col-sm-10">
                            <h3>Mark Nicol</h3>
                            <div class="contact-block col-sm-4">
                                <p><i class="fa fa-phone fa-fw"></i> (123) 456 0789</p>
                                <p><i class="fa fa-map-marker fa-fw"></i> Hothrone Lane, New York, NY 100203</p>
                            </div>
                            <div class="education-block col-sm-6">
                                <p><i class="fa fa-graduation-cap fa-fw"></i> B.S. in Applied Mathematics</p>
                            </div>
                            <div class="tutor-type-block">
                                <p><i class="fa fa-user fa-fw"></i> PRO Tutor</p>
                            </div>
                        </div>
                    </div>
                    <div class="tutor-rating col-sm-3">
                        <div class="star-rating tutor-rating">
                            <span class="fa fa-star fa-star-active" data-rating="1"></span>
                            <span class="fa fa-star fa-star-active" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                        </div>
                        <p class="review-stats"><span class="num-of-students">13</span> Reviewes</p>
                        <a href="#" class="btn btn-default assign">Assign this Tutor</a>
                    </div>
                </div>
                <div class="row-result">
                    <div class="tutor-bio col-sm-9">
                        <div class="profile-img-holder">
                            <img class="img-circle" alt="Tutor Profile Image" src="images/tutor-prof-img.png">
                            <img class="tutor-status" src="images/tutor-available.png" alt="tutor is available">
                        </div>
                        <div class="profile-bio-info col-sm-10">
                            <h3>Mark Nicol</h3>
                            <div class="contact-block col-sm-4">
                                <p><i class="fa fa-phone fa-fw"></i> (123) 456 0789</p>
                                <p><i class="fa fa-map-marker fa-fw"></i> Hothrone Lane, New York, NY 100203</p>
                            </div>
                            <div class="education-block col-sm-6">
                                <p><i class="fa fa-graduation-cap fa-fw"></i> B.S. in Applied Mathematics</p>
                            </div>
                            <div class="tutor-type-block">
                                <p><i class="fa fa-user fa-fw"></i> PRO Tutor</p>
                            </div>
                        </div>
                    </div>
                    <div class="tutor-rating col-sm-3">
                        <div class="star-rating tutor-rating">
                            <span class="fa fa-star fa-star-active" data-rating="1"></span>
                            <span class="fa fa-star fa-star-active" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                        </div>
                        <p class="review-stats"><span class="num-of-students">13</span> Reviewes</p>
                        <a href="#" class="btn btn-default assign">Assign this Tutor</a>
                    </div>
                </div>
                <div class="row-result">
                    <div class="tutor-bio col-sm-9">
                        <div class="profile-img-holder">
                            <img class="img-circle" alt="Tutor Profile Image" src="images/tutor-prof-img.png">
                            <img class="tutor-status" src="images/tutor-not-available.png" alt="tutor is available">
                        </div>
                        <div class="profile-bio-info col-sm-10">
                            <h3>Mark Nicol</h3>
                            <div class="contact-block col-sm-4">
                                <p><i class="fa fa-phone fa-fw"></i> (123) 456 0789</p>
                                <p><i class="fa fa-map-marker fa-fw"></i> Hothrone Lane, New York, NY 100203</p>
                            </div>
                            <div class="education-block col-sm-6">
                                <p><i class="fa fa-graduation-cap fa-fw"></i> B.S. in Applied Mathematics</p>
                            </div>
                            <div class="tutor-type-block">
                                <p><i class="fa fa-user fa-fw"></i> PRO Tutor</p>
                            </div>
                        </div>
                    </div>
                    <div class="tutor-rating col-sm-3">
                        <div class="star-rating tutor-rating">
                            <span class="fa fa-star fa-star-active" data-rating="1"></span>
                            <span class="fa fa-star fa-star-active" data-rating="2"></span>
                            <span class="fa fa-star" data-rating="3"></span>
                            <span class="fa fa-star" data-rating="4"></span>
                            <span class="fa fa-star" data-rating="5"></span>
                        </div>
                        <p class="review-stats"><span class="num-of-students">13</span> Reviewes</p>
                        <a href="#" class="btn btn-default assign">Assign this Tutor</a>
                    </div>
                </div>
            </div>
        </div>

    </section>




<style>


    /* Body Content */
    .body-content .inner-wrapper{
        overflow: hidden;
        padding: 40px 25px;
        border: 1px solid #ebebeb;
        border-radius: 5px;
        background: #fff;
    }

    /* Form */
    .form-inline .form-group {
        margin: 0 50px 26px 0;
        width: 380px;
    }

    .form-inline .form-control {
        height: 44px;
        margin-left: 0;
        padding: 12px 0 12px 22px;
        text-transform: uppercase;
        width: 230px;
    }

    .form-group .filter-location span.fa-map-marker,
    .form-group .calendar span.fa-calendar{
        top: 14px;
    }

    .form-group label{
        color: #a4a4a4;
        font-weight: 300;
        margin-top: 8px;
    }

    .form-group button[type="submit"] {
        height: 45px;
        width: 232px;
    }

    .tags-holder{
        float: left;
        margin: 0 0 16px 150px;
    }

    .tags-holder .tag{
        color: #d6d6d7;
        display: inline-block;
        margin: 0 10px 10px 0;
        padding: 10px 12px;
        background: #f3f3f3;
        border-radius: 5px;
    }

    .tags-holder .active.tag{
        color: #fff;
        background: #126889;
    }

    .time-holder{
        float: right;
    }

    .select-time {
        display: inline;
        overflow: hidden;
    }

    .form-control.small-field {
        float: none;
        padding-left: 3px;
        width: 53px;
    }

    .form-control.small-field[type="text"] {
        width: 40px;
    }

    .select-time.start-time::after {
        color: #dedede;
        content: "To";
        margin: 0 5px;
    }

    /* Results */
    .row-result{
        border-top: 1px solid #e2e2e2;
        overflow: hidden;
        padding: 30px 0;
        width: 100%;
        color: #828282;
    }

    .tutor-bio {
        overflow: hidden;
    }

    .tutor-result-holder .profile-img-holder{
        float: left;
    }

    .tutor-bio img {
        display: block;
    }

    .tutor-bio .tutor-status {
        height: auto;
        margin: 10px auto 0;
        width: 80px;
    }

    .profile-bio-info {
        float: left;
        font-size: 0.9em;
        margin: 0 0 0 12px;
        padding: 0;
    }

    .profile-bio-info h3{
        margin: 0 0 12px 15px;
        font-size: 1.2em;
    }

    .profile-bio-info p {
        margin: 0 0 5px;
    }

    .profile-bio-info p .fa {
        margin: 0 10px 0 0;
        color: #f46722;
    }

    .tutor-rating {
        text-align: center;
    }

    .tutor-rating .star-rating {
        margin: 0 0 20px;
    }

    .tutor-rating .review-stats {
        color: #dfe3e5;
    }

    .tutor-rating .assign {
        background: #f46722;
        border: none;
        color: #fff;
        height: 45px;
        text-transform: uppercase;
        width: 230px;
        padding: 12px 0;
        margin: 20px 0 0;
    }

    /* Admin select Tutor */
    .student-data-filter{
        overflow: hidden;
    }

    .student-data-filter .inner-wrapper{
        border-radius: 0;
    }

    .student-data-filter .form-group,
    .pick-tutor .form-group {
        width: 392px;
    }

    .student-data-filter .summery-holder.inner-wrapper{
        background: #fbfbfb;
        padding-left: 12px;
    }

    .summery-holder .label-wrappe{
        color: #222222;
        float: left;
    }

    .summery-holder .summmry-info{
        float: right;
    }

    .summmry-info .summery-title {
        color: #a4a4a4;
        font-size: 0.85em;
    }

    .summmry-info .summery-data {
        font-size: 0.9em;
        margin: 0;
    }

    .filter-form.col-sm-10 {
        padding-left: 0;
    }

    .student-data-filter .form-group{
        margin-bottom: 0;
    }

    .gender-option-holder.col-sm-12 {
        padding-left: 0;
    }

    .gender-select-title.col-sm-2{
        padding: 0;
    }

    .gender-select-title p{
        color: #a4a4a4;
        font-weight: 300;
        margin-top: 8px;
    }

    .gender-options.col-sm-10 {
        padding-left: 0;
    }

    .gender-options .student_step_common_wrapper{
        margin: 0 0 25px;
    }

    .pick-tutor .time-holder{
        display: inline-block;
        float: none;
    }

    .pick-tutor .select-tutor-time label{
        margin-right: 12px;
    }

    .pick-tutor .small-field[type="text"] {
        padding-left: 5px;
        width: 48px;
    }

    .pick-tutor .submit-holder{
        margin-right: 0;
        width: 100%;
    }

    .pick-tutor .submit-holder button[type="submit"] {
        margin-left: 160px;
        float: none;
    }

    /* Media Querry */
    /* Mobile */
    @media screen and (max-width: 767px) {
        .filter-form {
            text-align: center;
        }

        .form-inline .form-group {
            margin: 0 0 26px 0;
            width: auto;
        }

        .form-inline .form-control {
            width: 100%;
            padding-left: 0;
        }

        .tags-holder {
            float: none;
            margin: 0 0 16px;
        }

        .form-group .filter-location span.fa-map-marker,
        .form-group .calendar span.fa-calendar {
            top: auto;
            bottom: 14px;
        }

        .time-holder {
            float: none;
        }

        .form-control.small-field[type="text"] {
            width: 100%;
        }

        .select-time.start-time::after {
            margin: 12px 0;
            display: block;
        }

        .form-group.submit-holder {
            overflow: hidden;
        }

        .form-group button[type="submit"] {
            width: 100%;
        }

        .row-result {
            text-align: center;
        }

        .tutor-bio img {
            float: none;
        }

        .profile-bio-info {
            margin: 12px 0 0;
        }

        .tutor-rating {
            margin: 25px 0 0;
        }

        .tutor-rating .assign {
            width: 100%;
        }

        .tutor-result-holder .profile-img-holder {
            float: none;
        }

        .profile-img-holder .img-circle{
            margin: 0 auto;
            width: 90px;
        }

        .profile-bio-info h3 {
            margin-right: 0;
        }

    }
</style>




@stop