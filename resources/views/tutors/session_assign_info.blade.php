
@extends('layouts.master')

@section('title', 'Session Information')

@section('content')


    <link href="{{ URL::asset('assets/css/assign-new-tutor.css') }}" rel="stylesheet" type="text/css"
          xmlns="http://www.w3.org/1999/html"/>


    <!-- Body Content -->
    <section class="body-content container">
        <div class="inner-wrapper">
            <!-- Content Holder -->
            <!-- Panel Section -->
            <div class="pop-up-holder col-sm-12">
                <section class="payment-info main-sub-sections inner-wrapper">
                    <div class="sub-inner-wrapper">
                        <div class="title">
                            <h3>Session Information</h3>
                            <div class="section-icons">
                                <i class="fa fa-times"></i>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="payment-summery main-sub-sections summery-holder inner-wrapper">
                    <div class="col-sm-3">
                        <div class="label-wrappe">
                            <p>Summary</p>
                        </div>
                    </div>
                    <div class="col-sm-4 summmry-info">
                        <div class="std-name block-raw">
                            <p class="summery-title">Student Name</p>
                            <p class="summery-data"><?php   echo $session_assign_info["session_assign_info_student_first_name"]." ".$session_assign_info["session_assign_info_student_last_name"];  ?></p>
                        </div>
                        <div class="session-date block-raw">
                            <p class="summery-title">Session Date</p>
                            <p class="summery-data"><?php   echo $session_assign_info["session_assign_info_session_date"]; ?></p>
                        </div>
                        <div class="location block-raw">
                            <p class="summery-title">Location</p>
                            <p class="summery-data"><?php echo $session_assign_info["session_assign_info_student_location"];  ?></p>
                        </div>
                        <div class="tut-type block-raw">
                            <p class="summery-title">Tutor Name</p>
                            <p class="summery-data"><?php  echo $session_assign_info["session_assign_info_tutor_first_name"]." ".$session_assign_info["session_assign_info_tutor_last_name"]; ?>  </p>
                        </div>

                    </div>
                    <div class="col-sm-4 summmry-info">
                        <div class="tut-name block-raw">
                            <p class="summery-title">Category</p>
                            <p class="summery-data"><?php echo $session_assign_info["session_assign_info_student_subject_name"]."(".$session_assign_info["session_assign_info_student_grade"].")";  ?></p>
                        </div>
                        <div class="session-time block-raw">
                            <p class="summery-title">Session Time</p>
                            <p class="summery-data"><?php echo  $session_assign_info["session_assign_info_session_start_time"].' - '.$session_assign_info['session_assign_info_session_end_time']; ?></p>
                        </div>
                        <div class="tut-type block-raw">
                            <p class="summery-title">Tutor Type</p>
                            <p class="summery-data"><?php  echo $session_assign_info["session_assign_info_tutor_type"] ?>  </p>
                        </div>

                    </div>
                </section>

                <section class="payment-details main-sub-sections inner-wrapper">


                    <div class="session-btns payment-controllers-holder">
                        <a href="{{ url('tutors/clear_booking_sessions') }}" class="cancel-btn btn btn-default">Cancel</a>

                        {!!Form::open(array('action' => 'TutorsController@tutor_booking_update','id' => 'tutor_booking_update', 'class' => 'form-inline','role'=>'form'))!!}

                        {!! Form::hidden('session_assign_info_session_id', $session_assign_info["session_assign_info_session_id"], array('id'=>'session_assign_info_session_id')) !!}
                        {!! Form::hidden('session_assign_info_session_date', $session_assign_info["session_assign_info_passed_session_date"], array('id'=>'session_assign_info_session_date')) !!}
                        {!! Form::hidden('session_assign_info_session_duration', $session_assign_info["session_assign_info_session_duration"], array('id'=>'session_assign_info_session_duration')) !!}
                        {!! Form::hidden('session_assign_info_begin_time', $session_assign_info["session_assign_info_begin_time"], array('id'=>'session_assign_info_begin_time')) !!}
                        {!! Form::hidden('session_assign_info_tutor_id', $session_assign_info["session_assign_info_tutor_id"], array('id'=>'session_assign_info_tutor_id')) !!}

                        {!! Form::button('Book Appointment',array('type'=>'submit','class'=>'save-btn btn btn-default')) !!}

                        {!!Form::close()!!}

                       <!--<a href="#" class="save-btn btn btn-default" onclick="return tutor_booking_cofirm(<?php // echo $session_assign_info["session_assign_info_session_tutor_id"] ?>); ">Book Appointment</a>--!>

                    </div>

                </section>


            </div>
        </div>
    </section>



    <script>


        function tutor_booking_cofirm(tutor_id, is_available) {

            if (tutor_id == '' || is_available == '0' || $('input[name="session_date"]').val() == '' || $('input[name="session_duration"]').val() == '' || $('input[name="begin_time"]').val() == '') {
                alert("Please select date, duration and start time");
                return false;
            } else {

                var session_id = $('input[name="session_id"]').val();
                var session_date = $('input[name="session_date"]').val();
                var session_duration = $('input[name="session_duration"]').val();
                var begin_time = $('input[name="begin_time"]').val();
                var token =  $("input[name=_token]").val();
                var tutor_id =  tutor_id;


                $.ajax({
                    url: "{{ url('tutors/tutor_booking_update') }}",
                    type: "post",
//                    dataType: "json",
                    data: {
                        'session_id': session_id,
                        'session_date':session_date,
                        'session_duration':session_duration,
                        'begin_time': begin_time,
                        'tutor_id': tutor_id,
                        '_token': token
                    },
                    success: function (response) {
                        console.log(response);

                        var redirect_url = '{{ url('sessions/all_sessions') }}';
                        $(location).attr('href',redirect_url)


                    }
                });

            }

        }


    </script>









@stop
