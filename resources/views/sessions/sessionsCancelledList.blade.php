@extends('layouts.master')

@section('title', 'Session Cancelled List')

@section('content')

    {{--pagination scripts--}}

    <link href="{{ URL::asset('assets/tbl/footable.core.css?v=2-0-1') }}" rel="stylesheet" type="text/css"/>

    <script src="{{ URL::asset('assets/tbl/footable.js?v=2-0-1') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/tbl/footable.paginate.js?v=2-0-1') }} " type="text/javascript"></script>
    <script src="{{ URL::asset('assets/tbl/bootstrap-tab.js') }}" type="text/javascript"></script>

    {{--end pagination scripts--}}


    <section class="body-content container">
            <!-- Tutor tabs -->
            <section class="tutor-tabs">
                <ul class="nav nav-pills nav-justified">
                    <li class="{{ Route::currentRouteName() == 'allSessions'||Route::currentRouteName() == 'allSessionsFilter'?'active':''}}"><a href="{{ URL::route('allSessions') }}">All Sessions</a></li>
                <li class="{{ Route::currentRouteName() == 'scheduledSessions'||Route::currentRouteName() == 'scheduledSessionsFilter'?'active':''}}"><a href="{{ URL::route('scheduledSessions') }}">Scheduled Sessions</a></li>
                <li class="{{ Route::currentRouteName() == 'completedSessions'||Route::currentRouteName() == 'completedSessionsFilter'?'active':''}}"><a href="{{ URL::route('completedSessions') }}">Completed Sessions</a></li>
                <li class="{{ Route::currentRouteName() == 'cancelledSessions'||Route::currentRouteName() == 'cancelledSessionsFilter'?'active':''}}"><a href="{{ URL::route('cancelledSessions') }}">Cancelled Sessions</a></li>
                <li class="{{ Route::currentRouteName() == 'declinedSessions'||Route::currentRouteName() == 'declinedSessionsFilter'?'active':''}}"><a href="{{ URL::route('declinedSessions') }}">Declined Sessions</a></li>
                    <li class="{{ Route::currentRouteName() == 'abandonedSessions'||Route::currentRouteName() == 'abandonedSessionsFilter'?'active':''}}"><a href="{{ URL::route('abandonedSessions') }}">Abandoned Sessions</a></li>
            </ul>
        </section>
        <!-- Filter -->
        <section class="filter-holder">

            {!! Form::open(array('action' => 'SessionsController@filter_Cancelled_sessions', 'class' => 'form-inline','role'=>'form')) !!}
            <div class="form-group">
                <div class="top-block">
                    {!! Form::label('tutorName', 'Tutor Name', array('for' => 'tutorName'))!!}
                    {!! Form::text('tutorName', Input::get("tutorName"), array('class' => 'form-control', 'id'=>'tutorName','placeholder'=>''))!!}
                </div>
                <div class="bottom-block filter-location">
                    {!! Form::label('location', 'Location', array('for' => 'location'))!!}
                    {!! Form::text('location', Input::get("location"), array('class' => 'form-control', 'id'=>'location','placeholder'=>''))!!}
                    <span class="fa fa-map-marker"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="top-block">
                    {!! Form::label('studentName', 'Student name', array('for' => 'studentName'))!!}
                    {!! Form::text('studentName', Input::get("studentName"), array('class' => 'form-control', 'id'=>'studentName','placeholder'=>''))!!}
                </div>
                <div class="form-group">
                    <div class="bottom-block">
                        {!! Form::button('<span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filter',array('type'=>'submit','class'=>'btn btn-default')) !!}
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="top-block calendar">
                    {!! Form::label('sessionDate', 'Session Date', array('for' => 'sessionDate'))!!}
                    {!! Form::text('sessionDate', Input::get("sessionDate"), array('class' => 'form-control', 'id'=>'sessionDate'))!!}
                    <span class="fa fa-calendar"></span>
                </div>
            </div>

            {!! Form::close() !!}

            <form class="form-inline" role="form">

            </form>
            {!!Form::open(array('action' => 'SessionsController@to_excel','id' => 'to_excel_form', 'class' => 'form-inline','role'=>'form'))!!}
            <div class="bottom-block submit-btn">
                {!! Form::hidden('tutorNameEx', isset($_POST['tutorName'])?$_POST['tutorName']:'' , array('id'=>'tutorNameEx')) !!}
                {!! Form::hidden('studentNameEx', isset($_POST['studentName'])?$_POST['studentName']:'' , array('id'=>'studentNameEx')) !!}
                {!! Form::hidden('sessionDateEx', isset($_POST['sessionDate'])?$_POST['sessionDate']:'' , array('id'=>'sessionDateEx')) !!}
                {!! Form::hidden('locationEx', isset($_POST['location'])?$_POST['location']:'' , array('id'=>'locationEx')) !!}
                {!! Form::hidden('statusEx', '3' , array('id'=>'statusEx')) !!}
                <button style="float: right;" class="btn btn-default" id="toExcel">to excel</button>
            </div>
            {!!Form::close()!!}
        </section>
        <!-- Tutor Table Container -->
        <div class="tutor-table table-responsive">

            <table class="table table-condensed table demo" data-page-size="10">
                <thead>
                <tr>
                    <th>Tutor Name</th>
                    <th>Student Name</th>
                    <th>Subject</th>
                    <th>Date</th>
                    <th>Duration</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Location</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($all_session_data)) { ?>
                @foreach($all_session_data as $session)
                <tr>
                    <td><a href="{{ url('sessions/cancelled_sessions/'. $session['session_id']) }}">{{ $session['tutor_name'] }}</a></td>
                    <td>{{ $session['student_name'] }}</td>
                    <td>{{ $session['subject'] }}</td>
                    <td>{{ $session['date'] }}</td>
                    <td>{{ $session['duration'] }}</td>
                    <td>{{ $session['time'] }}</td>
                    <td>{{ $session['status'] }}</td>
                    <td>{{ $session['location'] }}</td>
                </tr>
                @endforeach
                <?php } else { ?>
                    <tr><td colspan="8" style="text-align: center;">No result found</td></tr>
                <?php } ?>
                {{--<tr>--}}
                    {{--<td colspan="8">--}}
                        {{--<button class="btn form-controler-btn pull-left">Previous</button>--}}
                        {{--<button class="btn form-controler-btn pull-right">Next</button>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                </tbody>



                    <tfoot>
                    <tr>
                        <td colspan="7">
                            {!! $paginations->render() !!}
                        </td>
                    </tr>
                    </tfoot>





            </table>
        </div>
    </section>





        <style type="text/css">

            .pagination li {
                display: inline-block!important;
                margin: 0px 10px;
            }


        </style>





@stop