@extends('layouts.master')

@section('title', 'Abandon Session')

@section('content')
    <?php

    if(!$sessions_details->is_completed == "-1"){
        $profile_picture = "";
        if($sessions_details->profile_image == '') {
            if($sessions_details->gender == 'm') {
                $profile_picture = URL::asset('assets/images/tutor-male.png');
            } else {
                $profile_picture = URL::asset('assets/images/tutor-female.png');
            }
        } else {
            $profile_picture = $sessions_details->profile_image;
        }
    }



    ?>
    <section class="body-content container">
        <!-- Content Holder -->
        <div class="inner-wrapper">
            <h2 class="heading">Abandoned Sessions </h2>
            <div class="session-info">
                <div class="info-panel col-sm-6">
                    <div class="info-raw">
                        <p>Subject</p>
                        <span class="subject">{{ $sessions_details->subject_name }}</span>
                    </div>
                    <div class="info-raw">
                        <p>Date</p>
                        <span class="date">{{ date('F j, Y', strtotime($sessions_details->date)) }}</span>
                    </div>
                    <div class="info-raw">
                        <p>Duration</p>
                        <span class="duration">{{ $sessions_details->duration }} Hour</span>
                    </div>
                    <div class="info-raw">
                        <p>Time</p>
                        <span class="time">{{ date("g a", strtotime($sessions_details->start_time.':00')) }} to {{ date("g a", strtotime($sessions_details->end_time.':00')) }}</span>
                    </div>
                    <div class="info-raw">
                        <p>Location</p>
                        <span class="location">{{ trim (  $sessions_details->street. ",".$sessions_details->city. ",".$sessions_details->state , ',')}}</span>
                    </div>
                    {{--<div class="info-raw">--}}
                        {{--<p>Rate</p>--}}
                        {{--<span class="rate">${{ $sessions_details->per_hour}}/Hr</span>--}}
                    {{--</div>--}}
                    {{--<div class="info-raw">--}}
                        {{--<p>Student Payment</p>--}}
                        {{--<span class="payment">${{ $sessions_details->student_payment }}/Hour</span>--}}
                        {{--<a href="#" class="btn btn-default">{{$sessions_details->package_type}}</a>--}}
                    {{--</div>--}}
                </div>

                <div class="prof-cards-holder col-sm-5">
                    <?php  if($sessions_details->is_completed != "-1"){ ?>
                    <!-- Tutor Profile Card -->
                    <div class="tutor profile">
                        <h3 class="profile-title">Tutor Profile</h3>
                        <div class="profile-bio">
                            <div class="profile-img-holder">
                                <img src="<?php  echo $profile_picture;  ?>" alt="Tutor Profile Image" class="img-circle">
                            </div>
                            <div class="profile-bio-info">
                                <h3>{{$sessions_details->tutor_first_name.' '.$sessions_details->tutor_last_name }}</h3>

                                <p><i class="fa fa-mars"></i>{{ $sessions_details->gender }}</p>
                                <p><i class="fa fa-user"></i>{{ $sessions_details->tutor_type }}</p>
                            </div>
                        </div>

                    </div>

                    <?php } ?>
                    <!-- Student Profile Card -->
                    <div class="student profile">
                        <h3 class="profile-title">Student Profile</h3>
                        <div class="profile-bio">
                            <!--   <div class="profile-img-holder">
                            <img src="{{ URL::asset('assets/images/tutor-prof-img.png') }}" alt="Student Profile Image" class="img-circle">
                        </div> -->
                            <div class="profile-bio-info">
                                <h3>{{ $sessions_details->student_first_name.' '.$sessions_details->student_last_name }}</h3>

                                <p><i class="fa fa-phone"></i>{{ $sessions_details->phone }}</p>
                                <p><i class="fa fa-star"></i>{{ $sessions_details->student_email }}</p>
                                {{--<p><i class="fa fa-map-marker"></i>{{ $sessions_details->city }},{{ $sessions_details->state }}</p>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Sessions controlers -->
            <div class="session-btns">
                <a href="#" class="cancel-btn btn btn-default">Cancel</a>
                <a href="{{ url('tutors/update_booking/'.$sessions_details->session_id) }}" class="reschedule-btn btn btn-default">Assign Tutor</a>
            </div>
        </div>
    </section>




@stop