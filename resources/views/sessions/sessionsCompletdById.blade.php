@extends('layouts.master')

@section('title', 'All Tutors')

@section('content')
<?php
$profile_picture = "";
if($sessions_details->profile_image == '') {
    if($sessions_details->gender == 'm') {
        $profile_picture = URL::asset('assets/images/tutor-male.png');
    } else {
        $profile_picture = URL::asset('assets/images/tutor-female.png');
    }
} else {
    $profile_picture = $sessions_details->profile_image;
}
?>
    <section class="body-content container">
        <!-- Content Holder -->
        <div class="inner-wrapper">
            <h2 class="heading">Completed Sessions</h2>
            <div class="session-info">
                <div class="info-panel col-sm-6">
                    <div class="info-raw">
                        <p>Subject</p>
                        <span class="subject">{{ $sessions_details->subject_name }}</span>
                    </div>
                    <div class="info-raw">
                        <p>Date</p>
                        <span class="date">{{ date('F j, Y', strtotime($sessions_details->date)) }}</span>
                    </div>
                    <div class="info-raw">
                        <p>Duration</p>
                        <span class="duration">{{ $sessions_details->duration }} Hour</span>
                    </div>
                    <div class="info-raw">
                        <p>Time</p>
                        <span class="time">{{ date("g a", strtotime($sessions_details->start_time.':00')) }} to {{ date("g a", strtotime($sessions_details->end_time.':00')) }}</span>
                    </div>

                    <div class="info-raw">
                        <p>Location</p>
                        <span class="location">{{ trim (  $sessions_details->street. ",".$sessions_details->city. ",".$sessions_details->state , ',')}}</span>
                    </div>

                    <?php if($sessions_details->package_type == "Package") {  ?>
                        <div class="info-raw">
                            <p>Package: </p>
                            <span class="rate">10 Hours <?php echo "[".$sessions_details->subject_name."] [".$sessions_details->grade_name."]"; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Hours Remaining: </p>
                            <span class="rate"><?php echo $sessions_details->remaining_hours." hr(s)"; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Total: </p>
                            <span class="rate">$<?php echo $sessions_details->student_payment; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Discount: </p>
                            <span class="rate"><?php echo $sessions_details->discount; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Coupon code: </p>
                            <span class="rate"><?php echo $sessions_details->coupon_code; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Final Total: </p>
                            <span class="rate">$<?php echo $sessions_details->final_total; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Status:[Paid/Payment Pending] </p>
                    <span class="rate">
                        <?php

                        if($sessions_details->is_charged==0) {
                            echo "Payment Pending";
                        } else {
                            echo "Paid";
                        } ?>

                    </span>
                        </div>
                    <?php } else { ?>
                        <div class="info-raw">
                            <p>Hourly Price: </p>
                            <span class="rate">$<?php echo $sessions_details->per_hour; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Total: </p>
                            <span class="rate">$<?php echo $sessions_details->student_payment; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Discount: </p>
                            <span class="rate"><?php echo $sessions_details->discount; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Coupon code: </p>
                            <span class="rate"><?php echo $sessions_details->coupon_code; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Final Total: </p>
                            <span class="rate">$<?php echo $sessions_details->final_total; ?></span>
                        </div>
                        <div class="info-raw">
                            <p> Status:[Paid/Payment Pending] </p>
                    <span class="rate">
                        <?php

                        if($sessions_details->is_charged==0) {
                            echo "Payment Pending";
                        } else {
                            echo "Paid";
                        } ?>

                    </span>
                        </div>
                    <?php }  ?>
                </div>
                <div class="prof-cards-holder col-sm-5">
                    <!-- Tutor Profile Card -->
                    <div class="tutor profile">
                        <h3 class="profile-title">Tutor Profile</h3>
                        <div class="profile-bio">
                            <div class="profile-img-holder">
                                <img src="<?php echo $profile_picture; ?>" alt="Tutor Profile Image" class="img-circle img-responsive">
                            </div>
                            <div class="profile-bio-info">
                                <h3>{{$sessions_details->tutor_first_name.' '.$sessions_details->tutor_last_name }}</h3>

                                <p><i class="fa fa-mars"></i>{{ $sessions_details->gender }}</p>
                                <p><i class="fa fa-user"></i>{{ $sessions_details->tutor_type }}</p>
                                <p><i class="fa fa-phone"></i>{{ $sessions_details->tutor_phone }}</p>
                                <p><i class="fa fa-envelope"></i>{{ $sessions_details->tutor_email }}</p>
                            </div>
                        </div>
                    </div>
                    <!-- Student Profile Card -->
                    <div class="student profile">
                        <h3 class="profile-title">Student Profile</h3>

                        <div class="profile-bio">
                        <!--    <div class="profile-img-holder">
                                <img src="{{ URL::asset('assets/images/tutor-prof-img.png') }}" alt="Student Profile Image" class="img-circle">
                            </div> -->
                            <div class="profile-bio-info">
                                <h3>{{ $sessions_details->student_first_name.' '.$sessions_details->student_last_name }}</h3>

                                <p><i class="fa fa-phone"></i>{{ $sessions_details->phone }}</p>
                                <p><i class="fa fa-star"></i>{{ $sessions_details->student_email }}</p>
                                {{--<p><i class="fa fa-envelope"></i>{{ $sessions_details->city }},{{ $sessions_details->state }}</p>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop