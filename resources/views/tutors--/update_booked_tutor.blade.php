@extends('layouts.master')

@section('title', 'All Sessions - TutorSync')

@section('content')

    <section class="body-content container">
        <!-- Content Holder -->
        <div class="inner-wrapper">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="filter-form col-sm-10">
                {!! Form::open(array('action' => 'TutorsController@find', 'class' => 'form-inline','role'=>'form')) !!}
                <div class="form-group">
                    {!! Form::hidden('subject') !!}
                    {!! Form::label('', 'Student Email', array('for' => 'studentEmail'))!!}
                    {!! Form::text('studentEmail', Input::get("studentEmail"), array('class' => 'form-control','name'=>'studentEmail', 'id'=>'studentEmail','placeholder'=>''))!!}
                </div>
                <div class="form-group">
                    <label for="studentGrade">Grade</label>
                    <?php

                    $studentGrade = array();
                    $studentGrade[''] = 'select*';
                    foreach($tutor_details['grades'] as $value){
                        $studentGrade[$value->grade_id] = $value->grade;
                    }

                    ?>
                    {!! Form::select('studentGrade', $studentGrade, null, array('class' => 'form-control', 'id'=> "studentGrade")) !!}

                </div>
                <div class="form-group">
                    <label for="categories">Subject</label>
                    <select class="form-control" name="categories" id="categories">
                        <option id="subjects" value="">select*</option>
                    </select>
                </div>
                <div class="tags-holder" id="subjectsList">
                </div>
                <div class="form-group">
                    <div class="calendar">
                        {!! Form::label('', 'Session Date', array('for' => 'sessionDate'))!!}
                        {!! Form::text('sessionDate', Input::get("sessionDate"), array('class' => 'form-control','name'=>'sessionDate', 'id'=>'sessionDate','placeholder'=>''))!!}
                        <span class="fa fa-calendar"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="time">Time</label>
                    {!! Form::label('', 'Time', array('for' => 'time'))!!}
                    <div class="time-holder">
                        <div class="select-time start-time">
                            {!! Form::text('beginTime', Input::get("beginTime"), array('class' => 'beginTime form-control small-field', 'id'=>'time','placeholder'=>''))!!}
                            {!! Form::select('beginTimeType', array('AM' => 'AM', 'PM' => 'PM'), null, array('class' => 'form-control small-field', 'id'=> "beginTimeType")) !!}
                        </div>
                        <div class="select-time end-time">
                            {!! Form::text('endTime', Input::get("endTime"), array('class' => 'endTime form-control small-field', 'id'=>'time','placeholder'=>''))!!}
                            {!! Form::select('endTimeType', array('AM' => 'AM', 'PM' => 'PM'), null, array('class' => 'form-control small-field', 'id'=> "endTimeType")) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tutorGender">Gender</label>
                    {!! Form::select('tutorGender', array('' => 'Any', 'm' => 'Male', 'f' => 'Female'), null, array('class' => 'form-control', 'id'=> "tutorGender")) !!}
                </div>
                <div class="form-group">
                    <div class="filter-location">
                        <label for="location">Location</label>
                        {!! Form::text('location', Input::get("location"), array('class' => 'form-control','name'=>'location', 'id'=>'location','placeholder'=>''))!!}
                        <span class="fa fa-map-marker"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tutorName">Tutor Name</label>
                    {!! Form::text('tutorName', Input::get("tutorName"), array('class' => 'form-control','name'=>'tutorName', 'id'=>'tutorName','placeholder'=>''))!!}
                </div>
                <div class="form-group">
                    <label for="tutorType">Tutor Type</label>
                    {!! Form::select('tutorType', array('' => 'ANY', 'PRO' => 'PRO', 'PEER' => 'PEER'), null, array('class' => 'form-control', 'id'=> "tutorType")) !!}
                </div>

                <div class="form-group submit-holder">
                    <div class="form-field">
                        {!! Form::button('Find a Tutor',array('type'=>'submit','class'=>'btn btn-default')) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- Tutor Results -->
            <div class="tutor-result-holder col-sm-12">


                <?php $session_id = Route::getCurrentRoute()->getParameter('id');?>

                @foreach($tutor_details['selected_tutors'] as $each_tutor)

                    {!! Form::open(array('action' => array('TutorsController@storesession'), 'class' => 'form-inline','role'=>'form')) !!}
                    {!! Form::hidden('session_data', serialize($each_tutor['session_data'])) !!}

                    <div class="row-result">
                        <div class="tutor-bio col-sm-4">
                            <div class="profile-img-holder">
                                <img class="img-circle" alt="Tutor Profile Image" src="{{ URL::asset('assets/images/tutor-prof-img.png') }}">
                            </div>
                            <div class="profile-bio-info">
                                <h3><a href="{{ url('tutors/'. $each_tutor['tutor_id']) }}"><?php echo $each_tutor['first_name'].' '.$each_tutor['last_name']; ?></a></h3>
                                <p><i class="fa fa-graduation-cap fa-fw"></i> </p>
                                <p><i class="fa fa-map-marker fa-fw"></i> <?php echo $each_tutor['city'];  ?>,
                                    <?php echo $each_tutor['state'];  ?></p>
                            </div>
                        </div>
                        <div class="tutor-intro col-sm-4">
                            <p>Price : <?php echo $each_tutor['session_charge']; ?></p>
                        </div>
                        <div class="tutor-rating col-sm-4">
                            <div class="star-rating tutor-rating">
                                @for( $i=0; $i < $each_tutor['rating']; $i++)
                                    <span class="fa fa-star fa-star-active" data-rating="1"></span>
                                @endfor
                                @for( $i=0; $i < 5-$each_tutor['rating']; $i++)
                                    <span class="fa fa-star" data-rating="3"></span>
                                @endfor
                            </div>
                            <p class="review-stats"><span class="num-of-students">{{ $each_tutor['rating_count'] }}</span> Reviewes</p>
                            <div class="form-group submit-holder">
                                <div class="form-field">{!! Form::button('Assign This Tutor',array('type'=>'submit','class'=>'btn btn-default')) !!}

                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @endforeach

            </div>
        </div>
    </section>

    <script type="text/javascript">

        $( document ).ready(function() {

            $('#studentGrade').on('change',function(){

                $('input[name="subject"]').val('');
                var grade_id = $(this).val();
                var token =  $("input[name=_token]").val();

                $.ajax({
                    url: 'update_booking_categories_for_grade',
                    type: "post",
                    dataType: "json",
                    data: {'grade_id':grade_id, '_token': token},
                    success: function(response){
                        $('#categories').html($("<option></option>").attr("value",'').text('select*'));
                        $('#subjectsList').empty();
                        $.each(response, function(key, value) {
                            var category_id = value['category_id'];
                            $('#categories').append($("<option></option>").attr("value",category_id).text(value['category_name']));
                        });
                    }
                });
            });


            $('#categories').on('change',function(){

                $('input[name="subject"]').val('');
                var category_id = $(this).val();
                var token =  $("input[name=_token]").val();

                $.ajax({
                    url: 'update_booking_categories_for_subject',
                    type: "post",
                    dataType: "json",
                    data: {'category_id':category_id, '_token': token},
                    success: function(response){
                        $('#subjectsList').empty();
                        $.each(response, function(key, value) {
                            var subject_id = value['subject_id'];
                            $('#subjectsList').append('<span class="tag" onClick="subjectTag('+value['subject_id']+');" id="subject_'+value['subject_id']+'"><i class="fa fa-check"></i>' +value['subject_name']+'</span>');
                        });
                    }
                });

            });

        });

        @if(!empty(Session::get('studentGrade')))

            var grade_id = '{{ Session::get('studentGrade') }}';
            var token =  $("input[name=_token]").val();

            $.ajax({
                url: 'update_booking_categories_for_grade',
                type: "post",
                dataType: "json",
                data: {'grade_id':grade_id, '_token': token},
                success: function(response){
                    $('#categories').html($("<option></option>").attr("value",'').text('select*'));
                    $('#subjectsList').empty();
                    $.each(response, function(key, value) {
                        var category_id = value['category_id'];
                        $('#categories').append($("<option></option>").attr("value",category_id).text(value['category_name']));
                    });

                    $("#categories option[value='{{ Session::get('categories') }}']").attr("selected","selected");
                }
            });

            @if(!empty(Session::get('categories')))

                var category_id = '{{ Session::get('categories') }}';
                var token =  $("input[name=_token]").val();

                $.ajax({
                    url: 'update_booking_categories_for_subject',
                    type: "post",
                    dataType: "json",
                    data: {'category_id':category_id, '_token': token},
                    success: function(response){
                        $('#subjectsList').empty();
                        $.each(response, function(key, value) {
                            var subject_id = value['subject_id'];
                            $('#subjectsList').append('<span class="tag" onClick="subjectTag('+value['subject_id']+');" id="subject_'+value['subject_id']+'"><i class="fa fa-check"></i>' +value['subject_name']+'</span>');
                        });

                        subject_id = '#subject_'+'{{ Session::get('subject') }}';
                        $(subject_id).addClass("active");

                    }
                });

            @endif

        @else

        var session_id = '{{ Route::getCurrentRoute()->getParameter('id') }}';
        var token =  $("input[name=_token]").val();

        $.ajax({
            url: 'update_booking_get_session_details_onload',
            type: "post",
            dataType: "json",
            data: {'session_id':session_id, '_token': token},
            success: function(response){
//                console.log(response);return false;

                $('#studentEmail').val(response['student_email']);

                var dateAr = response['date'].split('-');
                var newDate = dateAr[1] + '/' + dateAr[2] + '/' + dateAr[0];

                $('#sessionDate').val(newDate);
                $('#location').val(response['location']);


                $("#studentGrade option[value='"+response['grade_id']+"']").attr("selected","selected");

                if(!$.isEmptyObject(response['gender']) || !$.isEmptyObject(response['tutor_type']) || !$.isEmptyObject(response['tutor_name'])){
                    $("#tutorGender option[value='" + response['gender'] + "']").attr("selected", "selected");
                    $("#tutorType option[value='" + response['tutor_type'] + "']").attr("selected", "selected");
                    $('#tutorName').val(response['tutor_name']);
                }

                if(response['start_time'] < 12){
                    $('.beginTime').val(response['start_time']);
                    $("#beginTimeType option[value='"+response['AM']+"']").attr("selected","selected");
                } else {
                    $("#beginTimeType option[value='"+response['PM']+"']").attr("selected","selected");
                    $start_time = response['start_time'] % 12
                    $('.beginTime').val($start_time);
                }

                if(response['end_time'] < 12){
                    $("#endTimeType option[value='"+response['AM']+"']").attr("selected","selected");
                    $('.endTime').val(response['end_time']);
                } else {
                    $("#endTimeType option[value='"+response['PM']+"']").attr("selected","selected");
                    $end_time = response['end_time'] % 12
                    $('.endTime').val($end_time);
                }



                /*
                 **   Next Ajax Request to list subjects
                 **
                 */

                var grade_id = response['grade_id'];
                var token =  $("input[name=_token]").val();

                $.ajax({
                    url: 'update_booking_categories_for_grade',
                    type: "post",
                    dataType: "json",
                    data: {'grade_id':grade_id, '_token': token},
                    success: function(response_next){
                        $('#categories').html($("<option></option>").attr("value",'').text('select*'));
                        $('#subjectsList').empty();
                        $.each(response_next, function(key, value) {
                            var category_id = value['category_id'];
                            $('#categories').append($("<option></option>").attr("value",category_id).text(value['category_name']));
                        });

                        var category_id = response['category_id'];
                        var token =  $("input[name=_token]").val();

                        $.ajax({
                            url: 'update_booking_categories_for_subject',
                            type: "post",
                            dataType: "json",
                            data: {'category_id':category_id, '_token': token},
                            success: function(response_next_next){
                                $('#subjectsList').empty();
                                $.each(response_next_next, function(key, value) {
                                    var subject_id = value['subject_id'];
                                    $('#subjectsList').append('<span class="tag" onClick="subjectTag('+value['subject_id']+');" id="subject_'+value['subject_id']+'"><i class="fa fa-check"></i>' +value['subject_name']+'</span>');
                                });

                               // console.log(response['category_id']);
//                                $("#categories option[value='12']").attr("selected","selected");
                                $("#categories option[value='"+response['category_id']+"']").attr("selected","selected");
                                subject_id = '#subject_'+response['subject_id'];
                                $(subject_id).addClass("active");
                                $('input[name="subject"]').val(response['subject_id']);

                            }
                        });


                    }
                });

            }
        });

        @endif

        function subjectTag($id) {

            subject_id = '#subject_'+$id;
            $('.tag').removeClass("active");
            $(subject_id).addClass("active");

            $('input[name="subject"]').val($id);

        }

    </script>


@stop
