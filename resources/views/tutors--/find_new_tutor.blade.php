@extends('layouts.master')

@section('title', 'All Sessions - TutorSync')

@section('content')
<script src="{{ URL::asset('assets/libs/zabuto_calendar.min.js') }}" type="text/javascript"></script>


<link href="{{ URL::asset('assets/css/zabuto_calendar.min.css') }}" rel="stylesheet" type="text/css"/>


    <section class="body-content container">
        <!-- Content Holder -->
        <div class="inner-wrapper">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(array('action' => 'TutorsController@find', 'class' => 'form-inline','role'=>'form')) !!}

            @if(isset($_POST['_token']))
                {!! Form::hidden('subject', $_POST['subject']) !!}
                {!! Form::hidden('gender', $_POST["gender"] ) !!}
                {!! Form::hidden('tutor_type',$_POST["tutor_type"]) !!}
                {!! Form::hidden('package_selected',$_POST["package_selected"]) !!}
                {!! Form::hidden('pro_hour_fee',$_POST["pro_hour_fee"] ) !!}
                {!! Form::hidden('peer_hour_fee',$_POST["peer_hour_fee"] ) !!}
                {!! Form::hidden('package_hour_fee',$_POST["package_hour_fee"] ) !!}
                {!! Form::hidden('student_id',$_POST["student_id"] ) !!}
                {!! Form::hidden('session_date',$_POST["session_date"] ) !!}
                {!! Form::hidden('begin_time',$_POST["begin_time"] ) !!}
            {!! Form::hidden('search_tutor_id',$_POST["search_tutor_id"] ) !!}

         @else

            {!! Form::hidden('subject') !!}
            {!! Form::hidden('categories') !!}
            {!! Form::hidden('studentGrade') !!}
            {!! Form::hidden('gender','any_gender') !!}
            {!! Form::hidden('tutor_type','PRO') !!}
            {!! Form::hidden('package_selected','1') !!}
            {!! Form::hidden('pro_hour_fee') !!}
            {!! Form::hidden('peer_hour_fee') !!}
            {!! Form::hidden('package_hour_fee') !!}
            {!! Form::hidden('student_id') !!}
            {!! Form::hidden('session_date') !!}
            {!! Form::hidden('session_duration') !!}
            {!! Form::hidden('begin_time') !!}
            {!! Form::hidden('search_tutor_id') !!}
            {!! Form::hidden('available_tutors_only','no') !!}
            {!! Form::hidden('all_tutors_list') !!}

            @endif
            <div class="student-data-filter">
                <div class="form-inline student-email-filter inner-wrapper" role="form">
                    <div class="form-group">
                        <label for="studentEmail">Student Email</label>
                        {!! Form::text('studentEmail', Input::get("studentEmail"), array('class' => 'form-control','name'=>'studentEmail', 'id'=>'studentEmail','placeholder'=>''))!!}
                    </div>
                </div>
                <div class="summery-holder inner-wrapper" id="selected_student_summery" style="display: none">

                </div>
            </div>
            <div class="inner-wrapper">
            <div class="filter-form pick-tutor col-sm-12">
                <form class="form-inline" role="form">

                    <div class="form-group">
                        <label for="studentGrade">Grade</label>
                        <?php
                        $studentGrade = array();
                        $studentGrade[''] = 'select*';
                        foreach($tutor_details['grades'] as $value){
                            $studentGrade[$value->grade_id] = $value->grade;
                        }
                        ?>
                        {!! Form::select('studentGrade', $studentGrade, null, array('class' => 'form-control', 'id'=> "studentGrade")) !!}
                    </div>
                    <div class="form-group">
                        <label for="categories">Subject</label>
                        <select class="form-control" name="categories" id="categories">
                            <option id="subjects" value="">select*</option>
                        </select>
                    </div>
                    <div class="tags-holder" id="subjectsList">

                    </div>

                    <div class="gender-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <p>Gender Preference of Tutor</p>
                        </div>
                        <div class="gender-options col-sm-10">
                            <div class="col-lg-4 col-md-4 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_gender <?php if(!isset($_POST['gender'])) { echo 'st_selected_type'; } else if($_POST['gender'] == 'any_gender') {  echo 'st_selected_type';  } ?> gender_common" onclick="modify_selection('any_gender',this,'gender_common')" >
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_3_1.png') }}" />
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_3_1-1_.png') }}" />
                                            Any
                                        </h6>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <!-- student_step_common_wrapper -->
                            <div class="col-lg-4 col-md-4 col-sm-12 student_step_common_wrapper ">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_gender gender_common <?php if(isset($_POST['gender'])) { if($_POST['gender'] == 'm') {  echo 'st_selected_type';  }}  ?>" onclick="modify_selection('m',this,'gender_common')">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_3_2.png') }}" >
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_3_2-1.png') }}" >
                                            male
                                        </h6>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <!-- student_step_common_wrapper -->
                            <div class="col-lg-4 col-md-4 col-sm-12 student_step_common_wrapper ">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_gender gender_common <?php if(isset($_POST['gender'])) { if($_POST['gender'] == 'f') {  echo 'st_selected_type';  }}  ?>" onclick="modify_selection('f',this,'gender_common')">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_3_3.png') }}">
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_3_3-1.png') }}">
                                            Female
                                        </h6>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_type tutor_type_common <?php if(isset($_POST['tutor_type'])) { if($_POST['tutor_type'] == 'PRO') {  echo 'st_selected_type';  }} else {   echo 'st_selected_type';   }  ?>" onclick="modify_selection('PRO',this,'tutor_type')">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_5-1.png') }}" >
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_5-2.png') }}" >
                                            PRO
                                        </h6>
                                        <div class="row row_clr type_of_tutor_step_3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h1 class="ng-binding" id="pro_tutor_fee"><?php if(isset($_POST['pro_hour_fee'])) { echo "$".$_POST['pro_hour_fee'];} ?></h1>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h3>PER<br>
                                                    HOUR
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                            <!-- student_step_common_wrapper -->
                            <div class="col-lg-6 col-md-6 col-sm-12 student_step_common_wrapper">
                                <div class="row row_clr">
                                    <div class="col-xs-12 student_step_3_type tutor_type_common <?php if(isset($_POST['tutor_type'])) { if($_POST['tutor_type'] == 'PEER') {  echo 'st_selected_type';  }}  ?>" onclick="modify_selection('PEER',this,'tutor_type')">
                                        <h6>
                                            <img class="st_step_3_show_icon" src="{{ URL::asset('assets/images/student_step_4-1.png') }}">
                                            <img class="st_step_3_hiden_icon" src="{{ URL::asset('assets/images/student_step_4-2.png') }}" >
                                            Peer
                                        </h6>
                                        <div class="row row_clr type_of_tutor_step_3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h1 class="ng-binding" id="peer_tutor_fee"><?php if(isset($_POST['peer_hour_fee'])) { echo "$".$_POST['peer_hour_fee'];} ?></h1>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <h3>PER<br>
                                                    HOUR
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- student_step_3_gender -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="package-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <p>Select Type of Package</p>
                        </div>
                        <div class="gender-options col-sm-10">
                            <div class="package-options">
                                <div class="package-opt col-sm-6 <?php if(isset($_POST['package_selected'])) { if($_POST['package_selected'] == '1') {  echo 'active';  }} else {   echo 'active';   }  ?>" id="package_tag" onclick="package_selection('1',this)" <?php if(isset($_POST['package_selected'])) { if($_POST['package_selected'] == '0' && $_POST['tutor_type'] == 'PEER') { echo "style='display:none;'"; }} ?> >
                                    <div class="inner-wrapper">
                                        <p class="package-opt-title">Use Package</p>
                                        <div class="price-per-hour" id="package_per_hour_block">
                                            <p class="price" id="package_hourly_fee"><?php if(isset($_POST['package_hour_fee'])) { echo "$".$_POST['package_hour_fee'];} ?></p>
                                            <p class="per-hour-text">Per Hour</p>
                                        </div>
                                        <div class="amount col-sm-4" style="float: right;display: none" id="remaining_hours">

                                            <div class="time-remaining"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="package-opt col-sm-6 <?php if(isset($_POST['package_selected'])) { if($_POST['package_selected'] == '0') {  echo 'active';  }} ?>"  onclick="package_selection('0',this)">
                                    <div class="inner-wrapper">
                                        <p class="package-opt-title">Hourly</p>
                                        <div class="price-per-hour">
                                            <p class="price" id="no_package_hourly_fee"><?php if(isset($_POST['tutor_type'])) { if($_POST['tutor_type'] == 'PRO') { echo "$".$_POST['pro_hour_fee']; } else if($_POST['tutor_type'] == 'PEER') { echo "$".$_POST['peer_hour_fee']; }} ?></p>
                                            <p class="per-hour-text">Per Hour</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-ng-show="showschedule" class="package-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 student_step_common_wrapper">
                                    <p>Duration Of First Session</p>
                                </div>
                                <!-- student_step_common_wrapper -->
                            </div>
                        </div>
                        <!-- student_step_common_wrapper -->
                        <div data-ng-show="showschedule" class="col-lg-9 col-md-9 col-sm-12 col-xs-12 step_common_wrapper">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 st_step_4_hours">
                                    <div  class="st_hours" onclick="select_session_duration(1,this)">
                                        <h4>1</h4>
                                    </div>
                                    <div class="st_hours" onclick="select_session_duration(2,this)">
                                        <h4>2</h4>
                                    </div>
                                    <div class="st_hours" onclick="select_session_duration(3,this)">
                                        <h4>3</h4>
                                    </div>
                                    <div class="st_hours" onclick="select_session_duration(4,this)">
                                        <h4>4</h4>
                                    </div>
                                    <h5>Hours</h5>
                                </div>
                                <!-- step_1_common_wrapper -->
                            </div>
                        </div>
                        <!-- step_common_wrapper -->
                    </div>
<!--
                    <div class="form-group">
                        <div class="calendar">
                            {!! Form::label('', 'Session Date', array('for' => 'sessionDate'))!!}
                            {!! Form::text('sessionDate', Input::get("sessionDate"), array('class' => 'form-control','name'=>'sessionDate', 'id'=>'sessionDate','placeholder'=>''))!!}

                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                    <div class="form-group select-tutor-time ">
                        <label for="time">Time</label>
                        {!! Form::label('', 'Time', array('for' => 'time'))!!}
                        <div class="time-holder">
                            <div class="select-time start-time">
                                {!! Form::text('beginTime', Input::get("beginTime"), array('class' => 'form-control small-field', 'id'=>'time','placeholder'=>''))!!}
                                {!! Form::select('beginTimeType', array('AM' => 'AM', 'PM' => 'PM'), null, array('class' => 'form-control small-field', 'id'=> "beginTimeType")) !!}
                            </div>
                            <div class="select-time end-time">
                                {!! Form::text('endTime', Input::get("endTime"), array('class' => 'form-control small-field', 'id'=>'time','placeholder'=>''))!!}
                                {!! Form::select('endTimeType', array('AM' => 'AM', 'PM' => 'PM'), null, array('class' => 'form-control small-field', 'id'=> "endTimeType")) !!}
                            </div>
                        </div>
                    </div>
                    -->
                    <div data-ng-show="showschedule" class="package-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 student_step_common_wrapper">
                                    <p>Pick a Date</p>
                                </div>
                                <!-- student_step_common_wrapper -->
                            </div>
                        </div>
                        <!-- student_step_common_wrapper -->
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 step_common_wrapper">
                            <div class="row">
                                <!-- <div ui-calendar="uiConfig.calendar" data-ng-model="eventSources" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 st_step_4_hours">
                                     </div>-->
                                <div id="eventCalendarDefault"></div>
                                <!-- step_1_common_wrapper -->
                            </div>
                        </div>
                        <!-- step_common_wrapper -->
                        <!-- Choose -->

                    </div>
                    <div data-ng-show="showschedule" class="package-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 student_step_common_wrapper">
                                    <p>Pick a Start Time</p>
                                </div>
                                <!-- student_step_common_wrapper -->
                            </div>
                        </div>
                        <!-- student_step_common_wrapper -->
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 step_common_wrapper">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 add_more_step">
                                            <div class="row">
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_10" class="select_time_button" onclick="select_start_time(10,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 10:00 AM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_11" class="select_time_button" onclick="select_start_time(11,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 11:00 AM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_12" class="select_time_button" onclick="select_start_time(12,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 12:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_13" class="select_time_button" onclick="select_start_time(13,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 1:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_14" class="select_time_button" onclick="select_start_time(14,this)">
                                                        <h6 class="subject_close_click" ><i class="fa"></i>&nbsp; 2:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_15" class="select_time_button" onclick="select_start_time(15,this)">
                                                        <h6 class="subject_close_click" ><i class="fa" ></i>&nbsp; 3:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_16" class="select_time_button"  onclick="select_start_time(16,this)">
                                                        <h6 class="subject_close_click" ><i class="fa" ></i>&nbsp; 4:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_17" class="select_time_button"  onclick="select_start_time(17,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 5:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_18" class="select_time_button"  onclick="select_start_time(18,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 6:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_19" class="select_time_button" onclick="select_start_time(19,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 7:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_20" class="select_time_button" onclick="select_start_time(20,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 8:00 PM</h6>
                                                    </div>
                                                </div>
                                                <div class="tutors-list col-sm-10">
                                                    <div class="opt-holder">
                                                        <input type="radio" name="available-tutors" id="all-tutors" checked="checked" onclick="show_available_tutors_only('no')">
                                                        <label class="radio-inline" for="all-tutors">Show All Tutors</label>
                                                    </div>
                                                    <div class="opt-holder">
                                                        <input type="radio" name="available-tutors" id="available-tutors"  onclick="show_available_tutors_only('yes')">
                                                        <label class="radio-inline" for="available-tutors">Show Available Tutors Only</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tutorName">Tutor Name</label>
                        {!! Form::text('tutorName', Input::get("tutorName"), array('class' => 'form-control','name'=>'tutorName', 'id'=>'tutorName','placeholder'=>''))!!}
                    </div>

                </form>
            </div>
            {!! Form::close() !!}
            <!-- Tutor Results -->
            <div class="tutor-result-holder col-sm-12" id="generated_tutor_result">

            </div>
            </div>

            <!-- Tutor Results -->

        </div>
    </section>

    <script type="text/javascript">
            var mycal = $("#eventCalendarDefault");

        var peer_houly_rate = '<?php if(isset($_POST['peer_hour_fee'])) { echo $_POST['peer_hour_fee']; } ?>';
        var pro_houly_rate = '<?php if(isset($_POST['pro_hour_fee'])) { echo $_POST['pro_hour_fee']; } ?>';
        $( document ).ready(function() {
            <?php
                if(isset($_POST['student_id'])) {
            ?>
            load_summery('<?php echo $_POST['student_id']; ?>','<?php echo $_POST['_token']; ?>');
            <?php
                }
            ?>
            $('#studentGrade').on('change',function(){
                $('#generated_tutor_result').html('');
                var grade_id = $(this).val();
                var token =  $("input[name=_token]").val();

                $('input[name="studentGrade"]').val(grade_id);
                $.ajax({
                    url: 'categories_for_grade',
                    type: "post",
                    dataType: "json",
                    data: {'grade_id':grade_id, '_token': token},
                    success: function(response){
                        $('#categories').html($("<option></option>").attr("value",'').text('select*'));
                        $('#subjectsList').empty();
                        $.each(response, function(key, value) {
                            var category_id = value['category_id'];
                            $('#categories').append($("<option></option>").attr("value",category_id).text(value['category_name']));
                        });
                    }
                });
            });



            $('#categories').on('change',function(){

                var category_id = $(this).val();
                var token =  $("input[name=_token]").val();
                $('input[name="categories"]').val(category_id);
                $('#generated_tutor_result').html('');
                $.ajax({
                    url: 'categories_for_subject',
                    type: "post",
                    dataType: "json",
                    data: {'category_id':category_id, '_token': token},
                    success: function(response){
                        $('#subjectsList').empty();
                        $.each(response, function(key, value) {
                            var subject_id = value['subject_id'];
                            $('#subjectsList').append('<span class="tag" onClick="subjectTag('+value['subject_id']+');" id="subject_'+value['subject_id']+'"><i class="fa fa-check"></i>' +value['subject_name']+'</span>');
                        });
                    }
                });

            });

            $('#studentEmail').autocomplete({
                source: "get_student_email",
                source: function(request, response) {
                    $.getJSON("get_student_email", { 'term':$('#studentEmail').val(), '_token' :$("input[name=_token]").val() },
                        response);
                },


                minLength: 1,
                select: function(event, ui) {
                    $('#studentEmail').val(ui.item.value);
                    load_summery(ui.item.id,$("input[name=_token]").val())
                }
            });

            $('#tutorName').autocomplete({
                source: "get_tutor_name",
                source: function(request, response) {
                    $.getJSON("get_tutor_name", { 'term':$('#tutorName').val(), '_token' :$("input[name=_token]").val() },
                        response);
                },


                minLength: 1,
                select: function(event, ui) {
                    $('#tutorName').val(ui.item.value);

                    $('input[name="search_tutor_id"]').val(ui.item.id);
                    if($('input[name="subject"]').val() != '') {
                        get_selected_tutors();
                    }
                }
            });



            mycal.zabuto_calendar({
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "get_available_tutors_for_calendar"
                }
            });

        });



    function subjectTag($id) {

            subject_id = '#subject_'+$id;
            $('.tag').removeClass("active");
            $(subject_id).addClass("active");
            $('input[name="subject"]').val($id);
            var token =  $("input[name=_token]").val();
            $.ajax({
                url: 'get_tutor_type_student_rates',
                type: "post",
                dataType: "json",
                data: {'subject_id':$id, '_token': token},
                success: function(response){
                    $('#pro_tutor_fee').html('$'+response.pro_student_amount);
                    $('#peer_tutor_fee').html('$'+response.peer_student_amount);
                    peer_houly_rate = response.peer_student_amount;
                    pro_houly_rate = response.pro_student_amount;


                    if($('input[name="tutor_type"]').val() == 'PRO') {
                        $('#no_package_hourly_fee').html('$'+response.pro_student_amount);
                    }
                    if($('input[name="tutor_type"]').val() == 'PEER') {
                        $('#no_package_hourly_fee').html('$'+response.peer_student_amount);
                    }
                    if($('input[name="tutor_type"]').val() == 'ALL') {
                        $('#no_package_hourly_fee').html('$'+response.pro_student_amount);
                    }

                    $('#package_hourly_fee').html('$'+response.package_10_rate);
                    $('input[name="pro_hour_fee"]').val(response.pro_student_amount);
                    $('input[name="peer_hour_fee"]').val(response.peer_student_amount);
                    $('input[name="package_hour_fee"]').val(response.package_10_rate);

                    get_selected_tutors();

                    update_calendar_availability();

                }
            });


        get_remaining_pro_hours($('input[name="tutor_type"]').val(), $id,$('input[name="studentGrade"]').val(),$('input[name="student_id"]').val());

        }
    function update_calendar_availability() {
        var calendar_subject = $('input[name="subject"]').val();
        var calendar_gender = $('input[name="gender"]').val();
        var calendar_tutor_type = $('input[name="tutor_type"]').val();
        var calendar_email = $('input[name="studentEmail"]').val();
        var calendar_duration = $('input[name="session_duration"]').val();

        $("#eventCalendarDefault").html('');
        $('.zabuto_calendar').remove();
        var calendar_display_date = $('input[name="session_date"]').val();
        if(calendar_display_date != '') {
            calendar_display_date = calendar_display_date.split('-');
            var calendar_display_month = "";

            if(calendar_display_date[1].length == 1) {
                calendar_display_month = '0'+calendar_display_date[1];
            } else {
                calendar_display_month = calendar_display_date[1];
            }

            mycal.zabuto_calendar({
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                month: calendar_display_month,
                year:calendar_display_date[0],
                ajax: {
                    url: "get_available_tutors_for_calendar?subject="+calendar_subject+"&gender="+calendar_gender+"&tutor_type="+calendar_tutor_type+"&studentEmail="+calendar_email+"&session_duration="+calendar_duration+"&month="+11
                }
            });

        } else {
        mycal.zabuto_calendar({
            action_nav: function () {
                return myNavFunction(this.id);
            },
            ajax: {
                url: "get_available_tutors_for_calendar?subject="+calendar_subject+"&gender="+calendar_gender+"&tutor_type="+calendar_tutor_type+"&studentEmail="+calendar_email+"&session_duration="+calendar_duration+"&month="+11
            }
        });
        }

    }
    function get_remaining_pro_hours(tutor_type,subject_id,grade_id,student_id) {
        if(tutor_type == 'PRO') {
            var token =  $("input[name=_token]").val();
            $.ajax({
                url: 'get_tutor_remaining_hours',
                type: "post",
                data: {'subject_id':subject_id,student_id:student_id,grade_id:grade_id, '_token': token},
                success: function(response){
                    if(response == 'null') {
                        $('#remaining_hours .time-remaining').html('');
                        $('#remaining_hours').hide();
                        $('#package_per_hour_block').show();

                    } else {
                        $('#package_per_hour_block').hide();
                        $('#remaining_hours .time-remaining').html(response);
                        $('#remaining_hours').show();
                    }
                }
            });
        } else {
            $('#remaining_hours .time-remaining').html('');
        }
    }
    function load_summery(user_id,token) {

        $('input[name="student_id"]').val(user_id);
        $.ajax({
            url: 'student_summery',
            type: "post",
            dataType: "json",
            data: {'user_id':user_id, '_token': token},
            success: function(response){
                html_print =  '<div class="summery-inner-wrapper col-sm-10"> <div class="label-wrappe"><p>Summery</p></div><div class="summmry-info col-sm-10"><div class="col-sm-4"><p class="summery-title">Student Name</p><p class="summery-data">'+response.first_name+' '+response.last_name+'</p></div><div class="col-sm-4"><p class="summery-title">Phone Number</p><p class="summery-data">'+response.phone+'</p></div><div class="col-sm-4"><p class="summery-title">Location</p><p class="summery-data address-line">'+response.street+'</p><p class="summery-data address-line">'+response.city+', '+response.state+' '+response.zip+'</p></div></div></div>';
                $('#selected_student_summery').html(html_print);
                $('#selected_student_summery').show('slow');
            }
        });

    }
    function modify_selection(section, element, type) {
        var token =  $("input[name=_token]").val();
        if (!$(element).hasClass('st_selected_type')) {
            if ($(element).hasClass('gender_common')) {
                $('.gender_common').removeClass('st_selected_type');

                $('input[name="gender"]').val(section);
                get_selected_tutors();
            }
            if ($(element).hasClass('tutor_type_common')) {
                $('.tutor_type_common').removeClass('st_selected_type');
                $('input[name="tutor_type"]').val(section);
                if(section == 'PEER') {
                    $('#no_package_hourly_fee').html('$'+peer_houly_rate);
                    $('#package_tag').hide();
                } else {
                    $('#package_tag').show();
                    $('#no_package_hourly_fee').html('$'+pro_houly_rate);
                }

                get_remaining_pro_hours($('input[name="tutor_type"]').val(),  $('input[name="subject"]').val(),$('input[name="studentGrade"]').val(),$('input[name="student_id"]').val());
                get_selected_tutors();
            }


            $(element).addClass("st_selected_type");
        }
        update_calendar_availability();
    }
    function package_selection(is_package,element) {
        if (!$(element).hasClass('active')) {

            $('.package-opt').removeClass('active');
            $(element).addClass("active");

            if(is_package == '1') {
                $('input[name="package_selected"]').val(1);
            } else {
                $('input[name="package_selected"]').val(0);
            }

        }
        update_calendar_availability();
    }
            function pad (str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }
    function select_session_duration(selected_duration,element) {
        $('input[name="session_duration"]').val(selected_duration);
        $('.st_hours').removeClass('student_hours_4');
        $(element).addClass("student_hours_4");
        $('input[name="begin_time"]').val('');

        var token =  $("input[name=_token]").val();
        if($('input[name="session_date"]').val() != '') {
            get_selected_tutors();
            var all_tutor_list = $('input[name="all_tutors_list"]').val();

            if($('input[name="session_duration"]').val() != '' && $('input[name="session_duration"]').val() != 0) {
                if(all_tutor_list != '') {
                    $.ajax({
                        url: 'get_available_time_slots',
                        type: "get",
                        dataType: "json",
                        data: {'tutor_list':all_tutor_list,'session_date':$('input[name="session_date"]').val() ,'session_duration':$('input[name="session_duration"]').val(), '_token': token},
                        success: function(response){
                            $(".select_time_button").removeClass("available_checked_time");
                            $(".select_time_button").removeClass("checked_time");
                            $.each(response, function(key, value) {
                                $('#slot_'+value).addClass('available_checked_time');
                            })
                        }
                    });
                }
            }
        }
        update_calendar_availability();
        var calendar_display_date = $('input[name="session_date"]').val();
        if(calendar_display_date != '') {
            calendar_display_date = calendar_display_date.split('-');
            var calendar_display_month = "";
            var calendar_display_date_print = "";

            if(calendar_display_date[1].length == 1) {
                calendar_display_month = '0'+calendar_display_date[1];
            } else {
                calendar_display_month = calendar_display_date[1];
            }
            if(calendar_display_date[2].length == 1) {
                calendar_display_date_print = '0'+calendar_display_date[2];
            } else {
                calendar_display_date_print = calendar_display_date[2];
            }

            var calendar_display_date_display_month = calendar_display_date[0]+'-'+calendar_display_month+'-'+calendar_display_date_print;
            console.log(calendar_display_date_display_month);
            $('#zabuto_calendar_teran_'+calendar_display_date_display_month+'_day').addClass('day selected_date_for_session');
        }

    }
    function select_start_time(selected_begin_time, element) {
        $('input[name="begin_time"]').val(selected_begin_time);
        $('.select_time_button').removeClass('checked_time');
        $(element).addClass("checked_time");
        if($('input[name="session_date"]').val() != '' && $('input[name="session_duration"]').val() != '') {
            get_selected_tutors();

        }
    }
    function get_date_selected(year_selected, month_selected, date_delected,element) {
        var token =  $("input[name=_token]").val();
        $(".selected_date_for_session").removeClass("selected_date_for_session");
        $('#'+element).addClass('selected_date_for_session');
        $('input[name="begin_time"]').val('');
        var set_session_date = year_selected+'-'+(month_selected+1)+'-'+date_delected;
        var currentTime = new Date();
        var show_result = 'no';
        if(currentTime.getFullYear() <= year_selected) {
            if(currentTime.getFullYear() == year_selected) {
                if(currentTime.getMonth() <= month_selected) {
                    if(currentTime.getMonth() == month_selected) {
                        if(currentTime.getDate() <= date_delected) {
                            $('input[name="session_date"]').val(set_session_date);

                            show_result = 'yes';
                        }
                    } else {
                        $('input[name="session_date"]').val(set_session_date);

                        show_result = 'yes';
                    }
                }

            } else {
                $('input[name="session_date"]').val(set_session_date);

                show_result = 'yes';
            }
        }
        if(show_result == 'yes') {
            get_selected_tutors();

            var all_tutor_list = $('input[name="all_tutors_list"]').val();

            if($('input[name="session_duration"]').val() != '' && $('input[name="session_duration"]').val() != 0) {
                if(all_tutor_list != '') {
                    $.ajax({
                        url: 'get_available_time_slots',
                        type: "get",
                        dataType: "json",
                        data: {'tutor_list':all_tutor_list,'session_date':set_session_date ,'session_duration':$('input[name="session_duration"]').val(), '_token': token},
                        success: function(response){
                            $(".select_time_button").removeClass("available_checked_time");
                            $(".select_time_button").removeClass("checked_time");
                            $.each(response, function(key, value) {
                                $('#slot_'+value).addClass('available_checked_time');
                            })
                        }
                    });
                }
            }
        }

    }
    function show_available_tutors_only(show_available_tutors_only) {
        if(show_available_tutors_only == 'yes') {
            $('input[name="available_tutors_only"]').val('yes');
        } else {
            $('input[name="available_tutors_only"]').val('no');
        }
        get_selected_tutors();
    }
    function get_selected_tutors() {

        var token =  $("input[name=_token]").val();
        var data_array = [];

        data_array['subject'] = $('input[name="subject"]').val();
        data_array['gender'] = $('input[name="gender"]').val();
        data_array['tutor_type'] = $('input[name="tutor_type"]').val();
        data_array['student_id'] = $('input[name="student_id"]').val();
        data_array['studentGrade'] = $('input[name="studentGrade"]').val();
        data_array['categories'] = $('input[name="categories"]').val();
        data_array['studentEmail'] = $('input[name="studentEmail"]').val();
        data_array['sessionDate'] = $('input[name="session_date"]').val();
        data_array['session_duration'] = $('input[name="session_duration"]').val();
        data_array['begin_time'] = $('input[name="begin_time"]').val();
        data_array['search_tutor_id'] = $('input[name="search_tutor_id"]').val();
        data_array['available_tutors_only'] = $('input[name="available_tutors_only"]').val();
        data_array['package_selected'] = $('input[name="package_selected"]').val();
        data_array['pro_hour_fee'] = $('input[name="pro_hour_fee"]').val();
        data_array['peer_hour_fee'] = $('input[name="peer_hour_fee"]').val();
        data_array['package_hour_fee'] = $('input[name="package_hour_fee"]').val();
        var all_tutors_list = "";
        $('#generated_tutor_result').html('');
        if(data_array['subject'] != '') {
            $.ajax({
                url: 'find',
                type: "post",
                dataType: "json",
                data: {'subject':data_array['subject'],'gender':data_array['gender'] ,'tutor_type':data_array['tutor_type'],'studentGrade':data_array['studentGrade'],'categories':data_array['categories'],'studentEmail':data_array['studentEmail'],'sessionDate':data_array['sessionDate'],'session_duration':data_array['session_duration'],'begin_time':data_array['begin_time'],'search_tutor_id':data_array['search_tutor_id'],'available_tutors_only':data_array['available_tutors_only'],'student_id':data_array['student_id'],'package_selected':data_array['package_selected'],'pro_hour_fee':data_array['pro_hour_fee'],'peer_hour_fee':data_array['peer_hour_fee'],'package_hour_fee':data_array['package_hour_fee'], '_token': token},
                success: function(response){
                    $('#generated_tutor_result').html(response.result);
                    all_tutors_list = response.all_tutor_list;
                    $('input[name="all_tutors_list"]').val(all_tutors_list);

                }
            });
        }


    }
    function tutor_booking_cofirm(tutor_id, is_available) {

        if(tutor_id == '' || is_available == '0' || $('input[name="session_date"]').val() == '' || $('input[name="session_duration"]').val() == '' || $('input[name="begin_time"]').val() == '' ) {
            alert("Please select date, duration and start time");
            return false;
       } else {
            var redirect_url = 'pick_tutor_payment_info/'+tutor_id;

            $(location).attr('href',redirect_url)
        }
    }
        function myNavFunction(id) {
            $(".select_time_button").removeClass("available_checked_time");
            $(".select_time_button").removeClass("checked_time");

                }


    </script>


@stop
