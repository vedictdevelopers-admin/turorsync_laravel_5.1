@extends('layouts.master')

@section('title', 'Tutor Profile - TutorSync')

@section('content')

<section class="body-content container">
    <!-- Panel Holder -->
    <div class="tutor-bio-card panel col-sm-4">
        <!-- Profile -->
        <div class="profile-card">
            <div class="inner-wrapper">
                <!-- Profile -->
                <?php
                    $profile_picture = "";
                    if($tutor['details']->profile_image == '') {
                        if($tutor['details']->gender == 'm') {
                            $profile_picture = URL::asset('assets/images/tutor-male.png');
                        } else {
                            $profile_picture = URL::asset('assets/images/tutor-female.png');
                        }
                    } else {
                            $profile_picture = $tutor['details']->profile_image;
                    }
                ?>
                <img src="<?php echo  $profile_picture; ?>" class="img-circle" alt="" width="90" height="90">
                <h3 class="title">{{ $tutor['details']->first_name.' '.$tutor['details']->last_name }}</h3>
                <a href="mailto:{{ $tutor['details']->email }}" class="email-add">{{ $tutor['details']->email }}</a>
                <div class="tut-bio">
                    <ul>

                        <li>
                            <p>Contact Number</p>
                            <span class="number">{{ $tutor['details']->phone }}</span>
                        </li>
                        <li>
                            <p>Location</p>
								<span class="locatione">
									{{ $tutor['details']->street }} {{ $tutor['details']->city }}., <br>{{$tutor['details']->state .','. $tutor['details']->zip }}
								</span>
                        </li>
                    </ul>
                </div>
                <div class="star-rating tutor-rating">
                    @for( $i=0; $i < $tutor['rating']; $i++)
                        <span class="fa fa-star fa-star-active" data-rating="1"></span>
                    @endfor
                    @for( $i=0; $i < 5-$tutor['rating']; $i++)
                        <span class="fa fa-star" data-rating="3"></span>
                    @endfor
                </div>
                <p class="review-stats"><span class="num-of-students">{{ $tutor['tutoring_sessions'] }}</span> Reviewes</p>
                <!-- Tutor Bio -->
                <div class="tut-bio">
                    <ul>
                        <li>
                            <p>Gender</p>
                            <span class="gender">{{ $tutor['details']->gender=='m'?'Male':'Female' }}</span>
                        </li>
                        <li>
                            <p>DOB</p>
                            <span class="dob">{{ date('m/d/Y', strtotime($tutor['details']->dob)) }}</span>
                        </li>
                        <li>
                            <p>Type of Tutor</p>
                            <span class="tutor-type">{{ $tutor['details']->tutor_type }}</span>
                        </li>
                    </ul>
                </div>
                <!-- Edit Profile -->
                <a href="#" class="edit-prof">Edit Profile</a>
            </div>
        </div>
        <!-- Earning Info Panel -->
        <div class="earning-info-panel">
            <div class="inner-wrapper">
                <h3>Earning <br/> Information</h3>
                <ul class="earning-info">
                    <li>
                        <p class="pull-left">Total no. of Hours</p>
                        <span class="tot-num-hours pull-right">
                         {{  $tutor['total_no_of_hours'][0]->total_hours }}   Hours
                        </span>
                    </li>
                    <li>
                        <p class="pull-left">Total Earning</p>
                        <span class="tot-earnings pull-right">
                            {{  $tutor['total_no_of_hours'][0]->total_earning }}
                        </span>
                    </li>
                    <li>
                        <p class="pull-left">Total Paid</p>
                        <p class="pay-period-wrapper">
                            <span class="pay-period pull-left"></span>
                            <span class="pp-earning pull-right">
                                {{  $tutor['total_paid_earning'][0]->total_paid }}
                            </span>
                        </p>
                    </li> <li>
                        <p class="pull-left">Pay Period</p>
                        <p class="pay-period-wrapper">
                            <span class="pay-period pull-left"></span>
                            <span class="pp-earning pull-right">0</span>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Tutor Acadimic Info Panel -->
    <div class="tutor-profile panel col-sm-8">
        <div class="inner-wrapper">
            <!-- Panel Section -->
            <section class="edu-info main-sub-sections">
                <div class="sub-inner-wrapper">
                    <div class="title">
                        <h3>Education Information</h3>
                        <div class="section-icons">
                            <ul>
                                <li><i class="fa fa-plus"></i></li>
                                <li><i class="fa fa-edit"></i></li>
                                <li><i class="fa fa-trash"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sub-section">

                        @foreach($tutor['education'] as $value)

                            <div class="col-sm-6">
                                <h4 class="sub-title">Degree</h4>
                                <p>{{ $value->college_name }}<br>{{ $value->degree }}<br>{{ $value->graduation_year }}</p>
                            </div>
                            <div class="col-sm-6">
                                @if(!empty($value->major))
                                <div class="specialized">
                                    <h4 class="sub-title">Major</h4>


                                    @foreach(json_decode($value->major) as $major_value)
                                        @if(is_array($major_value))
                                            @foreach( $major_value as  $list_value )
                                                <p class="minorin tick">   <i class="fa fa-check"></i>   {{ $list_value }}   </p>
                                            @endforeach
                                        @else
                                            <p class="minorin tick">   <i class="fa fa-check"></i>    {{$major_value}}   </p>
                                        @endif
                                    @endforeach



                                    {{--@foreach(json_decode($value->major) as $major_val)--}}
                                        {{--<p class="majorin tick">     <i class="fa fa-check"></i> {{$major_val}} </p>--}}
                                    {{--@endforeach--}}

                                </div>
                                @endif
                                @if(!empty($value->minor))
                                <div class="specialized">
                                    <h4 class="sub-title">Minor</h4>

                                        @foreach(json_decode($value->minor) as $minor_value)
                                            @if(is_array($minor_value))
                                            @foreach( $minor_value as  $list_value )
                                                <p class="minorin tick">   <i class="fa fa-check"></i>   {{ $list_value }}   </p>
                                            @endforeach
                                             @else
                                                <p class="minorin tick">   <i class="fa fa-check"></i>    {{$minor_value}}   </p>
                                             @endif
                                        @endforeach

                                </div>
                                @endif
                            </div>

                        @endforeach
                    </div>
                <?php if($tutor['details']->tutor_type == 'PEER') { ?>
                    <div class="sub-section">

                        <div class="col-sm-6">
                            <h4 class="sub-title">High School</h4>
                            @foreach($tutor['high_school'] as $value)
                                <p>{{ $value->school_name  }}<br>{{ $value->school_city  }}<br>{{ $value->year  }}</p>
                                <br/>
                            @endforeach

                        </div>

                    </div>
                <?php } ?>
                    <div class="sub-section">
                        <h4 class="sub-title">Certificate</h4>
                        @foreach($tutor['certificates'] as $value)
                            <p class="certificate tick"><i class="fa fa-check"></i>{{ $value->certificate }}</p>
                        @endforeach
                    </div>



                </div>
            </section>

            <!-- Panel Section -->
            <section class="subject-tests main-sub-sections">
                <div class="sub-inner-wrapper">
                    <div class="title">
                        <h3>Subject Tests</h3>
                        <div class="section-icons">
                            <ul>
                                <li><i class="fa fa-plus"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sub-tests">

                        <!-- Test Result sections -->
                        @foreach($tutor['subjects_tested'] as $value)
                            @if($value->is_tested == 1)
                                <div class="test-result-panel">
                                    <h4 class="pull-left">{{ $value->subject_name }}</h4>
                                    <div class="pull-right">
                                        <span class="test-status passed"><i class="fa fa-check-circle"></i> Passed</span>
                                        <a ><i class="fa fa-trash" onclick="delete_subject('{{ $value->subject_id}}','{{csrf_token()}}')"></i></a>
                                    </div>
                                </div>
                            @else
                                <div class="test-result-panel fail">
                                    <h4 class="pull-left">{{ $value->subject_name }}</h4>
                                    <div class="pull-right">
                                        <span class="test-status">Not tested Yet</span>
                                        <a><i id="{{ $value->subject_id }}" class="fa fa-trash" onclick="delete_subject('{{ $value->subject_id}}','{{csrf_token()}}')"></i></a>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </section>
            <!-- Panel Section -->
            <section class="tutor-experiance main-sub-sections">


                <div class="sub-section">
                    <h4 class="sub-title">Tutoring Experience</h4>

                    <p class="certificate "><i class="fa"></i>{{ $tutor['details']->experience }} year(s)</p>

                </div>
            </section>
            <!-- Panel Section -->
        </div>
    </div>
</section>
    <script>

        function delete_subject(subject_id, token){

            var tutor_id = '{{ Route::getCurrentRoute()->getParameter('id') }}';
//            var subject_id = id;
            $(this).parents().eq(1);

          //  var token =  $("input[name=_token]").val();

            if(confirm("Are you sure want to delete this Subject ?")) {
                $.ajax({
                    url: 'delete_tutor_subject_from_profile',
                    type: "post",
                    dataType: "json",
                    data: {'subject_id': subject_id, 'tutor_id': tutor_id, '_token': token},
                    success: function (response) {
                        location.reload();
                    }
                });
            } else {
                return false;
            }

        }
    </script>

@stop