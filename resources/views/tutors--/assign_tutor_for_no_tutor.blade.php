@extends('layouts.master')

@section('title', 'All Sessions - TutorSync')

@section('content')
<script src="{{ URL::asset('assets/libs/zabuto_calendar.min.js') }}" type="text/javascript"></script>


<link href="{{ URL::asset('assets/css/zabuto_calendar.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('assets/css/assign-new-tutor.css') }}" rel="stylesheet" type="text/css"/>

    <section class="body-content container">
        <!-- Content Holder -->
        <div class="inner-wrapper">

            {!! Form::open(array('action' => 'TutorsController@find', 'class' => 'form-inline','role'=>'form')) !!}



            {!! Form::hidden('subject',$sessions_details->subject_id) !!}
            {!! Form::hidden('categories',$sessions_details->category_id) !!}
            {!! Form::hidden('studentGrade',$sessions_details->grade_id) !!}
            {!! Form::hidden('gender',$sessions_details->tutor_gender) !!}
            {!! Form::hidden('tutor_type',$sessions_details->tutor_type) !!}
            {!! Form::hidden('student_email',$sessions_details->student_email) !!}
            {!! Form::hidden('student_id',$sessions_details->student_id) !!}
            {!! Form::hidden('session_date') !!}
            {!! Form::hidden('session_duration',$sessions_details->session_duration) !!}
            {!! Form::hidden('begin_time') !!}
            {!! Form::hidden('session_id',$sessions_details->session_id) !!}

            {!! Form::hidden('available_tutors_only','no') !!}
            {!! Form::hidden('all_tutors_list') !!}



            <div class="inner-wrapper">
            <div class="filter-form pick-tutor col-sm-12">
                <form class="form-inline" role="form">

                    <section class="payment-summery main-sub-sections summery-holder inner-wrapper">
                        <div class="col-sm-3">
                            <div class="label-wrappe">
                                <p>Summery</p>
                            </div>
                        </div>
                        <div class="col-sm-4 summmry-info">
                            <div class="std-name block-raw">
                                <p class="summery-title">Student Name</p>
                                <p class="summery-data"><?php echo $sessions_details->first_name.' '.$sessions_details->last_name;  ?></p>
                            </div>
                            <div class="session-date block-raw">
                                <p class="summery-title">Category</p>
                                <p class="summery-data"><?php echo $sessions_details->category_name; ?></p>
                            </div>
                            <div class="session-date block-raw">
                                <p class="summery-title">Session Date</p>
                                <p class="summery-data"><?php echo $sessions_details->date; ?></p>
                            </div>
                            <div class="location block-raw">
                                <p class="summery-title">Location</p>
                                <p class="summery-data"><?php echo $sessions_details->location;  ?></p>
                            </div>
                        </div>
                        <div class="col-sm-4 summmry-info">
                            <div class="tut-name block-raw">
                                <p class="summery-title">Student Email</p>
                                <p class="summery-data"><?php echo $sessions_details->email;  ?></p>
                            </div>
                            <div class="tut-name block-raw">
                                <p class="summery-title">Grade</p>
                                <p class="summery-data"><?php echo $sessions_details->grade;  ?></p>
                            </div>
                            <div class="session-time block-raw">
                                <p class="summery-title">Session Time</p>
                                <p class="summery-data"><?php echo $sessions_details->show_time;  ?></p>
                            </div>
                            <div class="tut-type block-raw">
                                <p class="summery-title">Tutor Type</p>
                                <p class="summery-data"><?php echo $sessions_details->tutor_type;  ?></p>
                            </div>
                        </div>
                    </section>





                        <!-- step_common_wrapper -->
                    </div>

                    <div data-ng-show="showschedule" class="package-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 student_step_common_wrapper">
                                    <p>Pick a Date</p>
                                </div>
                                <!-- student_step_common_wrapper -->
                            </div>
                        </div>
                        <!-- student_step_common_wrapper -->
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 step_common_wrapper">
                            <div class="row">
                                <!-- <div ui-calendar="uiConfig.calendar" data-ng-model="eventSources" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 st_step_4_hours">
                                     </div>-->
                                <div id="eventCalendarDefault"></div>
                                <!-- step_1_common_wrapper -->
                            </div>
                        </div>
                        <!-- step_common_wrapper -->
                        <!-- Choose -->

                    </div>
                    <div data-ng-show="showschedule" class="package-option-holder col-sm-12">
                        <div class="gender-select-title col-sm-2">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 student_step_common_wrapper">
                                    <p>Pick a Start Time</p>
                                </div>
                                <!-- student_step_common_wrapper -->
                            </div>
                        </div>
                        <!-- student_step_common_wrapper -->
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 step_common_wrapper">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 add_more_step">
                                            <div class="row">
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_10" class="select_time_button" onclick="select_start_time(10,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 10:00 AM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_11" class="select_time_button" onclick="select_start_time(11,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 11:00 AM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_12" class="select_time_button" onclick="select_start_time(12,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 12:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_13" class="select_time_button" onclick="select_start_time(13,this)">
                                                        <h6 class="subject_close_click"><i class="fa"></i>&nbsp; 1:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_14" class="select_time_button" onclick="select_start_time(14,this)">
                                                        <h6 class="subject_close_click" ><i class="fa"></i>&nbsp; 2:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_15" class="select_time_button" onclick="select_start_time(15,this)">
                                                        <h6 class="subject_close_click" ><i class="fa" ></i>&nbsp; 3:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_16" class="select_time_button"  onclick="select_start_time(16,this)">
                                                        <h6 class="subject_close_click" ><i class="fa" ></i>&nbsp; 4:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_17" class="select_time_button"  onclick="select_start_time(17,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 5:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_18" class="select_time_button"  onclick="select_start_time(18,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 6:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_19" class="select_time_button" onclick="select_start_time(19,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 7:00 PM</h6>
                                                    </div>
                                                </div>
                                                <!--select_college_button  -->
                                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 ">
                                                    <div id="slot_20" class="select_time_button" onclick="select_start_time(20,this)">
                                                        <h6 class="subject_close_click"><i class="fa" ></i>&nbsp; 8:00 PM</h6>
                                                    </div>
                                                </div>
                                                <div class="tutors-list col-sm-10">
                                                    <div class="opt-holder">
                                                        <input type="radio" name="available-tutors" id="all-tutors" checked="checked" onclick="show_available_tutors_only('no')">
                                                        <label class="radio-inline" for="all-tutors">Show All Tutors</label>
                                                    </div>
                                                    <div class="opt-holder">
                                                        <input type="radio" name="available-tutors" id="available-tutors"  onclick="show_available_tutors_only('yes')">
                                                        <label class="radio-inline" for="available-tutors">Show Available Tutors Only</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
            {!! Form::close() !!}
            <!-- Tutor Results -->
            <div class="tutor-result-holder col-sm-12" id="generated_tutor_result">

            </div>
            </div>

            <!-- Tutor Results -->

        </div>
    </section>

    <script type="text/javascript">


        $( document ).ready(function() {
            var calendar_subject = $('input[name="subject"]').val();
            var calendar_gender = $('input[name="gender"]').val();
            var calendar_tutor_type = $('input[name="tutor_type"]').val();

            var calendar_email = $('input[name="student_email"]').val();
            $("#eventCalendarDefault").zabuto_calendar({


                ajax: {

                    url: "{{ url('tutors/get_available_tutors_for_calendar')}}"//?subject="+calendar_subject+"&gender="+calendar_gender+"&tutor_type="+calendar_tutor_type+"&studentEmail="+calendar_email+') }}"

                }
            });
            get_selected_tutors();

        });


    function select_session_duration(selected_duration,element) {
        $('input[name="session_duration"]').val(selected_duration);
        $('.st_hours').removeClass('student_hours_4');
        $(element).addClass("student_hours_4");
        var token =  $("input[name=_token]").val();
        if($('input[name="session_date"]').val() != '') {
            get_selected_tutors();
            var all_tutor_list = $('input[name="all_tutors_list"]').val();

            if($('input[name="session_duration"]').val() != '' && $('input[name="session_duration"]').val() != 0) {
                if(all_tutor_list != '') {
                    $.ajax({
                        url: "{{ url('tutors/get_available_time_slots') }}",
                        type: "get",
                        dataType: "json",
                        data: {'tutor_list':all_tutor_list,'session_date':$('input[name="session_date"]').val() ,'session_duration':$('input[name="session_duration"]').val(), '_token': token},
                        success: function(response){
                            $(".select_time_button").removeClass("available_checked_time");
                            $(".select_time_button").removeClass("checked_time");
                            $.each(response, function(key, value) {
                                $('#slot_'+value).addClass('available_checked_time');
                            })
                        }
                    });
                }
            }
        }
    }
    function select_start_time(selected_begin_time, element) {
        $('input[name="begin_time"]').val(selected_begin_time);
        $('.select_time_button').removeClass('checked_time');
        $(element).addClass("checked_time");
        if($('input[name="session_date"]').val() != '' && $('input[name="session_duration"]').val() != '') {
            get_selected_tutors();

        }
    }
    function get_date_selected(year_selected, month_selected, date_delected,element) {
        var token =  $("input[name=_token]").val();
        $(".selected_date_for_session").removeClass("selected_date_for_session");
        $('#'+element).addClass('selected_date_for_session');
        var set_session_date = year_selected+'-'+(month_selected+1)+'-'+date_delected;
        var currentTime = new Date();
        var show_result = 'no';
        if(currentTime.getFullYear() <= year_selected) {
            if(currentTime.getFullYear() == year_selected) {
                if(currentTime.getMonth() <= month_selected) {
                    if(currentTime.getMonth() == month_selected) {
                        if(currentTime.getDate() <= date_delected) {
                            $('input[name="session_date"]').val(set_session_date);
                            show_result = 'yes';
                        }
                    } else {
                            $('input[name="session_date"]').val(set_session_date);
                            show_result = 'yes';
                    }
                }

            } else {
                $('input[name="session_date"]').val(set_session_date);

                show_result = 'yes';
            }
        }
        if(show_result == 'yes') {
            get_selected_tutors();

            var all_tutor_list = $('input[name="all_tutors_list"]').val();

            if($('input[name="session_duration"]').val() != '' && $('input[name="session_duration"]').val() != 0) {
                if(all_tutor_list != '') {
                    $.ajax({
                        url: "{{ url('tutors/get_available_time_slots') }}",
                        type: "get",
                        dataType: "json",
                        data: {'tutor_list':all_tutor_list,'session_date':set_session_date ,'session_duration':$('input[name="session_duration"]').val(), '_token': token},
                        success: function(response){
                            $(".select_time_button").removeClass("available_checked_time");
                            $(".select_time_button").removeClass("checked_time");
                            $.each(response, function(key, value) {
                                $('#slot_'+value).addClass('available_checked_time');
                            })
                        }
                    });
                }
            }
        }

    }
    function show_available_tutors_only(show_available_tutors_only) {
        if(show_available_tutors_only == 'yes') {
            $('input[name="available_tutors_only"]').val('yes');
        } else {
            $('input[name="available_tutors_only"]').val('no');
        }
        get_selected_tutors();
    }
    function get_selected_tutors() {

        var token =  $("input[name=_token]").val();
        var data_array = [];

        data_array['subject'] = $('input[name="subject"]').val();
        data_array['gender'] = $('input[name="gender"]').val();
        data_array['tutor_type'] = $('input[name="tutor_type"]').val();
        data_array['student_id'] = $('input[name="student_id"]').val();
        data_array['studentGrade'] = $('input[name="studentGrade"]').val();
        data_array['categories'] = $('input[name="categories"]').val();
        data_array['studentEmail'] = $('input[name="student_email"]').val();
        data_array['sessionDate'] = $('input[name="session_date"]').val();
        data_array['session_duration'] = $('input[name="session_duration"]').val();
        data_array['begin_time'] = $('input[name="begin_time"]').val();
        data_array['available_tutors_only'] = $('input[name="available_tutors_only"]').val();

        var all_tutors_list = "";
        $('#generated_tutor_result').html('');
        if(data_array['subject'] != '') {
            $.ajax({
                url: "{{ url('tutors/find') }}",
                type: "post",
                dataType: "json",
                data: {'subject':data_array['subject'],'gender':data_array['gender'] ,'tutor_type':data_array['tutor_type'],'studentGrade':data_array['studentGrade'],'categories':data_array['categories'],'studentEmail':data_array['studentEmail'],'sessionDate':data_array['sessionDate'],'session_duration':data_array['session_duration'],'begin_time':data_array['begin_time'],'available_tutors_only':data_array['available_tutors_only'],'student_id':data_array['student_id'], '_token': token},
                success: function(response){
                    $('#generated_tutor_result').html(response.result);
                    all_tutors_list = response.all_tutor_list;
                    $('input[name="all_tutors_list"]').val(all_tutors_list);

                }
            });
        }


    }
        function tutor_booking_cofirm(tutor_id, is_available) {

            if (tutor_id == '' || is_available == '0' || $('input[name="session_date"]').val() == '' || $('input[name="session_duration"]').val() == '' || $('input[name="begin_time"]').val() == '') {
                alert("Please select date, duration and start time");
                return false;
            } else {

                var session_id = $('input[name="session_id"]').val();
                var session_date = $('input[name="session_date"]').val();
                var session_duration = $('input[name="session_duration"]').val();
                var begin_time = $('input[name="begin_time"]').val();
                var token =  $("input[name=_token]").val();
                var tutor_id =  tutor_id;


                $.ajax({
                    url: "{{ url('tutors/tutor_booking_update') }}",
                    type: "post",
//                    dataType: "json",
                    data: {
                        'session_id': session_id,
                        'session_date':session_date,
                        'session_duration':session_duration,
                        'begin_time': begin_time,
                        'tutor_id': tutor_id,
                        '_token': token
                    },
                    success: function (response) {
                        console.log(response);

                        var redirect_url = '{{ url('sessions/all_sessions') }}';
                        $(location).attr('href',redirect_url)


                    }
                });




            }
            function myNavFunction(id) {
                $(".select_time_button").removeClass("available_checked_time");
                $(".select_time_button").removeClass("checked_time");

            }
        }

    </script>


@stop
