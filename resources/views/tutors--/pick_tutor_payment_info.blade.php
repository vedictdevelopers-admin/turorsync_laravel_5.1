@extends('layouts.master')

@section('title', 'All Tutors')

@section('content')



    <link href="{{ URL::asset('assets/css/assign-new-tutor.css') }}" rel="stylesheet" type="text/css"/>


    <!-- Body Content -->
    <section class="body-content container">
        <div class="inner-wrapper">
        <!-- Content Holder -->
        <!-- Panel Section -->
        <div class="pop-up-holder col-sm-12">
            <section class="payment-info main-sub-sections inner-wrapper">
                <div class="sub-inner-wrapper">
                    <div class="title">
                        <h3>Payment Information</h3>
                        <div class="section-icons">
                            <i class="fa fa-times"></i>
                        </div>
                    </div>
                </div>
            </section>
            <section class="payment-summery main-sub-sections summery-holder inner-wrapper">
                <div class="col-sm-3">
                    <div class="label-wrappe">
                        <p>Summery</p>
                    </div>
                </div>
                <div class="col-sm-4 summmry-info">
                    <div class="std-name block-raw">
                        <p class="summery-title">Student Name</p>
                        <p class="summery-data"><?php echo $pick_tutor_payment["student_details"]->first_name.' '.$pick_tutor_payment["student_details"]->last_name;  ?></p>
                    </div>
                    <div class="session-date block-raw">
                        <p class="summery-title">Session Date</p>
                        <p class="summery-data"><?php echo $pick_tutor_payment["session_date"]; ?></p>
                    </div>
                    <div class="location block-raw">
                        <p class="summery-title">Location</p>
                        <p class="summery-data"><?php echo $pick_tutor_payment["student_details"]->street.', '.$pick_tutor_payment["student_details"]->city.', '.$pick_tutor_payment["student_details"]->state.', '.$pick_tutor_payment["student_details"]->zip;  ?></p>
                    </div>
                </div>
                <div class="col-sm-4 summmry-info">
                    <div class="tut-name block-raw">
                        <p class="summery-title">Tutor Name</p>
                        <p class="summery-data"><?php echo $pick_tutor_payment["tutor_name"]->first_name.' '.$pick_tutor_payment["tutor_name"]->last_name;  ?></p>
                    </div>
                    <div class="session-time block-raw">
                        <p class="summery-title">Session Time</p>
                        <p class="summery-data"><?php echo $pick_tutor_payment['session_start'].' - '.$pick_tutor_payment['session_end']; ?></p>
                    </div>
                    <div class="tut-type block-raw">
                        <p class="summery-title">Tutor Type</p>
                        <p class="summery-data"><?php echo $pick_tutor_payment['tutor_type'] ?> Tutor - <?php echo $pick_tutor_payment['tutor_gender']; ?></p>
                    </div>
                </div>
            </section>

         <?php   if($pick_tutor_payment['tutor_type'] == 'PRO' && $pick_tutor_payment['session_duration'] <= $pick_tutor_payment['available_remaining_hours']) { ?>
                {!!Form::open(array('action' => 'TutorsController@pick_tutors_no_charge','id' => 'form','role'=>'form'))!!}
            <?php } else { ?>
            {!!Form::open(array('action' => 'TutorsController@pick_tutors_charge','id' => 'form','role'=>'form'))!!}
<?php } ?>
            {!! Form::hidden('package_id',$pick_tutor_payment['package_id'] ) !!}
            <section class="payment-details main-sub-sections inner-wrapper">
                <?php
                    $amount_to_be_paid = 0;
                if($pick_tutor_payment['tutor_type'] == 'PRO') {
                    if($pick_tutor_payment['package_selected'] == 1) {
                        if($pick_tutor_payment['available_remaining_hours'] > 0 && $pick_tutor_payment['session_duration'] > $pick_tutor_payment['available_remaining_hours']) {
                            $amount_to_be_paid = $pick_tutor_payment['package_hour_fee'] * 10;
                        }else if($pick_tutor_payment['available_remaining_hours'] > 0 && $pick_tutor_payment['session_duration'] <= $pick_tutor_payment['available_remaining_hours']) {
                            $amount_to_be_paid = 0;
                        } else {
                            $amount_to_be_paid = $pick_tutor_payment['package_hour_fee'] * 10;
                        }
                    } else {
                        $amount_to_be_paid = $pick_tutor_payment['pro_hour_fee'] * $pick_tutor_payment['session_duration'];
                    }
                } else {
                    $amount_to_be_paid = $pick_tutor_payment['peer_hour_fee'] * $pick_tutor_payment['session_duration'];
                }
                ?>

                {!!  Form::hidden('amount', $amount_to_be_paid) !!}
                {!!  Form::hidden('tutor_id', $pick_tutor_payment['tutor_id']) !!}
                {!!  Form::hidden('is_package', $pick_tutor_payment['package_selected']) !!}
                {!!  Form::hidden('available_hours', $pick_tutor_payment['available_remaining_hours']) !!}

                <?php
                if($pick_tutor_payment['tutor_type'] == 'PRO') {
                    if($pick_tutor_payment['available_remaining_hours'] > 0 && $pick_tutor_payment['session_duration'] > $pick_tutor_payment['available_remaining_hours']) {
                ?>
                <div class="time-price alert alert-danger">
                    <div class="tut-time col-sm-4">
                        <p class="tut-type">Pro Tutor</p>
                        <p class="session-duration"><?php echo $pick_tutor_payment['session_duration']; ?> Hour Session</p>
                    </div>
                    <div class="amount col-sm-8">

                        <div class="time-remaining"><?php echo ($pick_tutor_payment['session_duration']-$pick_tutor_payment['available_remaining_hours']); ?></div>
                    </div>
                </div>

                <div class="select-package">
                    <p class="not-enough-time">You don't have enough hours left in your package.</p>
                    <p class="select-package-text">Select package to continue</p>
                </div>
                <?php } else if($pick_tutor_payment['available_remaining_hours'] > 0 && $pick_tutor_payment['session_duration'] <= $pick_tutor_payment['available_remaining_hours']) { ?>
                        <div class="time-price">
                            <div class="tut-time col-sm-4">
                                <p class="tut-type">Pro Tutor</p>
                                <p class="session-duration"><?php echo $pick_tutor_payment['session_duration']; ?> Hour Session</p>
                            </div>
                            <div class="amount col-sm-8">
                                <div class="time-remaining"><?php echo $pick_tutor_payment['available_remaining_hours']; ?> </div>
                            </div>
                        </div>
                <?php    } else { ?>
                        <div class="time-price">
                            <div class="tut-time col-sm-4">
                                <p class="tut-type">Pro Tutor</p>
                                <p class="session-duration"><?php echo $pick_tutor_payment['session_duration']; ?> Hour Session</p>
                            </div>

                        </div>
                <?php    } } else { ?>
                    <div class="time-price">
                        <div class="tut-time col-sm-4">
                            <p class="tut-type">Peer Tutor</p>
                            <p class="session-duration"><?php echo $pick_tutor_payment['session_duration']; ?> Hour Session</p>
                        </div>

                    </div>
                <?php } if($pick_tutor_payment['tutor_type'] == 'PRO' && $pick_tutor_payment['session_duration'] <= $pick_tutor_payment['available_remaining_hours']) { } else{ ?>
                <div class="package-options">
                    <?php if($pick_tutor_payment['tutor_type'] == 'PRO') {?>
                    <div class="package-opt <?php if($pick_tutor_payment['package_selected'] == 1){ echo "active";} ?> col-sm-6" onclick="package_selection('1',this)">
                        <div class="inner-wrapper">
                            <p class="package-opt-title">10 Hour Package</p>
                            <div class="price-per-hour">
                                <p class="price">$<?php echo $pick_tutor_payment['package_hour_fee']; ?></p>
                                <p class="per-hour-text">Per Hour</p>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="package-opt col-sm-6 <?php if($pick_tutor_payment['tutor_type'] == 'PEER' || $pick_tutor_payment['package_selected'] == 0) { echo "active"; } ?>"  onclick="package_selection('0',this)">
                        <div class="inner-wrapper">
                            <p class="package-opt-title">Hourly</p>
                            <div class="price-per-hour">
                                <p class="price">$<?php if($pick_tutor_payment['tutor_type'] == 'PEER') { echo $pick_tutor_payment['peer_hour_fee'];  } else if($pick_tutor_payment['tutor_type'] == 'PRO') {echo $pick_tutor_payment['pro_hour_fee'];  } ?></p>
                                <p class="per-hour-text">Per Hour</p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </section>

            <section class="payment-controllers main-sub-sections inner-wrapper">
                <?php if($pick_tutor_payment['tutor_type'] == 'PRO' && $pick_tutor_payment['session_duration'] <= $pick_tutor_payment['available_remaining_hours']) { } else { ?>
                <!-- Payment method preview -->
                <div class="card-preview col-sm-8">
                    <div class="title">
                        <h3>Payment</h3>
                        <div class="section-icons">
                            <i class="fa fa-edit"></i>
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="card-data">
                        <p class="card-id">xxxx-xxxx-xxxx-{{$pick_tutor_payment["payment_info"]->card_no}}</p>
                        <p class="card-type">{{$pick_tutor_payment['card_type']}}</p>
                    </div>
                </div>
                <?php } ?>
                <!-- Sessions controlers -->
                <div class="session-btns payment-controllers-holder">
                    <a href="{{ url('tutors/clear_booking_sessions') }}" class="cancel-btn btn btn-default">Cancel</a>
                    {{--<a href="#" class="save-btn btn btn-default">Book Appointment</a>--}}
                    {!! Form::submit('Book Appointment',array('type'=>'submit','class'=>'save-btn btn btn-default')) !!}
                </div>

            </section>

            {!!Form::close()!!}


        </div>
            </div>
    </section>

<script type="text/javascript">
    var peer_houly_rate = '<?php if(isset($_POST['peer_hour_fee'])) { echo $_POST['peer_hour_fee']; } ?>';
    var pro_houly_rate = '<?php if(isset($_POST['pro_hour_fee'])) { echo $_POST['pro_hour_fee']; } ?>';

        function package_selection(is_package,element) {
            if (!$(element).hasClass('active')) {

                $('.package-opt').removeClass('active');
                $(element).addClass("active");

                if(is_package == '1') {

                    $('input[name="is_package"]').val(1);
                    $('input[name="amount"]').val(<?php echo $pick_tutor_payment['package_hour_fee'] * 10; ?>);
                } else {
                    $('input[name="is_package"]').val(0);
                    <?php if($pick_tutor_payment['tutor_type'] == 'PRO') {?>

                    $('input[name="amount"]').val(<?php echo ($pick_tutor_payment['pro_hour_fee'] * $pick_tutor_payment['session_duration']); ?>);
                    <?php } ?>
                }

            }
        }

    </script>








@stop