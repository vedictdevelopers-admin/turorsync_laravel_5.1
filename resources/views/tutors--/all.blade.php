@extends('layouts.master')

@section('title', 'All Tutors')

@section('content')

    {{--pagination scripts--}}

    <link href="{{ URL::asset('assets/tbl/footable.core.css?v=2-0-1') }}" rel="stylesheet" type="text/css"/>
    <script>
        if (!window.jQuery) { document.write('<script src="js/jquery-1.9.1.min.js"><\/script>'); }
    </script>
    <script src="{{ URL::asset('assets/tbl/footable.js?v=2-0-1') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('assets/tbl/footable.paginate.js?v=2-0-1') }} " type="text/javascript"></script>
    <script src="{{ URL::asset('assets/tbl/bootstrap-tab.js') }}" type="text/javascript"></script>
    {{--end pagination scripts--}}


    <!-- Body Content -->
    <section class="body-content container">
        <!-- Tutor tabs -->
        <section class="tutor-tabs">
            <ul class="nav nav-pills nav-justified">
                <li>
                    <a href="{{ url('tutors/new_applications') }}">New Applications</a>
                </li>
                <li  class="active">
                    <a href="{{ url('tutors') }}">All Tutors</a>
                </li>
                <li><a href="{{ url('tutors/deactivated_tutors') }}">Deactivated Tutors</a></li>
            </ul>
        </section>
        <!-- Filter -->
        <section class="filter-holder">


            {!!Form::open(array('action' => 'TutorsController@filter_all_tutors_by_category','id' => 'filter_all_tutors_by_category', 'class' => 'form-inline','role'=>'form'))!!}

            <div class="form-group">
                <div class="top-block">
                    {!! Form::label('tutorName', ' Tutor Name', array('for' => 'tutorName'))!!}
                    {!! Form::text('tutorName',Input::get("tutorName"), array('class' => 'form-control', 'id'=>'tutorName','placeholder'=>''))!!}

                </div>
                <div class="bottom-block">
                    {!! Form::label('tutorType', ' Tutor Type', array('for' => 'tutorType'))!!}
                    {!! Form::select('tutorType', array('' => 'ALL','PRO' => 'PRO','PEER' => 'PEER'),  Input::get('tutorType'), ['class' => 'form-control','id'=>'tutorType']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="top-block">

                    {!! Form::label('subjectName', ' Subject Name', array('for' => 'subjectName'))!!}
                    {!! Form::text('subjectName',Input::get("subjectName"), array('class' => 'form-control', 'id'=>'subjectName'))!!}

                </div>
                <div class="bottom-block">

                    {!! Form::label('location', ' Location', array('for' => 'location'))!!}
                    {!! Form::text('location',Input::get("location"), array('class' => 'form-control', 'id'=>'location','placeholder'=>''))!!}

                    <span class="fa fa-map-marker"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="top-block">

                    {!! Form::label('status', ' Status', array('for' => 'status'))!!}
                    {!! Form::select('status',array('' => 'All','0' => 'Pending', '1' => 'Approved', '2' => 'Deactivated','3' => 'Rejected'), Input::get('status'), ['class' => 'form-control','id'=>'status']) !!}
                    {!! Form::hidden('sort_method', Input::get("sort_method"), array('id'=>'sort_method')) !!}
                    {!! Form::hidden('sort_value', Input::get("sort_value"), array('id'=>'sort_value')) !!}
                </div>
                <div class="bottom-block">
                    {{--<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span> Filter</button>--}}
                    {!! Form::button('filter',array('type'=>'submit','class'=>'btn btn-default')) !!}
                </div>
            </div>
            {!!Form::close()!!}
        </section>


        {{--list messages--}}
        <div class="msg_type_success messages success_msg">
          <!--  <p>The Status has been updated.</p> -->
        </div>
        <div class="success_msg_append "> </div>

        <!-- Tutor Table Container -->
        <div class="tutor-table table-responsive">
            <table class="table table-condensed table demo sort-caret" data-page-size="10">
                <thead>
                <tr>
                    <th style="cursor:pointer" id="sort_name" value="sort_name" class="active-sort asc">Tutor Name <span class="sort-icon">&#9650;</span></th>
                    <th style="cursor:pointer" id="sort_type" value="sort_type">Tutor Type</th>
                    <th style="cursor:pointer" id="sort_location" value="sort_location">Location</th>
                    <th id="sort_subject" value="sort_subject">Subjects</th>
                    <th style="cursor:pointer" id="sort_status" value="sort_status">Status</th>
                    <th style="cursor:pointer" id="sort_date" value="sort_date">Date Created</th>
                    <th style="cursor:pointer" id="sort_willing_to_travel" value="sort_willing_to_travel">Willing to travel</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($tutors)) { ?>
                @foreach ($tutors as $tutor)

                    <tr id="tbl_row_{{ $tutor['user_id'] }}">
                        <td>

                            <p><a href="{{ url('tutors/'. $tutor['tutor_id']) }}"> {{ $tutor['first_name'] }} {{ $tutor['last_name'] }}</a> </p>
                            <div class="star-rating">
                            @for($i=1; $i <= 5 ; $i++)
                                @if($i <= $tutor['rating'])
                                    <span class="fa fa-star fa-star-active" data-rating="1"></span>
                                    @else
                                    <span class="fa fa-star" data-rating="3"></span>
                                @endif
                            @endfor
                            </div>



                            <p><span class="numOfStudents">{{$tutor['rating_count']}}</span> Reviewed</p>
                        </td>

                        <td>
                            {{ $tutor['tutor_type'] }}
                        </td>

                        <td>
                            <p>{{ ltrim ( $tutor['street']  .",".  $tutor['city'] .",". $tutor['state'] , ',') }} </p>

                        </td>
                        <td>

                            @foreach ($tutor['subjects'] as $tutor_subj)
                                <p id="row_subtuid_{{$tutor['user_id']}}_{{  $tutor_subj['subject_id'] }}"> {{  $tutor_subj['subject_name'] }}
                                  <a  class="subject_action" data-token="{{ csrf_token() }}" id="subtuid_{{$tutor['user_id']}}_{{  $tutor_subj['subject_id'] }}" onclick="return deleteSubject(this);">
                                      <span class="glyphicon glyphicon-minus"></span>
                                  </a>
                                </p>
                            @endforeach

                        </td>
                        <td>
                            <select name="status" id="tutorStatus_{{ $tutor['user_id'] }}" data-token="{{ csrf_token() }}"  class="form-control tutorStatus"  onchange="return tutorStatus(this);">

                                <option value="0" @if($tutor['is_approved']==0) selected @endif>Pending</option>
                                <option value="1" @if($tutor['is_approved']==1) selected @endif>Approve</option>
                                <option value="3" @if($tutor['is_approved']==3) selected @endif>Reject</option>
                                <option value="2" @if($tutor['is_approved']==2) selected @endif>Deactivated</option>
                            </select>
                        </td>

                        <td>{{ date_format(date_create($tutor['created_at']),"d-m-Y") }}</td>
                        <td>{{ $tutor['miles_travel'] }} Miles</td>
                    </tr>

                @endforeach
                <?php } else { ?>
                    <tr><td colspan="7" style="text-align: center;">No result found</td></tr>
                <?php } ?>

                {{--<tr>--}}
                {{--<td colspan="7">--}}
                {{--{!!   $paginations->render() !!}--}}
                {{----}}

                {{--<button class="btn form-controler-btn pull-left">Previous</button>--}}
                {{--<button class="btn form-controler-btn pull-right">Next</button>--}}
                {{--</td>--}}
                {{--</tr>--}}
                </tbody>


                    <tfoot>
                    <tr>
                        <td colspan="7">

                            {!! $paginations->render() !!}
                        </td>
                    </tr>
                    </tfoot>

            </table>
        </div>
    </section>



    <style type="text/css">

        .pagination li {
            display: inline-block!important;
            margin: 0px 10px;
        }


    </style>

<script type="text/javascript">

    function tutorStatus(sel){

        var user_id = $(sel).attr('id').split('_');
        var stutus_value = $(sel).val();
        var _token = $(sel).data("token");

        var msg_text = "";
        if(stutus_value == 1) {
            msg_text = "Are you sure you want to approve this tutor";
        } else if(stutus_value == 2) {
            msg_text = "Are you sure you want to deactivate this tutor";
        } else if(stutus_value == 3) {
            msg_text = "Are you sure you want to reject this tutor";
        }

        if(confirm(msg_text)) {
            $.post('tutors/update_application_status', { user_id : user_id[1],stutus_value:stutus_value, _token : _token }, function(data) {

                $('#tbl_row_'+data['user_id']).fadeOut(1000);

                if(data['stutus_value'] ==1){

                    $('.success_msg_append').html("<div class='msg_type_success messages'><p>Tutor approved successfully</p></div>");
                    $('.success_msg_append').css('display','block');
                    $('.success_msg_append').fadeOut(3000);

                }else  if(data['stutus_value'] ==2){

                    $('.success_msg_append').html("<div class='msg_type_success_danger messages'><p>Tutor deactivated successfully</p></div>");
                    $('.success_msg_append').css('display','block');
                    $('.success_msg_append').fadeOut(3000);

                }else  if(data['stutus_value'] ==3){

                    $('.success_msg_append').html("<div class='msg_type_success_danger messages'><p>Tutor rejected successfully.</p></div>");
                    $('.success_msg_append').css('display','block');
                    $('.success_msg_append').fadeOut(3000);

                }
            });
        } else {
            return false;
        }
    }


    function deleteSubject(del){

        var subject_details = $(del).attr('id').split('_');
        var _token = $(del).data("token");


        if(confirm("Are you sure want to delete this Subject ?")) {
            $.post('tutors/delete_tutor_subject', { user_id : subject_details[1],subject_id : subject_details[2], _token : _token }, function(data) {

                $('#row_subtuid_'+data['user_id']+"_"+data['subject_id']).fadeOut(1000);

                $('.success_msg').css('display','block');
                $('.success_msg').text("Delete has been Success.");
                $('.success_msg').fadeOut(3000);
            });
        } else {
            return false;
        }

    }

    $(document).ready(function(){
        $( 'th' ).on( 'click', function() {

            if($(this).attr('id') == 'sort_subject'){
                return false;
            }

            class_name = $( this ).attr('class');
            if(typeof class_name === 'undefined'){

                if ( $( '.table th' ).hasClass('active-sort') ) {
                    $('#sort_method').val('asc');
                    $("#sort_value").val($(this).attr('value'));
                    $('#filter_all_tutors_by_category').submit();
                }

            } else {

                if ( $( '.table th' ).hasClass('asc') ) {
                    $('#sort_method').val('desc');
                    $("#sort_value").val($(this).attr('value'));
                    $('#filter_all_tutors_by_category').submit();
                } else if( $( '.table th' ).hasClass('desc') ){
                    $('#sort_method').val('asc');
                    $("#sort_value").val($(this).attr('value'));
                    $('#filter_all_tutors_by_category').submit();
                }

            }
        });
    });

    if($('#sort_method').val() == 'asc'){

        sort_value = $('#sort_value').val()
        $( '.table th' ).removeAttr('class');
        $( '.table th span' ).remove();
        $('#'+sort_value).attr('class', 'active-sort asc');
        $('#'+sort_value).append(' <span class="sort-icon">&#9650;</span>')

    } else if($('#sort_method').val() == 'desc'){

        sort_value = $('#sort_value').val()
        $( '.table th' ).removeAttr('class');
        $( '.table th span' ).remove();
        $('#'+sort_value).attr('class', 'active-sort desc');
        $('#'+sort_value).append(' <span class="sort-icon">&#9660;</span>');

    }


</script>




@stop