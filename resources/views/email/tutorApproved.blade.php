
   <div style="box-sizing: border-box;font-family: &quot;Montserrat&quot;, sans-serif;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;width: 100% !important">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0" style="box-sizing: border-box;border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: middle;text-align: left">
            <tr sclass="email-top-header-banner" style="box-sizing: border-box;padding: 0;vertical-align: middle;text-align: left">
                <td colspan="2" style="box-sizing: border-box;word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: middle;text-align: left;border-collapse: collapse !important"><img style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: 100%;max-width: 100%;float: left;clear: both;display: block;box-sizing: border-box" src="https://s3tutorsyncmain.s3.amazonaws.com/production/email_templates/email-top-header-banner.jpg"/><!--/*pls use http url*/--></td>
			</tr>
            <tr class="email-body" style="box-sizing: border-box;padding: 40px 5%;vertical-align: middle;text-align: left;background: #f8f8f8 none repeat scroll 0 0;display: block">
                <td scolspan="2" style="box-sizing: border-box;word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: middle;text-align: left;border-collapse: collapse !important">
					<p class="title" style="box-sizing: border-box;line-height: 1.4em;color: #2a2a2a;font-size: 130%;font-family: &quot;Source Sans Pro&quot;, sans-serif;padding-bottom: 25px">Hi ,</p>
                    <p style="box-sizing: border-box;line-height: 1.4em;color: #8d8d8d;font-size: 130%;font-family: &quot;Source Sans Pro&quot;, sans-serif">Congratulations your tutor application has been approved.</p>
					<p style="box-sizing: border-box;line-height: 1.4em;color: #8d8d8d;font-size: 130%;font-family: &quot;Source Sans Pro&quot;, sans-serif">Please download our application to get started.</p>
					<p style="box-sizing: border-box;line-height: 1.4em;color: #8d8d8d;font-size: 130%;font-family: &quot;Source Sans Pro&quot;, sans-serif">
                        Thank you,<br style="box-sizing: border-box"/>The TutorSync Team<br style="box-sizing: border-box"/><br style="box-sizing: border-box"/>
                        <img alt="Tutorsync" width="152px" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;max-width: 100%;float: left;clear: both;display: block;box-sizing: border-box" src="https://s3tutorsyncmain.s3.amazonaws.com/production/email_templates/logo.png"/>
                    </p>
				</td>
			</tr>
            <tr id="footer" class="footer_row" style="box-sizing: border-box;padding: 40px 5%;vertical-align: middle;text-align: left;background: #eaeaea none repeat scroll 0 0;display: block">
                <td class="copy_right_text" style="box-sizing: border-box;word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: middle;text-align: left;width: 70%;float: left;border-collapse: collapse !important">
					<span style="box-sizing: border-box;color: #b4b4b7;font-size: 100%;margin: 0;padding-top: 5px">© 2015 TutorSync. All Rights Reserved</span>
				</td>
				<td class="nav_right" style="box-sizing: border-box;word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;padding: 0;vertical-align: middle;text-align: left;width: 30%;border-collapse: collapse !important">
					<ul class="footer_nav" style="box-sizing: border-box;text-align: right;margin: 0;padding: 0"><a href="" style="box-sizing: border-box"><img background-position="5px 0" src="https://s3tutorsyncmain.s3.amazonaws.com/production/email_templates/FBicon.jpg" style="box-sizing: border-box;border: none"/></a>
						<a href="" style="box-sizing: border-box"><img background-position="-62px 0" src="https://s3tutorsyncmain.s3.amazonaws.com/production/email_templates/TWicon.jpg" hspace="30px" style="box-sizing: border-box;border: none"/></a>
						<a href="" style="box-sizing: border-box"><img background-position="-130px 0" src="https://s3tutorsyncmain.s3.amazonaws.com/production/email_templates/Licon.jpg" style="box-sizing: border-box;border: none"/></a>   	                         
					</ul>
                </td>
			</tr>
        </table>
   </div>
