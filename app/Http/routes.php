<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'Auth\AuthController@getLogin');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');



Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::model('user_id', 'User');
Route::get('tutors', 'TutorsController@index');
Route::post('tutors', 'TutorsController@filter_all_tutors_by_category');
Route::get('tutors/new_applications', 'TutorsController@new_applications');
Route::post('tutors/new_applications', 'TutorsController@filter_all_new_application_by_category');
Route::get('tutors/deactivated_tutors', 'TutorsController@deactivated_tutors');
Route::post('tutors/deactivated_tutors', 'TutorsController@filter_deactivated_tutors_by_category');

Route::get('tutors/app_approved_email', 'TutorsController@application_approved_email');
Route::get('tutors/app_rejected_email', 'TutorsController@application_rejected_email');
Route::get('tutors/tutor_assigned_email', 'TutorsController@tutor_assigned_email');

Route::post('tutors/update_application_status', 'TutorsController@update_application_status');
Route::post('tutors/delete_tutor_subject', 'TutorsController@delete_tutor_subject');
Route::post('tutors/delete_tutor_subject_from_profile', 'TutorsController@delete_tutor_subject_from_profile');
Route::get('tutors/get_student_email' , 'TutorsController@getStudentEmail');
Route::get('tutors/get_tutor_name' , 'TutorsController@getTutorName');
Route::post('tutors/student_summery', 'TutorsController@student_summery');
Route::post('tutors/get_tutor_type_student_rates', 'TutorsController@get_tutor_type_student_rates');

Route::get('tutors/pick_tutor_add_card/{id}', 'TutorsController@pick_tutor_add_card');
Route::post('tutors/pick_tutor_add_card/{id}', 'TutorsController@pick_tutor_add_card_details');

Route::get('tutors/pick_tutor_payment_info/{id}', 'TutorsController@pick_tutor_payment_info');
Route::post('tutors/pick_tutors_charge', 'TutorsController@pick_tutors_charge');
Route::post('tutors/pick_tutors_no_charge', 'TutorsController@pick_tutors_no_charge');
Route::get('tutors/get_available_tutors_for_calendar', 'TutorsController@get_available_tutors_for_calendar');
Route::get('tutors/get_available_time_slots', 'TutorsController@get_available_time_slots');
Route::post('tutors/get_tutor_remaining_hours', 'TutorsController@get_tutor_remaining_hours');

Route::get('tutors/clear_booking_sessions', 'TutorsController@clear_booking_sessions');
Route::get('tutors/assign_tutor_for_no_tutor/{id}', 'TutorsController@assign_tutor_for_no_tutor');

Route::post('tutors/tutor_booking_update', 'TutorsController@tutor_booking_update');

Route::post('tutors/delete_tutor__by_tutor_id', 'TutorsController@delete_tutor__by_tutor_id');
Route::post('students/delete_student_by_student_id', 'StudentsController@delete_student_by_student_id');

Route::post('sessions/to_excel', 'SessionsController@to_excel');

Route::post('students/to_excel', 'StudentsController@to_excel');

Route::post('tutors/to_excel', 'TutorsController@to_excel');
Route::post('tutors/session_assign_info', 'TutorsController@session_assign_info');

Route::post('students/update_student_status', 'StudentsController@update_student_status');
//Route::get('tutors/pick_tutors_charge', 'TutorsController@pick_tutors_charge');
//Route::post('tutors/pick_tutors_charge', 'TutorsController@pick_tutors_charge');


/**
 *  CHANGES DONE BY SADAKA
 *
 */



Route::get('students', 'StudentsController@listing');
Route::post('students', 'StudentsController@filter_all_students');
Route::get('students/{id}', 'StudentsController@details');


/**
 * route for sessions/all_sessions
 */

Route::get('sessions/all_sessions', [
    'as' => 'allSessions', 'uses' => 'SessionsController@listing'
]);
Route::post('sessions/all_sessions', [
    'as' => 'allSessionsFilter', 'uses' => 'SessionsController@filter_all_sessions'
]);

/**
 * route for sessions/scheduled_sessions
 */
Route::get('sessions/scheduled_sessions', [
    'as' => 'scheduledSessions', 'uses' => 'SessionsController@scheduled_listing'
]);
Route::post('sessions/scheduled_sessions', [
    'as' => 'scheduledSessionsFilter', 'uses' => 'SessionsController@filter_Scheduled_sessions'
]);
Route::get('sessions/scheduled_sessions/{id}', [
    'as' => 'scheduledSessionsById', 'uses' => 'SessionsController@scheduled_sessions_by_id'
]);

/**
 * route for sessions/completed_sessions
 */
Route::get('sessions/completed_sessions', [
    'as' => 'completedSessions', 'uses' => 'SessionsController@completed_listing'
]);
Route::post('sessions/completed_sessions', [
    'as' => 'completedSessionsFilter', 'uses' => 'SessionsController@filter_Completed_sessions'
]);
Route::get('sessions/completed_sessions/{id}', [
    'as' => 'scheduledSessionsById', 'uses' => 'SessionsController@completed_sessions_by_id'
]);
/**
 * route for sessions/abandoned_sessions
 */
Route::get('sessions/abandoned_sessions', [
    'as' => 'abandonedSessions', 'uses' => 'SessionsController@abandoned_listing'
]);
Route::post('sessions/abandoned_sessions', [
    'as' => 'abandonedSessionsFilter', 'uses' => 'SessionsController@filter_Abandoned_sessions'
]);
Route::get('sessions/abandoned_sessions/{id}/{is_completed}', [
    'as' => 'abandonedSessionsById', 'uses' => 'SessionsController@abandoned_sessions_by_id'
]);
/**
 * route for sessions/cancelled_sessions
 */
Route::get('sessions/cancelled_sessions', [
    'as' => 'cancelledSessions', 'uses' => 'SessionsController@cancelled_listing'
]);
Route::post('sessions/cancelled_sessions', [
    'as' => 'cancelledSessionsFilter', 'uses' => 'SessionsController@filter_Cancelled_sessions'
]);
Route::get('sessions/cancelled_sessions/{id}', [
    'as' => 'scheduledSessionsById', 'uses' => 'SessionsController@cancelled_sessions_by_id'
]);

/**
 * route for sessions/declined_sessions
 */
Route::get('sessions/declined_sessions', [
    'as' => 'declinedSessions', 'uses' => 'SessionsController@declined_listing'
]);
Route::post('sessions/declined_sessions', [
    'as' => 'declinedSessionsFilter', 'uses' => 'SessionsController@filter_Declined_sessions'
]);
Route::get('sessions/declined_sessions/{id}', [
    'as' => 'scheduledSessionsById', 'uses' => 'SessionsController@declined_sessions_by_id'
]);
Route::get('sessions/cancel_session/{id}', 'SessionsController@cancell_session');
Route::get('sessions/not_assigned/{id}', [
    'as' => 'NotAssigned', 'uses' => 'SessionsController@not_assigned'
]);


Route::get('tutors/booking', [
    'as' => 'findNewTutor', 'uses' => 'TutorsController@booking'
]);
Route::post('tutors/categories_for_grade', 'TutorsController@categories_for_grade');
Route::post('tutors/categories_for_subject', 'TutorsController@categories_for_subject');
Route::post('tutors/find', 'TutorsController@find');


Route::get('tutors/update_booking/{id}', [
    'as' => 'findUpdateTutor', 'uses' => 'TutorsController@update_booking'
]);

Route::post('tutors/update_booking/update_booking_categories_for_grade', 'TutorsController@update_booking_categories_for_grade');
Route::post('tutors/update_booking/update_booking_categories_for_subject', 'TutorsController@update_booking_categories_for_subject');
Route::post('tutors/update_booking/update_booking_get_session_details_onload', 'TutorsController@update_booking_get_session_details_onload');



Route::get('tutors/{id}', 'TutorsController@details');
Route::post('tutors/storesession/{id}', 'TutorsController@storesession');

