<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class StudentsController extends Controller
{


    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function listing()
    {
        $url_param = $_GET;
        $students = DB::table('users')
                        ->join('students', 'students.user_id', '=', 'users.user_id')
                        ->join('address', 'users.user_id', '=', 'address.user_id');



        if(!empty($url_param)) {

            if(count($url_param) > 1) {
                if(!empty($url_param['student_name'])) {
                    $students->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                }

                if(!empty($url_param['location'])) {

                    $students->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'LIKE', "%".$url_param['location']."%");
                }
                if(!empty($url_param['email'])) {
                    $students->where(DB::raw('users.email'), 'LIKE', "%".$url_param['email']."%");
                }
            }
        }
        $students->select('users.user_id', 'users.first_name', 'users.last_name', 'users.phone', 'address.city','address.street','address.zip','address.state','users.email','users.created_at','students.respond'  );

        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $students = $students->paginate(50);
        } else {
            $students = $students->paginate(50);
        }
        
        $bulk_data = array();
        $i = 1;
        foreach($students as $student){
            $bulk_data[$i]['student_id'] = $student->user_id;
            $bulk_data[$i]['first_name'] = $student->first_name;
            $bulk_data[$i]['last_name'] = $student->last_name;
            $bulk_data[$i]['user_id'] = $student->user_id;
            $bulk_data[$i]['email'] = $student->email;
            $bulk_data[$i]['phone'] = $student->phone;
            $bulk_data[$i]['created_at'] = $student->created_at;
            $bulk_data[$i]['respond'] = $student->respond;

            $bulk_data[$i]['street'] = $student->street;
            $bulk_data[$i]['city'] = $student->city;
            $bulk_data[$i]['state'] = $student->state;

            $students_subjects = DB::table('sessions')->where('student_id', '=', $student->user_id)->count();

            $bulk_data[$i]['sessions'] = $students_subjects;
            $student_payments = DB::table('packages')
                ->join('payments', 'packages.package_id', '=', 'payments.package_id')
                ->where('student_id', '=', $student->user_id)
                ->where('is_charged', '=', 1)
                ->select(DB::raw('(total_hours*per_hour) as row_total'))
                ->groupBy('payments.package_id')
                ->get();
            if(!empty($student_payments)) {
                $full_total_student_payment = 0;
                foreach($student_payments as $each_payment) {
                    $full_total_student_payment += $each_payment->row_total;
                }
                $bulk_data[$i]['student_payment'] = '$'.$full_total_student_payment;
             } else {
                $bulk_data[$i]['student_payment'] = 'N/A';
            }
            $i++;
        }
        if(!empty($url_param) && count($url_param) > 1) {
            $students->setPath('?student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&email='.$url_param['email'].'&');
        } else {
            $students->setPath('');
        }


        return view('students.studentsList', ['bulk_data' => $bulk_data,'paginations'=>$students]);


    }

    public function filter_all_students(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $location       =   $request->input('location');
        $email       =      $request->input('email');
        $students = DB::table('users')
                    ->join('students', 'students.user_id', '=', 'users.user_id')
                    ->join('address', 'users.user_id', '=', 'address.user_id');
        if(!empty($student_name)) {
            $students->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), 'LIKE', "%".$student_name."%");
        }
        if(!empty($location)) {
            $students->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'LIKE', "%".$location."%");
        }
        if(!empty($email)) {
            $students->where(DB::raw('users.email'), 'LIKE', "%".$email."%");
        }
/*
        if(!empty($student_name) && !empty($location) ){
            $student_filter = DB::table('users')
                ->leftJoin('students', 'students.user_id', '=', 'users.user_id')
                ->leftJoin('address', 'users.user_id', '=', 'address.user_id')->where('users.first_name', 'like', '%'.$student_name.'%')
                                ->orWhere('users.last_name', 'like', '%'.$student_name.'%')
                                ->orWhere(DB::raw("CONCAT( users.first_name, ' ', users.last_name)"), 'LIKE', "%".$student_name."%");

            $students = DB::table('users')
                ->leftJoin('students', 'students.user_id', '=', 'users.user_id')
                ->leftJoin('address', 'users.user_id', '=', 'address.user_id')->orwhere('address.city', 'like',  '%'.$location.'%')
                                ->orWhere('address.street', 'like', '%'.$location.'%')
                                ->orWhere(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'LIKE', "%".$location."%");
//            echo'<pre>'; print_r($student_filter); echo'<br>'; print_r($students);exit;
//            )->union($student_filter)
        } else {

            if(!empty($student_name)) {
                $students->where('users.first_name', 'like', '%'.$student_name.'%')
                    ->orWhere('users.last_name', 'like', '%'.$student_name.'%')
                    ->orWhere(DB::raw("CONCAT( users.first_name, ' ', users.last_name)"), 'LIKE', "%".$student_name."%");
            }
            if(!empty($location)){
                $students->where('address.city', 'like',  '%'.$location.'%')
                    ->orWhere('address.street', 'like', '%'.$location.'%')
//                    ->orWhere('address.state', 'like', '%'.$location.'%');
                    ->orWhere(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'LIKE', "%".$location."%");
            }

        }

*/

        $students->select('users.user_id', 'users.first_name', 'users.last_name', 'users.phone', 'address.city','address.street','address.zip','address.state','users.email','users.created_at','students.respond'  );

        if(!empty($request->input('sort_method'))){

            $sort_value     =   $request->input('sort_value');
            $sort_method    =   $request->input('sort_method');

            if( $sort_value == 'sort_name'){
                $students->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), $sort_method);
            }

            if( $sort_value == 'sort_email'){
                $students->orderBy('users.email', $sort_method);
            }

            if( $sort_value == 'sort_date'){
                $students->orderBy('users.created_at', $sort_method);
            }

            if( $sort_value == 'sort_location'){
                $students->orderBy(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), $sort_method);
            }


        }


        $students = $students->paginate(100);

        $bulk_data = array();
        $i = 1;
        foreach($students as $student){
            $bulk_data[$i]['student_id'] = $student->user_id;
            $bulk_data[$i]['first_name'] = $student->first_name;
            $bulk_data[$i]['last_name'] = $student->last_name;
            $bulk_data[$i]['user_id'] = $student->user_id;
            $bulk_data[$i]['phone'] = $student->phone;
            $bulk_data[$i]['email'] = $student->email;
            $bulk_data[$i]['street'] = $student->street;
            $bulk_data[$i]['city'] = $student->city;
            $bulk_data[$i]['state'] = $student->state;
            $bulk_data[$i]['created_at'] = $student->created_at;
            $bulk_data[$i]['respond'] = $student->respond;
         /*   $student_sessions =
                DB::table('sessions')
                    ->join('payments', 'payments.session_id', '=', 'sessions.session_id')
                    ->where('sessions.student_id', '=', $student->user_id)
                    ->where('sessions.is_completed', '=', 1)
                    ->where('payments.is_credited', '=', 1)->count(); */
            $students_subjects = DB::table('sessions')->where('student_id', '=', $student->user_id)->count();
            $bulk_data[$i]['sessions'] = $students_subjects;
            $student_payments = DB::table('packages')
                ->join('payments', 'packages.package_id', '=', 'payments.package_id')
                ->where('student_id', '=', $student->user_id)
                ->where('is_charged', '=', 1)
                ->select(DB::raw('(total_hours*per_hour) as row_total'))
                ->groupBy('payments.package_id')
                ->get();
            if(!empty($student_payments)) {
                $full_total_student_payment = 0;
                foreach($student_payments as $each_payment) {
                    $full_total_student_payment += $each_payment->row_total;
                }
                $bulk_data[$i]['student_payment'] = '$'.$full_total_student_payment;
            } else {
                $bulk_data[$i]['student_payment'] = 'N/A';
            }
          //  $bulk_data[$i]['sessions'] = $student_sessions;

            $i++;
        }
        $students->setPath('?student_name='.$student_name.'&location='.$location.'&email='.$email.'&');

        return view('students.studentsList', ['bulk_data' => $bulk_data,'paginations'=>$students]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function details($id)
    {

        $students['details'] = DB::table('students')
            ->join('users', 'students.user_id', '=', 'users.user_id')
            ->join('address', 'users.user_id', '=', 'address.user_id')
            ->where('users.user_id', '=', $id)
            ->select('users.email','users.first_name','users.last_name','users.phone','address.street','address.city','address.state','address.zip')
            ->first();

        $students['grade'] = DB::table('sessions')
            ->join('grades', 'sessions.grade_id', '=', 'grades.grade_id')
            ->join('subjects', 'sessions.subject_id', '=', 'subjects.subject_id')
            ->where('sessions.student_id', '=', $id)
            ->select('grades.grade', 'subjects.subject_name', DB::raw('count(sessions.session_id) AS total_sessions'))
            ->groupBy('sessions.grade_id','sessions.subject_id')
            ->get();

        $students['no_sessions'] = DB::table('sessions')->where('student_id', '=', $id)->count();
//
//        $students['package_details'] = DB::table('packages')
//                                    ->join('grade_subjects', 'packages.subject_grade_id', '=', 'grade_subjects.id')
//                                    ->join('subjects', 'grade_subjects.subject_id', '=', 'subjects.subject_id')
//                                    ->join('grades', 'grade_subjects.grade_id', '=', 'grades.grade_id')
//                                    ->where('packages.student_id', '=', $id)
//                                    ->where('packages.package_type', '=',1)
//                                    ->select('grades.grade','grades.grade_id','subjects.subject_name','packages.total_hours','packages.package_id')
//                                    ->get();

        $students_package_details = DB::table('packages')
            ->join('grade_subjects', 'packages.subject_grade_id', '=', 'grade_subjects.id')
            ->join('subjects', 'grade_subjects.subject_id', '=', 'subjects.subject_id')
            ->join('grades', 'grade_subjects.grade_id', '=', 'grades.grade_id')
            ->where('packages.student_id', '=', $id)
            ->where('packages.package_type', '=',1)
            ->select('grades.grade','grades.grade_id','subjects.subject_name','packages.total_hours','packages.package_id')
            ->get();


        $package_details_arr = array();
        $i = 1;
        foreach($students_package_details as $students_package_details_val){

            $package_details_arr[$i]['grade'] = $students_package_details_val->grade;
            $package_details_arr[$i]['grade_id'] = $students_package_details_val->grade_id;
            $package_details_arr[$i]['subject_name'] = $students_package_details_val->subject_name;
            $package_details_arr[$i]['total_hours'] = $students_package_details_val->total_hours;
            $package_details_arr[$i]['package_id'] = $students_package_details_val->package_id;

            $payment_hours = DB::table('payments')
                ->where('package_id', '=', $students_package_details_val->package_id)
                ->select(DB::raw("sum(hours) as used_hours"))
                ->get();

            $package_details_arr[$i]['used_hours'] = $payment_hours[0]->used_hours;
            $i++;

        }
        $students['package_details'] = $package_details_arr;


        //  select grades.grade,subjects.subject_name from packages
        // join grade_subjects on packages.subject_grade_id = grade_subjects.id
        //  join subjects on grade_subjects.subject_id=subjects.subject_id
        //   join grades on grade_subjects.grade_id=grades.grade_id
        // where packages.student_id=469 and packages.package_type=1

//        var_dump($students['package_details']);
        // exit;

        if(empty($students['details'])) {
            return redirect()->action('StudentsController@listing');
        } else {
            return view('students.studentDetails', compact('students'));
        }


    }

    public function  delete_student_by_student_id(Request $request){


        $student_id =  $request->input('student_id');

        $is_student = DB::table('students')
            ->where('user_id', '=', $student_id)
            ->get();

        if(count($is_student) > 0 ){

            $is_student_in_session = DB::table('students')
                ->join('sessions', 'students.user_id', '=', 'sessions.student_id')
                ->join('payments', 'payments.session_id', '=', 'sessions.session_id')
                ->where('sessions.student_id', '=', $student_id)
                ->get();

            if(count($is_student_in_session) > 0 ){

                echo  $message = "This student has a scheduled session";

            }else{

                $is_student_in_address = DB::table('address')
                    ->where('user_id', '=', $student_id)
                    ->get();

                if(count($is_student_in_address) > 0 ){
                    $result = DB::table('address')
                        ->where('user_id', '=', $student_id)
                        ->delete();
                }

                $is_student_in_student_accounts = DB::table('student_accounts')
                    ->where('user_id', '=', $student_id)
                    ->get();

                if(count($is_student_in_student_accounts) > 0 ){

                    $result = DB::table('student_accounts')
                        ->where('user_id', '=', $student_id)
                        ->delete();
                }

                $is_student_in_sessions = DB::table('sessions')
                    ->where('student_id', '=', $student_id)
                    ->get();

                if(count($is_student_in_sessions) > 0 ){

                    $result = DB::table('sessions')
                        ->where('student_id', '=', $student_id)
                        ->delete();

                }

                $is_student_in_users = DB::table('users')
                    ->where('user_id', '=', $student_id)
                    ->get();

                if(count($is_student_in_users) > 0 ){

                    $result = DB::table('users')
                        ->where('user_id', '=', $student_id)
                        ->delete();
                }

                echo  $message = "Student deleted has been success";
            }

        }

    }
    public function to_excel(Request $request)
    {

        $students = DB::table('users')
            ->join('students', 'students.user_id', '=', 'users.user_id')
            ->join('address', 'users.user_id', '=', 'address.user_id');

        if($request->input('studentNameEx') != '') {
            $students->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), 'LIKE', "%".$request->input('studentNameEx')."%");
        }
        if($request->input('locationEx') != ''){
            $students->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'LIKE', "%".$request->input('locationEx')."%");
        }

        //Paginator
        $students->select('users.user_id', 'users.first_name', 'users.last_name', 'users.phone', 'address.city','address.street','address.zip','address.state','users.email' );
        $students = $students->get();

        $bulk_data = array();
        $i = 1;
        foreach($students as $student){
            $bulk_data[$i]['first_name'] = $student->first_name;
            $bulk_data[$i]['last_name'] = $student->last_name;
            $bulk_data[$i]['email'] = $student->email;
            $bulk_data[$i]['phone'] = $student->phone;

            $bulk_data[$i]['street'] = $student->street;
            $bulk_data[$i]['city'] = $student->city;
            $bulk_data[$i]['state'] = $student->state;

            $students_subjects = DB::table('sessions')->where('student_id', '=', $student->user_id)->count();

            $bulk_data[$i]['sessions'] = $students_subjects;

            $i++;

        }


        Excel::create('All student list', function($excel) use($bulk_data)  {

            $excel->sheet('details list', function($sheet) use($bulk_data)  {

                $sheet->fromArray($bulk_data);

            });

        })->download('xls');

        return Redirect::back();

    }

    public function update_student_status(Request $request){

        $status_value =  $request->input('status_value');
        $user_id =  $request->input('user_id');

        $is_result =  DB::table('students')
            ->where('user_id', $user_id)
            ->update(array('respond' => $status_value));

        if($status_value == 0){

            $arr = array(
                'status_value'=>$status_value,
                'user_id'=>$user_id
            );
            return $arr;

        }else if($status_value == 1){

            $arr = array(
                'status_value'=>$status_value,
                'user_id'=>$user_id
            );

            return $arr;

        }

    }




}
