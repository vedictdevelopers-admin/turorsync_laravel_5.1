<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class SessionsController extends Controller
{


    public function __construct() {
        $this->middleware('auth');
    }


    public function listing()
    {
        $url_param = $_GET;
        $sessions = DB::table('sessions')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("address","address.user_id","=","student_users.user_id")
            ->join("payments","payments.session_id","=","sessions.session_id")
            ->leftJoin(DB::raw("users as tutor_users"), function ($join) {
                $join->on(DB::raw("`sessions`.`tutor_id`"), '=', DB::raw("`tutor_users`.`user_id`"))
                      ->on(DB::raw("`sessions`.`tutor_id`"), '!=', DB::raw(0));
                });

        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if(!empty($url_param['tutor_name'])) {
                    $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }


                if(!empty($url_param['student_name'])) {
                    $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                }
                if(!empty($url_param['location'])) {
                    $sessions->where('sessions.street', "like" , '%'.$url_param['location'].'%');
                }
                if(!empty($url_param['session_date'])) {
                    $sessions->where('sessions.date', "=" ,  $url_param['session_date']);
                }
            }
        }
        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',
            DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));
        $sessions->groupBy('sessions.session_id');

        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $sessions = $sessions->paginate(100);
        } else {
            $sessions = $sessions->paginate(100);
        }

        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;// DB::table('users')->select(DB::raw("CONCAT(first_name, ' ', last_name) as tutor_name"))->where('user_id', '=', $each_session->tutor_id)->first();
           // $all_session_data[$i]['tutor_name'] = $tutor_name;//->tutor_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;//DB::table('users')->select(DB::raw("CONCAT(first_name, ' ', last_name) as student_name"))->where('user_id', '=', $each_session->student_id)->first();
           // $all_session_data[$i]['student_name'] = $student_name->student_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));

            if($each_session->is_completed == 0) {
                $all_session_data[$i]['status'] = 'Scheduled';
            } elseif($each_session->is_completed == 1) {
                $all_session_data[$i]['status'] = 'Completed';
            }elseif($each_session->is_completed == 2) {
                $all_session_data[$i]['status'] = 'Declined';
            }elseif($each_session->is_completed == 3) {
                $all_session_data[$i]['status'] = 'Cancelled';
            }elseif($each_session->is_completed == -1) {
                $all_session_data[$i]['status'] = 'No Tutor';
            }

            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }

         if(!empty($url_param) && count($url_param) > 1) {
             $sessions->setPath('?tutor_name='.$url_param['tutor_name'].'&student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&session_date='.$url_param['session_date'].'&');
         } else {
             $sessions->setPath('');
         }


        return view('sessions.sessionsList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);

    }

    public function filter_all_sessions(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $tutor_name     =   $request->input('tutorName');
        $location       =   $request->input('location');
        if($request->input('sessionDate') != '') {
            $session_date   =   date('Y-m-d', strtotime($request->input('sessionDate')));
        } else {
            $session_date = "";
        }


        $sessions = DB::table('sessions')
                    ->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id")
                    ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
                    ->join("address","address.user_id","=","student_users.user_id")
                    ->join("payments","payments.session_id","=","sessions.session_id")
                    ->leftJoin(DB::raw("users as tutor_users"), function ($join) {
                        $join->on(DB::raw("`sessions`.`tutor_id`"), '=', DB::raw("`tutor_users`.`user_id`"))
                             ->on(DB::raw("`sessions`.`tutor_id`"), '!=', DB::raw(0));
    });
        if(!empty($tutor_name)) {
            $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$tutor_name.'%');
        }


        if(!empty($student_name)) {
            $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$student_name.'%');
        }
        if(!empty($location)) {
            $sessions->where('sessions.street', "like" , '%'.$location.'%');
        }
        if(!empty($session_date)) {
            $sessions->where('sessions.date', "=" ,  $session_date);
        }

        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',
            DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));
        $sessions->groupBy('sessions.session_id');
        $sessions = $sessions->paginate(100);



        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));

            $all_session_data[$i]['location'] = $each_session->street;
            if($each_session->is_completed == 0) {
                $all_session_data[$i]['status'] = 'Scheduled';
            } elseif($each_session->is_completed == 1) {
                $all_session_data[$i]['status'] = 'Completed';
            }elseif($each_session->is_completed == 2) {
                $all_session_data[$i]['status'] = 'Declined';
            }elseif($each_session->is_completed == 3) {
                $all_session_data[$i]['status'] = 'Cancelled';
            }elseif($each_session->is_completed == -1) {
                $all_session_data[$i]['status'] = 'No Tutor';
            }
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }

        $sessions->setPath('?tutor_name='.$tutor_name.'&student_name='.$student_name.'&location='.$location.'&session_date='.$session_date.'&');

        return view('sessions.sessionsList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);

    }

    public function scheduled_listing()
    {
        $url_param = $_GET;
        $sessions = DB::table('sessions')
            ->where('sessions.is_completed', "=" ,  '0')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
            ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("payments","payments.session_id","=","sessions.session_id")
            ->join("address","address.user_id","=","student_users.user_id");

        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if(!empty($url_param['tutor_name'])) {
                    $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }


                if(!empty($url_param['student_name'])) {
                    $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                }
                if(!empty($url_param['location'])) {
                    $sessions->where('sessions.street', "like" , '%'.$url_param['location'].'%');
                }
                if(!empty($url_param['session_date'])) {
                    $sessions->where('sessions.date', "=" ,  $url_param['session_date']);
                }
            }
        }
        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));

        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $sessions = $sessions->paginate(100);
        } else {
            $sessions = $sessions->paginate(100);
        }
        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;

            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Scheduled';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }


        if(!empty($url_param) && count($url_param) > 1) {
            $sessions->setPath('?tutor_name='.$url_param['tutor_name'].'&student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&session_date='.$url_param['session_date'].'&');
        } else {
            $sessions->setPath('');
        }


        return view('sessions.sessionsScheduledList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function filter_Scheduled_sessions(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $tutor_name     =   $request->input('tutorName');
        $location       =   $request->input('location');

        if($request->input('sessionDate') != '') {
            $session_date   =   date('Y-m-d', strtotime($request->input('sessionDate')));
        } else {
            $session_date = "";
        }

        $sessions = DB::table('sessions')
                    ->where('sessions.is_completed', "=" ,  '0')
                    ->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id")
                    ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
                    ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
                    ->join("payments","payments.session_id","=","sessions.session_id")
                    ->join("address","address.user_id","=","student_users.user_id");
        if(!empty($tutor_name)) {
            $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$tutor_name.'%');
        }


        if(!empty($student_name)) {
            $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$student_name.'%');
        }
        if(!empty($location)) {
            $sessions->where('sessions.street', "like" , '%'.$location.'%');
        }
        if(!empty($session_date)) {

            $sessions->where('sessions.date', "=" ,  $session_date);
        }

        $sessions->select(DB::raw('tutor_users.first_name as tutor_first_name'),DB::raw('tutor_users.last_name as tutor_last_name'),DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.last_name as student_last_name'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.session_id');


        $sessions = $sessions->paginate(100);


        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Scheduled';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
        $sessions->setPath('?tutor_name='.$tutor_name.'&student_name='.$student_name.'&location='.$location.'&session_date='.$session_date.'&');

        return view('sessions.sessionsScheduledList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);



    }

    public function completed_listing()
    {
        $url_param = $_GET;
        $sessions = DB::table('sessions')
            ->where('sessions.is_completed', "=" ,  '1')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
            ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("payments","payments.session_id","=","sessions.session_id")
            ->join("address","address.user_id","=","student_users.user_id");

        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if(!empty($url_param['tutor_name'])) {
                    $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }


                if(!empty($url_param['student_name'])) {
                    $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                }
                if(!empty($url_param['location'])) {
                    $sessions->where('sessions.street', "like" , '%'.$url_param['location'].'%');
                }
                if(!empty($url_param['session_date'])) {
                    $sessions->where('sessions.date', "=" ,  $url_param['session_date']);
                }
            }
        }



        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));

        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $sessions = $sessions->paginate(100);
        } else {
            $sessions = $sessions->paginate(100);
        }

        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Completed';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
//        dd($all_session_data);
//        $all_session_data[$i]['time'] = $each_session->start_time<= 12 ? $each_session->start_time.' AM - ' : $each_session->start_time.' PM - '.$each_session->end_time
//        dd($all_session_data);

        if(!empty($url_param) && count($url_param) > 1) {
            $sessions->setPath('?tutor_name='.$url_param['tutor_name'].'&student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&session_date='.$url_param['session_date'].'&');
        } else {
            $sessions->setPath('');
        }

        return view('sessions.sessionsCompletedList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);

    }

    public function filter_Completed_sessions(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $tutor_name     =   $request->input('tutorName');
        $location       =   $request->input('location');
        if($request->input('sessionDate') != '') {
            $session_date   =   date('Y-m-d', strtotime($request->input('sessionDate')));
        } else {
            $session_date = "";
        }


        $sessions = DB::table('sessions')
                    ->where('sessions.is_completed', "=" ,  '1')
                    ->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id")
                    ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
                    ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
                    ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
                    ->join("payments","payments.session_id","=","sessions.session_id")
                    ->join("address","address.user_id","=","student_users.user_id");
        if(!empty($tutor_name)) {
            $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$tutor_name.'%');
        }


        if(!empty($student_name)) {
            $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$student_name.'%');
        }
        if(!empty($location)) {
            $sessions->where('sessions.street', "like" , '%'.$location.'%');
        }

        if(!empty($session_date)) {
            $sessions->where('sessions.date', "=" ,  $session_date);
        }

        $sessions->select(DB::raw('tutor_users.first_name as tutor_first_name'),DB::raw('tutor_users.last_name as tutor_last_name'),DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.last_name as student_last_name'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.session_id');

        $sessions = $sessions->paginate(100);
//        dd($sessions);


        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Completed';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
        $sessions->setPath('?tutor_name='.$tutor_name.'&student_name='.$student_name.'&location='.$location.'&session_date='.$session_date.'&');

        return view('sessions.sessionsCompletedList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function cancelled_listing()
    {
        $url_param = $_GET;
        $sessions = DB::table('sessions')
            ->where('sessions.is_completed', "=" ,  '3')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
            ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("payments","payments.session_id","=","sessions.session_id")
            ->join("address","address.user_id","=","student_users.user_id");

          if(!empty($url_param)) {
              if(count($url_param) > 1) {
                  if(!empty($url_param['tutor_name'])) {
                      $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                  }


                  if(!empty($url_param['student_name'])) {
                      $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                  }
                  if(!empty($url_param['location'])) {
                      $sessions->where('sessions.street', "like" , '%'.$url_param['location'].'%');
                  }
                  if(!empty($url_param['session_date'])) {
                      $sessions->where('sessions.date', "=" ,  $url_param['session_date']);
                  }
              }
          }


        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));
        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $sessions = $sessions->paginate(100);
        } else {
            $sessions = $sessions->paginate(100);
        }

        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Cancelled';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
//        dd($all_session_data);
//        $all_session_data[$i]['time'] = $each_session->start_time<= 12 ? $each_session->start_time.' AM - ' : $each_session->start_time.' PM - '.$each_session->end_time
//        dd($all_session_data);
        if(!empty($url_param) && count($url_param) > 1) {
            $sessions->setPath('?tutor_name='.$url_param['tutor_name'].'&student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&session_date='.$url_param['session_date'].'&');
        } else {
            $sessions->setPath('');
        }

        return view('sessions.sessionsCancelledList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function filter_Cancelled_sessions(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $tutor_name     =   $request->input('tutorName');
        $location       =   $request->input('location');
        if($request->input('sessionDate') != '') {
            $session_date   =   date('Y-m-d', strtotime($request->input('sessionDate')));
        } else {
            $session_date = "";
        }


        $sessions = DB::table('sessions');
        $sessions->where('sessions.is_completed', "=" ,  '3')
                 ->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id")
                  ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
                 ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
                 ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
                 ->join("payments","payments.session_id","=","sessions.session_id")
                 ->join("address","address.user_id","=","student_users.user_id");



        if(!empty($tutor_name)) {
            $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$tutor_name.'%');
        }


        if(!empty($student_name)) {
            $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$student_name.'%');
        }
        if(!empty($location)) {
            $sessions->where('sessions.street', "like" , '%'.$location.'%');
        }
        if(!empty($session_date)) {
            $sessions->where('sessions.date', "=" ,  $session_date);
        }

        $sessions->select(DB::raw('tutor_users.first_name as tutor_first_name'),DB::raw('tutor_users.last_name as tutor_last_name'),DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.last_name as student_last_name'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.session_id');

        $sessions = $sessions->paginate(100);
//        dd($sessions);


        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Cancelled';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
        $sessions->setPath('?tutor_name='.$tutor_name.'&student_name='.$student_name.'&location='.$location.'&session_date='.$session_date.'&');

        return view('sessions.sessionsCancelledList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function declined_listing()
    {
        $url_param = $_GET;
        $sessions = DB::table('sessions')
            ->where('sessions.is_completed', "=" ,  '2')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
            ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("payments","payments.session_id","=","sessions.session_id")
            ->join("address","address.user_id","=","student_users.user_id");

        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if(!empty($url_param['tutor_name'])) {
                    $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }


                if(!empty($url_param['student_name'])) {
                    $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                }
                if(!empty($url_param['location'])) {
                    $sessions->where('sessions.street', "like" , '%'.$url_param['location'].'%');
                }
                if(!empty($url_param['session_date'])) {
                    $sessions->where('sessions.date', "=" ,  $url_param['session_date']);
                }
            }
        }

        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));
        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $sessions = $sessions->paginate(100);
        } else {
            $sessions = $sessions->paginate(100);
        }

        $all_session_data = array();
        $i = 1;





        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Declined';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }

        if(!empty($url_param) && count($url_param) > 1) {
            $sessions->setPath('?tutor_name='.$url_param['tutor_name'].'&student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&session_date='.$url_param['session_date'].'&');
        } else {
            $sessions->setPath('');
        }


        return view('sessions.sessionsDeclinedList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function filter_Declined_sessions(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $tutor_name     =   $request->input('tutorName');
        $location       =   $request->input('location');
        if($request->input('sessionDate') != '') {
            $session_date   =   date('Y-m-d', strtotime($request->input('sessionDate')));
        } else {
            $session_date = "";
        }


        $sessions = DB::table('sessions');
        $sessions->where('sessions.is_completed', "=" ,  '2')
                 ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
                 ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
                 ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
                 ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
                 ->join("payments","payments.session_id","=","sessions.session_id")
                 ->join("address","address.user_id","=","student_users.user_id");



        if(!empty($tutor_name)) {
            $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$tutor_name.'%');
        }


        if(!empty($student_name)) {
            $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$student_name.'%');
        }
        if(!empty($location)) {
            $sessions->where('sessions.street', "like" , '%'.$location.'%');
        }
        if(!empty($session_date)) {
            $sessions->where('sessions.date', "like" ,  '%'.$session_date.'%');
        }

        $sessions->select(DB::raw('tutor_users.first_name as tutor_first_name'),DB::raw('tutor_users.last_name as tutor_last_name'),DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.last_name as student_last_name'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.session_id');

        $sessions = $sessions->paginate(100);
//        dd($sessions);


        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            $all_session_data[$i]['status'] = 'Declined';
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
        $sessions->setPath('?tutor_name='.$tutor_name.'&student_name='.$student_name.'&location='.$location.'&session_date='.$session_date.'&');

        return view('sessions.sessionsDeclinedList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }


    public function abandoned_listing()
    {

        $url_param = $_GET;
        $sessions = DB::table('sessions')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')

            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("address","address.user_id","=","student_users.user_id")
            ->leftJoin(DB::raw("users as tutor_users"), function ($join) {
                $join->on(DB::raw("`sessions`.`tutor_id`"), '=', DB::raw("`tutor_users`.`user_id`"))
                    ->on(DB::raw("`sessions`.`tutor_id`"), '!=', DB::raw(0));
            })
            ->whereNotIn('sessions.session_id', function($query)
            {
                $query->select(DB::raw('session_id'))
                    ->from('payments');
            });


        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if(!empty($url_param['tutor_name'])) {
                    $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }


                if(!empty($url_param['student_name'])) {
                    $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$url_param['student_name'].'%');
                }
                if(!empty($url_param['location'])) {
                    $sessions->where('sessions.street', "like" , '%'.$url_param['location'].'%');
                }
                if(!empty($url_param['session_date'])) {
                    $sessions->where('sessions.date', "=" ,  $url_param['session_date']);
                }
            }
        }

        $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date','sessions.is_completed', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));
        
        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $sessions = $sessions->paginate(100);
        } else {
            $sessions = $sessions->paginate(100);
        }

        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));

            if($each_session->is_completed == 0) {
                $all_session_data[$i]['status'] = 'Scheduled';
            } elseif($each_session->is_completed == 1) {
                $all_session_data[$i]['status'] = 'Completed';
            }elseif($each_session->is_completed == 2) {
                $all_session_data[$i]['status'] = 'Declined';
            }elseif($each_session->is_completed == 3) {
                $all_session_data[$i]['status'] = 'Cancelled';
            }elseif($each_session->is_completed == -1) {
                $all_session_data[$i]['status'] = 'No Tutor';
            }


            $all_session_data[$i]['is_completed'] = $each_session->is_completed;
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
        if(!empty($url_param) && count($url_param) > 1) {
            $sessions->setPath('?tutor_name='.$url_param['tutor_name'].'&student_name='.$url_param['student_name'].'&location='.$url_param['location'].'&session_date='.$url_param['session_date'].'&');
        } else {
            $sessions->setPath('');
        }


        return view('sessions.sessionsAbandonedList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function filter_Abandoned_sessions(Request $request)
    {

        $student_name   =   $request->input('studentName');
        $tutor_name     =   $request->input('tutorName');
        $location       =   $request->input('location');
        if($request->input('sessionDate') != '') {
            $session_date   =   date('Y-m-d', strtotime($request->input('sessionDate')));
        } else {
            $session_date = "";
        }


        $sessions = DB::table('sessions');
        $sessions->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->leftJoin(DB::raw("users as tutor_users"), function ($join) {
                $join->on(DB::raw("`sessions`.`tutor_id`"), '=', DB::raw("`tutor_users`.`user_id`"))
                    ->on(DB::raw("`sessions`.`tutor_id`"), '!=', DB::raw(0));
            })
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id")
            ->join("address","address.user_id","=","student_users.user_id")
            ->whereNotIn('sessions.session_id', function($query)
            {
                $query->select(DB::raw('session_id'))
                    ->from('payments');
            });



        if(!empty($tutor_name)) {
            $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$tutor_name.'%');
        }


        if(!empty($student_name)) {
            $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$student_name.'%');
        }
        if(!empty($location)) {
            $sessions->where('sessions.street', "like" , '%'.$location.'%');
        }
        if(!empty($session_date)) {
            $sessions->where('sessions.date', "like" ,  '%'.$session_date.'%');
        }

        $sessions->select(DB::raw('tutor_users.first_name as tutor_first_name'),DB::raw('tutor_users.last_name as tutor_last_name'),DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.last_name as student_last_name'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.session_id');

        $sessions = $sessions->paginate(100);
//        dd($sessions);


        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name.' '.$each_session->tutor_last_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));
            if($each_session->is_completed == 0) {
                $all_session_data[$i]['status'] = 'Scheduled';
            } elseif($each_session->is_completed == 1) {
                $all_session_data[$i]['status'] = 'Completed';
            }elseif($each_session->is_completed == 2) {
                $all_session_data[$i]['status'] = 'Declined';
            }elseif($each_session->is_completed == 3) {
                $all_session_data[$i]['status'] = 'Cancelled';
            }elseif($each_session->is_completed == -1) {
                $all_session_data[$i]['status'] = 'No Tutor';
            }
            $all_session_data[$i]['location'] = $each_session->street;
            $all_session_data[$i]['session_id'] = $each_session->session_id;
            $i++;
        }
        $sessions->setPath('?tutor_name='.$tutor_name.'&student_name='.$student_name.'&location='.$location.'&session_date='.$session_date.'&');

        return view('sessions.sessionsAbandonedList', ['all_session_data' => $all_session_data,'paginations'=>$sessions]);


    }

    public function abandoned_sessions_by_id($session_id,$is_completed){

        if($is_completed == "-1"){


            $sessions_details = DB::table('sessions');
            $sessions_details->where('sessions.session_id',"=",$session_id);
            $sessions_details->where('sessions.is_completed', "=" ,  '-1');
            $sessions_details->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id");
            $sessions_details->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
            $sessions_details->join("address","address.user_id","=","student_users.user_id");
            $sessions_details->select(DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.email as student_email'),DB::raw('student_users.last_name as student_last_name'),DB::raw('student_users.phone'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','sessions.session_id','address.city','address.state');
            $sessions_details = $sessions_details->first();



        }else{

            $sessions_details = DB::table('sessions');
            $sessions_details->where('sessions.session_id',"=",$session_id);

            $sessions_details->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id");
            $sessions_details->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id");
            $sessions_details->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id");
            $sessions_details->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
            $sessions_details->join("address","address.user_id","=","student_users.user_id");
            $sessions_details->select(DB::raw('tutor_users.first_name as tutor_first_name'),
                DB::raw('tutor_users.last_name as tutor_last_name'),
                DB::raw('student_users.first_name as student_first_name'),
                DB::raw('student_users.last_name as student_last_name'),
                DB::raw('student_users.email as student_email'),
                DB::raw('student_users.phone'),'tutors.gender','tutors.tutor_type','subjects.subject_name','sessions.date','sessions.session_id',
                DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.grade_id','sessions.subject_id','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','tutors.gender','tutors.profile_image');
            $sessions_details = $sessions_details->first();

            /*
           * Tutor earning amount query
           */

            $subject_id =  $sessions_details->subject_id;
            $grade_id =   $sessions_details->grade_id;
            $tutor_type = $sessions_details->tutor_type;
            $hours =  $sessions_details->end_time-$sessions_details->start_time;

            $pro_tutor_amount = DB::table('grade_subjects')
                ->where('subject_id',"=",$subject_id)
                ->where('grade_id',"=",$grade_id)
                ->select('pro_tutor_amount','peer_tutor_amount')->first();


            if($tutor_type=="PRO"){
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $hours;
            }else{
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $hours;
            }


        }

//        $session_payment_details = DB::table('payments');
//        $session_payment_details->where('payments.session_id', '=', $session_id);
//        $session_payment_details->join("packages","packages.package_id","=","payments.package_id");
//        $session_payment_details->select(DB::raw('packages.package_type'),'payments.hours','payments.per_hour','payments.per_hour_tutor');
//        $session_payment_details = $session_payment_details->get();
//
//
//        $sessions_details->per_hour = $session_payment_details[0]->per_hour;
//
//
//        if($session_payment_details[0]->package_type ==1){
//            $sessions_details->student_payment = 10 *($session_payment_details[0]->per_hour);
//            $sessions_details->package_type = "Package";
//        }else{
//            $sessions_details->student_payment = ($session_payment_details[0]->hours)*($session_payment_details[0]->per_hour);
//            $sessions_details->package_type = "Hourly";
//        }
//
        if(empty($sessions_details)) {
            return redirect()->action('SessionsController@abandoned_listing');
        } else {
            return view('sessions.sessionsAbandonedById', compact('sessions_details'));
        }


    }

    public function scheduled_sessions_by_id($session_id)
    {

        $sessions_details = DB::table('sessions');
        $sessions_details->where('sessions.session_id',"=",$session_id);
        $sessions_details->where('sessions.is_completed', "=" ,  '0');
        $sessions_details->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id");
        $sessions_details->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id");
        $sessions_details->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id");
        $sessions_details->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
        $sessions_details->join("address","address.user_id","=","student_users.user_id");
        $sessions_details->join("grades","grades.grade_id","=","sessions.grade_id");
        $sessions_details->select('grades.grade AS grade_name',DB::raw('tutor_users.first_name as tutor_first_name'),
            DB::raw('tutor_users.last_name as tutor_last_name'),
            DB::raw('student_users.first_name as student_first_name'),
            DB::raw('student_users.last_name as student_last_name'),
            DB::raw('student_users.email as student_email'),
            DB::raw('tutor_users.email as tutor_email'),
            DB::raw('tutor_users.phone as tutor_phone'),
            DB::raw('student_users.user_id as student_id'),
            DB::raw('student_users.phone'),'tutors.gender','tutors.tutor_type','subjects.subject_name','sessions.date','sessions.session_id',
            DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.subject_id','sessions.grade_id','sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','tutors.gender','tutors.profile_image','sessions.promo');
        $sessions_details = $sessions_details->first();



       $session_payment_details = DB::table('payments')
                                    ->where('payments.session_id', '=', $session_id)
                                    ->join("packages","packages.package_id","=","payments.package_id")
                                    ->select(DB::raw('packages.package_type'),DB::raw('packages.is_charged'),DB::raw('sum(payments.hours) as hours'),DB::raw('payments.per_hour'),'payments.per_hour_tutor')
                                    ->first();

        $sessions_details->per_hour = $session_payment_details->per_hour;
        $sessions_details->is_charged = $session_payment_details->is_charged;


        /*
         * Tutor earning amount query
         */

        $subject_id =  $sessions_details->subject_id;

        $grade_id =   $sessions_details->grade_id;

        $tutor_type = $sessions_details->tutor_type;

        $pro_tutor_amount = DB::table('grade_subjects')
            ->where('subject_id',"=",$subject_id)
            ->where('grade_id',"=",$grade_id)
            ->select('pro_tutor_amount','peer_tutor_amount')
            ->first();


   $student_id = $sessions_details->student_id;


        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=',$subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($session_payment_details->package_type == 1) {
        $package_count = DB::table('packages')
            ->where('subject_grade_id', '=',$grade_subjects_id->id )
            ->where('student_id', '=', $student_id)
            ->where('package_type', '=', 1)
            ->select(DB::raw("sum(total_hours) as all_count"))
            ->first();


        $package_count = $package_count->all_count;


        //need to add stubject_grade_id to the packages db.
           // DB::select("select sum((end_time-start_time)) as total_hours from sessions where student_id=".$student_id." and subject_id=".."3 and grade_id=2
        $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");



      $sessions_details->remaining_hours = (10 - ($package_count - $used_session[0]->hours));

         } else {
        $sessions_details->remaining_hours = "";
    }



        if($session_payment_details->package_type ==1){

            $sessions_details->student_payment = 10 *($session_payment_details->per_hour);
            $sessions_details->package_type = "Package";

           // $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;

        }else{
            $sessions_details->student_payment = ($session_payment_details->hours)*($session_payment_details->per_hour);
            $sessions_details->package_type = "Hourly";

            if($tutor_type=="PRO"){
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            }else{
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;
            }
        }
        if($sessions_details->promo != '') {
            $promo_values = DB::table('promo')
                ->where('promo', '=',$sessions_details->promo)
                ->select("discount","status")
                ->first();
            if(!empty($promo_values)) {
                if($promo_values->status == 1 && $promo_values->discount > 0) {
                    $student_payment_after_promo = ($sessions_details->student_payment) - (($sessions_details->student_payment) * ($promo_values->discount/100));
                    $sessions_details->discount = "$".($sessions_details->student_payment) * ($promo_values->discount/100);
                    $sessions_details->coupon_code = $sessions_details->promo;
                    $sessions_details->final_total = $student_payment_after_promo;
                } else {
                    $sessions_details->discount = 'Invalid';
                    $sessions_details->coupon_code = $sessions_details->promo;
                    $sessions_details->final_total = $sessions_details->student_payment;
                }
            } else {
                $sessions_details->discount = 'Invalid';
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $sessions_details->student_payment;
            }


        } else {
            $sessions_details->discount = 'N/A';
            $sessions_details->coupon_code = 'N/A';
            $sessions_details->final_total = $sessions_details->student_payment;
        }

        if(empty($sessions_details)) {
            return redirect()->action('SessionsController@scheduled_listing');
        } else {
            return view('sessions.sessionsScheduledById', compact('sessions_details'));
        }

    }


    public function completed_sessions_by_id($session_id)
    {


        $sessions_details = DB::table('sessions');
        $sessions_details->where('sessions.session_id',"=",$session_id);
        $sessions_details->where('sessions.is_completed', "=" ,  '1');
        $sessions_details->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id");
        $sessions_details->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id");
        $sessions_details->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id");
        $sessions_details->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
        $sessions_details->join("address","address.user_id","=","student_users.user_id");
        $sessions_details->join("grades","grades.grade_id","=","sessions.grade_id");
        $sessions_details->select('grades.grade AS grade_name',
            DB::raw('tutor_users.first_name as tutor_first_name'),
            DB::raw('tutor_users.last_name as tutor_last_name'),
            DB::raw('student_users.first_name as student_first_name'),
            DB::raw('student_users.last_name as student_last_name'),
            DB::raw('student_users.email as student_email'),
            DB::raw('tutor_users.email as tutor_email'),
            DB::raw('tutor_users.phone as tutor_phone'),
            DB::raw('student_users.user_id as student_id'),
            DB::raw('student_users.phone'),'tutors.gender','tutors.tutor_type','subjects.subject_name','sessions.date',
            DB::raw("sessions.end_time - sessions.start_time AS duration"),
            'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.subject_id','sessions.grade_id','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','tutors.gender','tutors.profile_image','sessions.promo');
        $sessions_details = $sessions_details->first();


        $total_amount = DB::table('sessions')
            ->join('payments', 'sessions.session_id', '=', 'payments.session_id')
            ->where('payments.session_id', '=',$session_id)
            ->select('payments.per_hour' , 'payments.hours')
            ->get();

        //SELECT payments.per_hour , payments.hours FROM `sessions`
        // join payments on sessions.session_id = payments.session_id
        // where payments.session_id  = 25

        $session_payment_details = DB::table('payments');
        $session_payment_details->where('payments.session_id', '=', $session_id);
        $session_payment_details->join("packages","packages.package_id","=","payments.package_id");
        $session_payment_details->select(DB::raw('packages.package_type'),DB::raw('packages.is_charged'),DB::raw('sum(payments.hours) as hours'),DB::raw('payments.per_hour'),'payments.per_hour_tutor');
        $session_payment_details = $session_payment_details->first();


        $sessions_details->per_hour = $session_payment_details->per_hour;
        $sessions_details->is_charged = $session_payment_details->is_charged;


        /*
         * Tutor earning amount query
         */

        $subject_id =  $sessions_details->subject_id;
        $grade_id =   $sessions_details->grade_id;
        $tutor_type = $sessions_details->tutor_type;

        $pro_tutor_amount = DB::table('grade_subjects')
            ->where('subject_id',"=",$subject_id)
            ->where('grade_id',"=",$grade_id)
            ->select('pro_tutor_amount','peer_tutor_amount')
            ->first();


        $student_id = $sessions_details->student_id;

        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=',$subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($session_payment_details->package_type == 1) {
            $package_count = DB::table('packages')
                ->where('subject_grade_id', '=',$grade_subjects_id->id )
                ->where('student_id', '=', $student_id)
                ->where('package_type', '=', 1)
                ->select(DB::raw("sum(total_hours) as all_count"))
                ->first();


            $package_count = $package_count->all_count;


            //need to add stubject_grade_id to the packages db.

            $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");



            $sessions_details->remaining_hours = (10 - ($package_count - $used_session[0]->hours));
        } else {
            $sessions_details->remaining_hours = "";
        }



        if($session_payment_details->package_type ==1){

            $sessions_details->student_payment = 10 *($session_payment_details->per_hour);
            $sessions_details->package_type = "Package";

            //$sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;

        }else{
            $sessions_details->student_payment = ($session_payment_details->hours)*($session_payment_details->per_hour);
            $sessions_details->package_type = "Hourly";

            if($tutor_type=="PRO"){
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            }else{
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;
            }
        }
        if($sessions_details->promo != '') {
            $promo_values = DB::table('promo')
                ->where('promo', '=',$sessions_details->promo)
                ->select("discount","status")
                ->first();
            if($promo_values->status == 1 && $promo_values->discount > 0) {
                $student_payment_after_promo = ($sessions_details->student_payment) - (($sessions_details->student_payment) * ($promo_values->discount/100));
                $sessions_details->discount = "$".($sessions_details->student_payment) * ($promo_values->discount/100);
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $student_payment_after_promo;
            } else {
                $sessions_details->discount = 'Invalid';
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $sessions_details->student_payment;
            }


        } else {
            $sessions_details->discount = 'N/A';
            $sessions_details->coupon_code = 'N/A';
            $sessions_details->final_total = $sessions_details->student_payment;
        }




        if(empty($sessions_details)) {
           return redirect()->action('SessionsController@completed_listing');
        } else {
            return view('sessions.sessionsCompletdById', compact('sessions_details','total_amount'));
        }

    }

    public function cancelled_sessions_by_id($session_id)
    {

        $sessions_details = DB::table('sessions');
        $sessions_details->where('sessions.session_id',"=",$session_id);
        $sessions_details->where('sessions.is_completed', "=" ,  '3');
        $sessions_details->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id");
        $sessions_details->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id");
        $sessions_details->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id");
        $sessions_details->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
        $sessions_details->join("address","address.user_id","=","student_users.user_id");
        $sessions_details->join("grades","grades.grade_id","=","sessions.grade_id");
        $sessions_details->select('grades.grade AS grade_name',
            DB::raw('tutor_users.first_name as tutor_first_name'),
            DB::raw('tutor_users.last_name as tutor_last_name'),
            DB::raw('student_users.first_name as student_first_name'),
            DB::raw('student_users.last_name as student_last_name'),
            DB::raw('student_users.email as student_email'),
            DB::raw('tutor_users.email as tutor_email'),
            DB::raw('tutor_users.phone as tutor_phone'),
            DB::raw('student_users.user_id as student_id'),
            DB::raw('student_users.phone'),'tutors.gender','tutors.tutor_type','subjects.subject_name','sessions.date',
            DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.subject_id','sessions.grade_id','sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','tutors.gender','tutors.profile_image','sessions.promo');
        $sessions_details = $sessions_details->first();

        $session_payment_details = DB::table('payments');
        $session_payment_details->where('payments.session_id', '=', $session_id);
        $session_payment_details->join("packages","packages.package_id","=","payments.package_id");
        $session_payment_details->select(DB::raw('packages.package_type'),DB::raw('packages.is_charged'),DB::raw('sum(payments.hours) as hours'),DB::raw('payments.per_hour'),'payments.per_hour_tutor');
        $session_payment_details = $session_payment_details->first();


        $sessions_details->per_hour = $session_payment_details->per_hour;
        $sessions_details->is_charged = $session_payment_details->is_charged;


        /*
         * Tutor earning amount query
         */

        $subject_id =  $sessions_details->subject_id;
        $grade_id =   $sessions_details->grade_id;
        $tutor_type = $sessions_details->tutor_type;

        $pro_tutor_amount = DB::table('grade_subjects')
            ->where('subject_id',"=",$subject_id)
            ->where('grade_id',"=",$grade_id)
            ->select('pro_tutor_amount','peer_tutor_amount')
            ->first();


        $student_id = $sessions_details->student_id;

        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=',$subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($session_payment_details->package_type == 1) {
            $package_count = DB::table('packages')
                ->where('subject_grade_id', '=',$grade_subjects_id->id )
                ->where('student_id', '=', $student_id)
                ->where('package_type', '=', 1)
                ->select(DB::raw("sum(total_hours) as all_count"))
                ->first();


            $package_count = $package_count->all_count;


            //need to add stubject_grade_id to the packages db.

            $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");



            $sessions_details->remaining_hours = (10 - ($package_count - $used_session[0]->hours));
        } else {
            $sessions_details->remaining_hours = "";
        }



        if($session_payment_details->package_type ==1){

            $sessions_details->student_payment = 10 *($session_payment_details->per_hour);
            $sessions_details->package_type = "Package";

           // $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;

        }else{
            $sessions_details->student_payment = ($session_payment_details->hours)*($session_payment_details->per_hour);
            $sessions_details->package_type = "Hourly";

            if($tutor_type=="PRO"){
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            }else{
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;
            }
        }
        if($sessions_details->promo != '') {
            $promo_values = DB::table('promo')
                ->where('promo', '=',$sessions_details->promo)
                ->select("discount","status")
                ->first();
            if($promo_values->status == 1 && $promo_values->discount > 0) {
                $student_payment_after_promo = ($sessions_details->student_payment) - (($sessions_details->student_payment) * ($promo_values->discount/100));
                $sessions_details->discount = "$".($sessions_details->student_payment) * ($promo_values->discount/100);
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $student_payment_after_promo;
            } else {
                $sessions_details->discount = 'Invalid';
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $sessions_details->student_payment;
            }


        } else {
            $sessions_details->discount = 'N/A';
            $sessions_details->coupon_code = 'N/A';
            $sessions_details->final_total = $sessions_details->student_payment;
        }

        if(empty($sessions_details)) {
            return redirect()->action('SessionsController@cancelled_listing');
        } else {
            return view('sessions.sessionsCancelledById', compact('sessions_details'));
        }

    }

    public function declined_sessions_by_id($session_id)
    {

        $sessions_details = DB::table('sessions');
        $sessions_details->where('sessions.session_id',"=",$session_id);
        $sessions_details->where('sessions.is_completed', "=" ,  '2')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
            ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
        $sessions_details->join("address","address.user_id","=","student_users.user_id");
        $sessions_details->join("grades","grades.grade_id","=","sessions.grade_id");
        $sessions_details->select('grades.grade AS grade_name',
            DB::raw('tutor_users.first_name as tutor_first_name'),
            DB::raw('tutor_users.last_name as tutor_last_name'),
            DB::raw('student_users.first_name as student_first_name'),
            DB::raw('student_users.last_name as student_last_name'),
            DB::raw('student_users.email as student_email'),
            DB::raw('tutor_users.email as tutor_email'),
            DB::raw('tutor_users.phone as tutor_phone'),
            DB::raw('student_users.user_id as student_id'),
            DB::raw('student_users.phone'),'tutors.tutor_type','subjects.subject_name','sessions.date',
            DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.subject_id','sessions.grade_id','sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','sessions.session_id','sessions.comment','sessions.promo');
        $sessions_details = $sessions_details->first();

        $session_payment_details = DB::table('payments');
        $session_payment_details->where('payments.session_id', '=', $session_id);
        $session_payment_details->join("packages","packages.package_id","=","payments.package_id");
        $session_payment_details->select(DB::raw('packages.package_type'),DB::raw('packages.is_charged'),DB::raw('sum(payments.hours) as hours'),DB::raw('payments.per_hour'),'payments.per_hour_tutor');
        $session_payment_details = $session_payment_details->first();


        $sessions_details->per_hour = $session_payment_details->per_hour;
        $sessions_details->is_charged = $session_payment_details->is_charged;


        /*
         * Tutor earning amount query
         */

        $subject_id =  $sessions_details->subject_id;
        $grade_id =   $sessions_details->grade_id;
        $tutor_type = $sessions_details->tutor_type;

        $pro_tutor_amount = DB::table('grade_subjects')
            ->where('subject_id',"=",$subject_id)
            ->where('grade_id',"=",$grade_id)
            ->select('pro_tutor_amount','peer_tutor_amount')
            ->first();


        $student_id = $sessions_details->student_id;

        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=',$subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($session_payment_details->package_type == 1) {
            $package_count = DB::table('packages')
                ->where('subject_grade_id', '=',$grade_subjects_id->id )
                ->where('student_id', '=', $student_id)
                ->where('package_type', '=', 1)
                ->select(DB::raw("sum(total_hours) as all_count"))
                ->first();


            $package_count = $package_count->all_count;


            //need to add stubject_grade_id to the packages db.

            $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");



            $sessions_details->remaining_hours = (10 - ($package_count - $used_session[0]->hours));
        } else {
            $sessions_details->remaining_hours = "";
        }



        if($session_payment_details->package_type ==1){

            $sessions_details->student_payment = 10 *($session_payment_details->per_hour);
            $sessions_details->package_type = "Package";

            //$sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;

        }else{
            $sessions_details->student_payment = ($session_payment_details->hours)*($session_payment_details->per_hour);
            $sessions_details->package_type = "Hourly";

            if($tutor_type=="PRO"){
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            }else{
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;
            }
        }
        if($sessions_details->promo != '') {
            $promo_values = DB::table('promo')
                ->where('promo', '=',$sessions_details->promo)
                ->select("discount","status")
                ->first();
            if($promo_values->status == 1 && $promo_values->discount > 0) {
                $student_payment_after_promo = ($sessions_details->student_payment) - (($sessions_details->student_payment) * ($promo_values->discount/100));
                $sessions_details->discount = "$".($sessions_details->student_payment) * ($promo_values->discount/100);
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $student_payment_after_promo;
            } else {
                $sessions_details->discount = 'Invalid';
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $sessions_details->student_payment;
            }


        } else {
            $sessions_details->discount = 'N/A';
            $sessions_details->coupon_code = 'N/A';
            $sessions_details->final_total = $sessions_details->student_payment;
        }


        if(empty($sessions_details)) {
            return redirect()->action('SSessionsController@declined_listing');
        } else {
            return view('sessions.sessionsDeclinedById', compact('sessions_details'));
        }

    }
    public function cancell_session($session_id) {
        DB::table('sessions')
            ->where('session_id', $session_id)
            ->update(array('is_completed' => '3'));
        return redirect()->action('SessionsController@cancelled_listing');

    }

    public function not_assigned($session_id){



        //  dd($sessions_details);
        $sessions_details = DB::table('sessions');
        $sessions_details->where('sessions.session_id',"=",$session_id);
        $sessions_details->where('sessions.is_completed', "=" ,  '-1')
            ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
            ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
        $sessions_details->join("address","address.user_id","=","student_users.user_id");
        $sessions_details->join("grades","grades.grade_id","=","sessions.grade_id");
        $sessions_details->select('grades.grade AS grade_name',
            DB::raw('student_users.first_name as student_first_name'),
            DB::raw('student_users.last_name as student_last_name'),
            DB::raw('student_users.email as student_email'),
            DB::raw('student_users.user_id as student_id'),
            DB::raw('student_users.phone'),'subjects.subject_name','sessions.date',
            DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.subject_id','sessions.grade_id','sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','sessions.session_id','sessions.promo');
        $sessions_details = $sessions_details->first();

        $session_payment_details = DB::table('payments');
        $session_payment_details->where('payments.session_id', '=', $session_id);
        $session_payment_details->join("packages","packages.package_id","=","payments.package_id");
        $session_payment_details->select(DB::raw('packages.package_type'),DB::raw('packages.is_charged'),DB::raw('sum(payments.hours) as hours'),DB::raw('payments.per_hour'),'payments.per_hour_tutor');
        $session_payment_details = $session_payment_details->first();



        $sessions_details->per_hour = $session_payment_details->per_hour;
        $sessions_details->is_charged = $session_payment_details->is_charged;


        /*
         * Tutor earning amount query
         */

        $subject_id =  $sessions_details->subject_id;
        $grade_id =   $sessions_details->grade_id;

        $pro_tutor_amount = DB::table('grade_subjects')
            ->where('subject_id',"=",$subject_id)
            ->where('grade_id',"=",$grade_id)
            ->select('pro_tutor_amount','peer_tutor_amount')
            ->first();


        $student_id = $sessions_details->student_id;

        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=',$subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($session_payment_details->package_type == 1) {
            $package_count = DB::table('packages')
                ->where('subject_grade_id', '=',$grade_subjects_id->id )
                ->where('student_id', '=', $student_id)
                ->where('package_type', '=', 1)
                ->select(DB::raw("sum(total_hours) as all_count"))
                ->first();


            $package_count = $package_count->all_count;


            //need to add stubject_grade_id to the packages db.

            $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");



            $sessions_details->remaining_hours = (10 - ($package_count - $used_session[0]->hours));
        } else {
            $sessions_details->remaining_hours = "";
        }



        if($session_payment_details->package_type ==1){

            $sessions_details->student_payment = ($session_payment_details->hours) *($session_payment_details->per_hour);
            $sessions_details->package_type = "Package";

            //$sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;

        }else{
            $sessions_details->student_payment = ($session_payment_details->hours)*($session_payment_details->per_hour);
            $sessions_details->package_type = "Hourly";


        }
        if($sessions_details->promo != '') {

            $promo_values = DB::table('promo')
                ->where('promo', '=',$sessions_details->promo)
                ->select("discount","status")
                ->first();
           if(!empty($promo_values)) {
            if($promo_values->status == 1 && $promo_values->discount > 0) {
                $student_payment_after_promo = ($sessions_details->student_payment) - (($sessions_details->student_payment) * ($promo_values->discount/100));
                $sessions_details->discount = "$".($sessions_details->student_payment) * ($promo_values->discount/100);
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $student_payment_after_promo;
            } else {
                $sessions_details->discount = 'Invalid';
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $sessions_details->student_payment;
            }
           } else {
               $sessions_details->discount = 'Invalid';
               $sessions_details->coupon_code = $sessions_details->promo;
               $sessions_details->final_total = $sessions_details->student_payment;
           }


        } else {
            $sessions_details->discount = 'N/A';
            $sessions_details->coupon_code = 'N/A';
            $sessions_details->final_total = $sessions_details->student_payment;
        }



        return view('sessions.notAssigned', compact('sessions_details'));

    }

    public function to_excel(Request $request)
    {

        if($request->input('statusEx') == 'all') {
            $sessions = DB::table('sessions')
                ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
                ->join(DB::raw("users as student_users"), "sessions.student_id", "=", "student_users.user_id")
                ->join("address", "address.user_id", "=", "student_users.user_id")
                ->join("payments", "payments.session_id", "=", "sessions.session_id")
                ->leftJoin(DB::raw("users as tutor_users"), function ($join) {
                    $join->on(DB::raw("`sessions`.`tutor_id`"), '=', DB::raw("`tutor_users`.`user_id`"))
                        ->on(DB::raw("`sessions`.`tutor_id`"), '!=', DB::raw(0));
                });

            if($request->input('tutorNameEx') !='') {
                $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$request->input('tutorNameEx').'%');
            }
            if($request->input('studentNameEx') !='') {
                $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$request->input('studentNameEx').'%');
            }
            if($request->input('locationEx') !='') {
                $sessions->where('sessions.street', "like" , '%'.$request->input('locationEx').'%');
            }
            if($request->input('sessionDateEx') !='') {

                $sessions->where('sessions.date', "=" ,  $request->input('sessionDateEx'));
            }

            $sessions->select(DB::raw("sessions.end_time - sessions.start_time AS duration"), 'sessions.student_id','sessions.date', 'sessions.tutor_id', 'sessions.start_time', 'sessions.end_time', 'sessions.street', 'sessions.is_completed','subjects.subject_name','sessions.session_id',
                DB::raw("tutor_users.first_name as tutor_first_name"),DB::raw("tutor_users.last_name as tutor_last_name"),DB::raw("student_users.first_name as student_first_name"),DB::raw("student_users.last_name  as student_last_name"));
            $sessions->groupBy('sessions.session_id');

            $sessions = $sessions->get();
        } else {

            if($request->input('sessionDateEx') != '') {
                $session_date   =   date('Y-m-d', strtotime($request->input('sessionDateEx')));
            } else {
                $session_date = "";
            }
            if($request->input('statusEx') == '-2' ) {
                $sessions = DB::table('sessions');
                $sessions->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
                    ->leftJoin(DB::raw("users as tutor_users"), function ($join) {
                        $join->on(DB::raw("`sessions`.`tutor_id`"), '=', DB::raw("`tutor_users`.`user_id`"))
                            ->on(DB::raw("`sessions`.`tutor_id`"), '!=', DB::raw(0));
                    })
                    ->join(DB::raw("users as student_users"), "sessions.student_id", "=", "student_users.user_id")
                    ->join("address", "address.user_id", "=", "student_users.user_id")
                    ->whereNotIn('sessions.session_id', function ($query) {
                        $query->select(DB::raw('session_id'))
                            ->from('payments');
                    });
            } else {
                $sessions = DB::table('sessions')
                    ->where('sessions.is_completed', "=", $request->input('statusEx'))
                    ->join(DB::raw("subjects"), "sessions.subject_id", "=", "subjects.subject_id")
                    ->join(DB::raw("users as tutor_users"), "sessions.tutor_id", "=", "tutor_users.user_id")
                    ->join(DB::raw("users as student_users"), "sessions.student_id", "=", "student_users.user_id")
                    ->join("payments", "payments.session_id", "=", "sessions.session_id")
                    ->join("address", "address.user_id", "=", "student_users.user_id");
            }



            if($request->input('tutorNameEx') !='') {
                $sessions->where(DB::raw("CONCAT(tutor_users.first_name, ' ', tutor_users.last_name)"), "like" ,  '%'.$request->input('tutorNameEx').'%');
            }
            if($request->input('studentNameEx') !='') {
                $sessions->where(DB::raw("CONCAT(student_users.first_name, ' ', student_users.last_name)"), "like" ,  '%'.$request->input('studentNameEx').'%');
            }
            if($request->input('locationEx') !='') {
                $sessions->where('sessions.street', "like" , '%'.$request->input('locationEx').'%');
            }
            if($request->input('sessionDateEx') !='') {

                $sessions->where('sessions.date', "=" , $session_date );
            }

            $sessions->select(DB::raw('tutor_users.first_name as tutor_first_name'),DB::raw('tutor_users.last_name as tutor_last_name'),DB::raw('student_users.first_name as student_first_name'),DB::raw('student_users.last_name as student_last_name'),'subjects.subject_name','sessions.date',DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.session_id');

            $sessions = $sessions->get();

        }

        $all_session_data = array();
        $i = 1;

        foreach($sessions as $each_session){
            $all_session_data[$i]['tutor_name'] = $each_session->tutor_first_name?$each_session->tutor_first_name.' '.$each_session->tutor_last_name:'-';// DB::table('users')->select(DB::raw("CONCAT(first_name, ' ', last_name) as tutor_name"))->where('user_id', '=', $each_session->tutor_id)->first();
            // $all_session_data[$i]['tutor_name'] = $tutor_name;//->tutor_name;
            $all_session_data[$i]['student_name'] = $each_session->student_first_name.' '.$each_session->student_last_name;//DB::table('users')->select(DB::raw("CONCAT(first_name, ' ', last_name) as student_name"))->where('user_id', '=', $each_session->student_id)->first();
            // $all_session_data[$i]['student_name'] = $student_name->student_name;
            $all_session_data[$i]['subject'] = $each_session->subject_name;
            $all_session_data[$i]['date'] = date('F j, Y', strtotime($each_session->date));
            $all_session_data[$i]['duration'] = $each_session->duration.' Hour';
            $all_session_data[$i]['time'] = date("g a", strtotime($each_session->start_time.':00')).'-'.date("g a", strtotime($each_session->end_time.':00'));

            if($each_session->is_completed == 0) {
                $all_session_data[$i]['status'] = 'Scheduled';
            } elseif($each_session->is_completed == 1) {
                $all_session_data[$i]['status'] = 'Completed';
            }elseif($each_session->is_completed == 2) {
                $all_session_data[$i]['status'] = 'Declined';
            }elseif($each_session->is_completed == 3) {
                $all_session_data[$i]['status'] = 'Cancelled';
            }elseif($each_session->is_completed == -1) {
                $all_session_data[$i]['status'] = 'No Tutor';
            }

            $all_session_data[$i]['location'] = $each_session->street;
            $i++;

        }


            if($request->input('statusEx') ==  '-2') {
                $excel_file_name = "Abandoned sessions";
            }else if($request->input('statusEx') == '3') {
                $excel_file_name = "Cancelled sessions";
            }else if($request->input('statusEx') == '1') {
                $excel_file_name = "Completed sessions";
            }else if($request->input('statusEx') == '2') {
                $excel_file_name = "Declined sessions";
            }else if($request->input('statusEx') == '0') {

                $excel_file_name = "Scheduled sessions";
            }else if($request->input('statusEx') == 'all') {

                $excel_file_name = "All sessions";
            }

            Excel::create($excel_file_name, function($excel) use($all_session_data)  {

            $excel->sheet('details list', function($sheet) use($all_session_data)  {

                $sheet->fromArray($all_session_data);

            });

        })->download('xls');

        return Redirect::back();

    }

}
