<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Http\Middleware\Authenticate;
use Illuminate\Support\Facades\URL;
//use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;

class TutorsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware('role');
    }

    public function application_approved_email($user_id) {


        if($user_id != '') {
            $user_data = DB::table('users')
                ->where('user_id', '=', $user_id)
                ->select('email','first_name','last_name')
                ->first();
            if(!empty($user_data)) {
                $data = array('first_name'=>$user_data->first_name,'last_name'=>$user_data->last_name);

                Mail::send('email.applicationApproved', $data, function($message) use ($user_data)
                {
                    $message->from('info@tutorsync.com', 'Tutor Sync');
                    $message->subject('In-Home Tutor Application Approved');
                    $message->to($user_data->email);
                    $message->bcc('sherine@tutorsync.com');
                });
            }
        }
    }


    public function application_rejected_email($user_id) {


        if($user_id != '') {
            $user_data = DB::table('users')
                ->where('user_id', '=', $user_id)
                ->select('email','first_name','last_name')
                ->first();
            if(!empty($user_data)) {
                $data = array('first_name'=>$user_data->first_name,'last_name'=>$user_data->last_name);

                Mail::send('email.applicationRejected', $data, function($message) use ($user_data)
                {
                    $message->from('info@tutorsync.com', 'Tutor Sync');
                    $message->subject('In-Home Tutor Application Rejected');
                    $message->to($user_data->email);
                    $message->bcc('sherine@tutorsync.com');
                });
            }
        }
    }


    public function tutor_assigned_email($session_id) {

       if($session_id != '') {

        $sessions_details = DB::table('sessions');
        $sessions_details->where('sessions.session_id',"=",$session_id);
        $sessions_details->where('sessions.is_completed', "=" ,  '0');
        $sessions_details->join(DB::raw("subjects"),"sessions.subject_id","=","subjects.subject_id");
        $sessions_details->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id");
        $sessions_details->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id");
        $sessions_details->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
        $sessions_details->join("address","address.user_id","=","student_users.user_id");
        $sessions_details->join("grades","grades.grade_id","=","sessions.grade_id");
        $sessions_details->select('grades.grade AS grade_name',DB::raw('tutor_users.first_name as tutor_first_name'),
            DB::raw('tutor_users.last_name as tutor_last_name'),
            DB::raw('student_users.first_name as student_first_name'),
            DB::raw('student_users.last_name as student_last_name'),
            DB::raw('tutor_users.email as tutor_email'),
            DB::raw('student_users.email as student_email'),
            DB::raw('student_users.user_id as student_id'),
            DB::raw('student_users.phone'),'tutors.gender','tutors.tutor_type','subjects.subject_name','sessions.date','sessions.session_id',
            DB::raw("sessions.end_time - sessions.start_time AS duration"),'sessions.subject_id','sessions.grade_id','sessions.start_time', 'sessions.end_time','sessions.is_completed','sessions.street','sessions.state','sessions.city','sessions.rate','address.city','address.state','tutors.gender','tutors.profile_image','sessions.promo');
        $sessions_details = $sessions_details->first();




        $session_payment_details = DB::table('payments');
        $session_payment_details->where('payments.session_id', '=', $session_id);
        $session_payment_details->join("packages","packages.package_id","=","payments.package_id");
        $session_payment_details->select(DB::raw('packages.package_type'),DB::raw('packages.is_charged'),DB::raw('sum(payments.hours) as hours'),DB::raw('payments.per_hour'),'payments.per_hour_tutor');
        $session_payment_details = $session_payment_details->first();


        $sessions_details->per_hour = $session_payment_details->per_hour;
        $sessions_details->is_charged = $session_payment_details->is_charged;


        $subject_id =  $sessions_details->subject_id;
        $grade_id =   $sessions_details->grade_id;
        $tutor_type = $sessions_details->tutor_type;

        $pro_tutor_amount = DB::table('grade_subjects')
            ->where('subject_id',"=",$subject_id)
            ->where('grade_id',"=",$grade_id)
            ->select('pro_tutor_amount','peer_tutor_amount')
            ->first();


        $student_id = $sessions_details->student_id;

        // package total amount

        $package_tot_hours = DB::table('packages')
            ->where('student_id', '=', $student_id)
            ->select(DB::raw('total_hours,subject_grade_id'))
            ->first();

        $package_total_hours = $package_tot_hours->total_hours;

        $package_10_rate = DB::table('grade_subjects')
            ->where('id', '=', $package_tot_hours->subject_grade_id)
            ->select(DB::raw('package_10_rate'))
            ->first();

        $package_total_amount = ($package_10_rate->package_10_rate * $package_total_hours);
        $sessions_details->package_total_amount = $package_total_amount;

        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=',$subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($session_payment_details->package_type == 1) {
            $package_count = DB::table('packages')
                ->where('subject_grade_id', '=',$grade_subjects_id->id )
                ->where('student_id', '=', $student_id)
                ->where('package_type', '=', 1)
                ->select(DB::raw("sum(total_hours) as all_count"))
                ->first();

            $package_count = $package_count->all_count;

            //need to add stubject_grade_id to the packages db.

            $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");

            $sessions_details->remaining_hours = (10 - ($package_count - $used_session[0]->hours));
        } else {
            $sessions_details->remaining_hours = "";
        }

        if($session_payment_details->package_type ==1){

            $sessions_details->student_payment = 10 *($session_payment_details->per_hour);
            $sessions_details->package_type = "Package";

            $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;

        }else{
            $sessions_details->student_payment = ($session_payment_details->hours)*($session_payment_details->per_hour);
            $sessions_details->package_type = "Hourly";

            if($tutor_type=="PRO"){
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->pro_tutor_amount * $session_payment_details->hours;
            }else{
                $sessions_details->tutor_earning_amount = $pro_tutor_amount->peer_tutor_amount * $session_payment_details->hours;
            }
        }
        if($sessions_details->promo != '') {

            $promo_values = DB::table('promo')
                ->where('promo', '=',$sessions_details->promo)
                ->select("discount","status")
                ->first();
            if($promo_values->status == 1 && $promo_values->discount > 0) {
                $student_payment_after_promo = ($sessions_details->student_payment) - (($sessions_details->student_payment) * ($promo_values->discount/100));
                $sessions_details->discount = "$".($sessions_details->student_payment) * ($promo_values->discount/100);
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $student_payment_after_promo;
                $sessions_details->is_promo = 1;
            } else {
                $sessions_details->discount = 'Invalid';
                $sessions_details->coupon_code = $sessions_details->promo;
                $sessions_details->final_total = $sessions_details->student_payment;
                $sessions_details->is_promo = 0;
            }


        } else {
            $sessions_details->is_promo = 0;
            $sessions_details->discount = 'N/A';
            $sessions_details->coupon_code = 'N/A';
            $sessions_details->final_total = $sessions_details->student_payment;
        }


        if($sessions_details->is_charged== 0){
            $is_charged = "not paid";
        }else{
            $is_charged = "paid";
        }

        $data = array(
            'date'=>$sessions_details->date,
            'start_time'=>$sessions_details->start_time,
            'end_time'=>$sessions_details->end_time,
            'subject_name'=>$sessions_details->subject_name,
            'grade'=>$sessions_details->grade_name,
            'tutor_first_name'=>$sessions_details->tutor_first_name,
            'tutor_last_name'=>$sessions_details->tutor_last_name,
            'student_first_name'=>$sessions_details->student_first_name,
            'student_last_name'=>$sessions_details->student_last_name,
            'tutor_email' =>$sessions_details->tutor_email,
            'student_email' =>$sessions_details->student_email,
            'street'=>$sessions_details->street,
            'state'=>$sessions_details->state,
            'city'=>$sessions_details->city,
            'tutor_earnings'=>$sessions_details->tutor_earning_amount,
            'student_payment' => $sessions_details->student_payment,
            'package_type' => $sessions_details->package_type,
            'is_promo' => $sessions_details->is_promo,
            'final_total' => $sessions_details->final_total
        );


        Mail::send('email.tutorAssignedEmail', $data, function($message) use ($data)
        {
            $message->from('info@tutorsync.com', 'Tutor Sync');
            $subject_email = "Confirmed - ".$data['subject_name']." Session with TutorSync in ".$data['city'];
            $message->subject($subject_email);
            $message->to($data['tutor_email']);
            $message->bcc('sherine@tutorsync.com');

        });

        Mail::send('email.student_assign', $data, function($message) use ($data)
        {
            $message->from('info@tutorsync.com', 'Tutor Sync');
            $subject_email = "Confirmed - ".$data['subject_name']." Tutor with TutorSync";
            $message->subject($subject_email);
            $message->to($data['student_email']);
            $message->bcc('sherine@tutorsync.com');

        });

        Mail::send('email.admin_session_assign', $data, function($message) use ($data)
        {
            $message->from('info@tutorsync.com', 'Tutor Sync');
            if($data == 'Hourly') {
                $subject_email = "Confirmed - Session Purchased by ".$data['student_first_name']." ".$data['student_last_name'];
            } else {
                $subject_email = "Confirmed - Package Purchased by ".$data['student_first_name']." ".$data['student_last_name'];
            }



            $message->subject($subject_email);
            $message->to('info@tutorsync.com');
            $message->bcc('sherine@tutorsync.com');

        });
       }

    }
    public function  index(){

        $url_param = $_GET;

        $tutors = DB::table('tutors')
            ->join('users', 'tutors.user_id', '=', 'users.user_id')
            ->join('address', 'tutors.user_id', '=', 'address.user_id');
        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if($url_param['tutor_name'] != '') {
                    $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }
                if(!empty($url_param['subject_name'])){
                    $tutors->where('subjects.subject_name', 'like',  '%'.$url_param['subject_name'].'%');
                }
                if(!empty($url_param['tutor_type'])){
                    $tutors->where('tutors.tutor_type','=',  $url_param['tutor_type']);
                }
                if($url_param['status'] != ""){
                    $tutors->where('tutors.is_approved','=',  $url_param['status']);
                }
                if(!empty($url_param['location'])){
                    $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$url_param['location'].'%');
                }
            }
        }


        //Paginator
        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');
        //$tutors->paginate(100);
        $tutors->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"));

        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $tutors = $tutors->paginate(100);
        } else {
            $tutors = $tutors->paginate(100);
        }



// list all users, with 10 users per page, on page 5

        $all_tuto_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $all_tuto_data[$i]['tutor_id'] = $tutor->user_id;
            $all_tuto_data[$i]['first_name'] = $tutor->first_name;
            $all_tuto_data[$i]['last_name'] = $tutor->last_name;
            $all_tuto_data[$i]['user_id'] = $tutor->user_id;
            $all_tuto_data[$i]['email'] = $tutor->email;
            $all_tuto_data[$i]['contact_number'] = $tutor->phone;

            $all_tuto_data[$i]['tutor_type'] = $tutor->tutor_type;
            $all_tuto_data[$i]['street'] = $tutor->street;
            $all_tuto_data[$i]['city'] = $tutor->city;
            $all_tuto_data[$i]['state'] = $tutor->state;
            $all_tuto_data[$i]['is_approved'] = $tutor->is_approved;
            $all_tuto_data[$i]['created_at'] = $tutor->created_at;
            $all_tuto_data[$i]['miles_travel'] = $tutor->miles_travel;



            /**  ----------------------rating---------------------------------- **/

            $rating_total = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("SUM(rating) as sum"));

            $rating_total = $rating_total->first();
            $rating_total = intval($rating_total->sum);

            $tutoring_sessions = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("COUNT(rating) as count"));

            $tutoring_sessions = $tutoring_sessions->first();
            $tutoring_sessions = intval($tutoring_sessions->count);


            if($tutoring_sessions>0){
                $tutor_rating = intval(ceil($rating_total/$tutoring_sessions));
            }else{
                $tutor_rating=0;
            }

            $all_tuto_data[$i]['rating'] = $tutor_rating;
            $all_tuto_data[$i]['rating_count'] =$tutoring_sessions;

            /**  -------------------------------------------------------- **/


            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name','subjects.subject_id')
                ->get();

            $all_tuto_data[$i]['subjects'] = array();

            foreach($tutors_subjects as $tutor_subj) {

                array_push($all_tuto_data[$i]['subjects'], array('subject_id'=>$tutor_subj->subject_id,'subject_name'=>$tutor_subj->subject_name));
            }

            $i++;
        }

        if(!empty($url_param) && count($url_param) > 1) {
            $tutors->setPath('?tutor_name='.$url_param['tutor_name'].'&tutor_type='.$url_param['tutor_type'].'&subject_name='.$url_param['subject_name'].'&location='.$url_param['location'].'&status='.$url_param['status'].'&');
        } else {
            $tutors->setPath('');
        }

        return view('tutors.all', ['tutors' => $all_tuto_data,'paginations'=>$tutors]);

    }

    /**
     * filter all approved/ un-approved tutors in Tutors.
     *
     * @param  Request  $request
     * @return Response
     */

    public function  filter_all_tutors_by_category(Request $request)
    {

        $tutors_name    =   $request->input('tutorName');
        $tutors_type    =   $request->input('tutorType');
        $subject_name   =   $request->input('subjectName');
        $location       =   $request->input('location');
        $status         =   $request->input('status');

        $tutors = DB::table('users')
            ->join('address', 'address.user_id', '=', 'users.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'address.user_id');
        if($tutors_name != '') {
            $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$tutors_name.'%');
        }
        if(!empty($subject_name)){
            $tutors->where('subjects.subject_name', 'like',  '%'.$subject_name.'%');
        }
        if(!empty($tutors_type)){
            $tutors->where('tutors.tutor_type','=',  $tutors_type);
        }
        if($status != ""){
            $tutors->where('tutors.is_approved','=',  $status);
        }
        if(!empty($location)){
            $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$location.'%');
        }

        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');

        if(!empty($request->input('sort_method'))){

            $sort_value     =   $request->input('sort_value');
            $sort_method    =   $request->input('sort_method');

            if( $sort_value == 'sort_name'){
                $tutors->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), $sort_method);
            }
            if( $sort_value == 'sort_type'){
                $tutors->orderBy('tutors.tutor_type', $sort_method);
            }
            if( $sort_value == 'sort_location'){
                $tutors->orderBy(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), $sort_method);
            }
            if( $sort_value == 'sort_status'){
                $tutors->orderBy('tutors.is_approved', $sort_method);
            }
            if( $sort_value == 'sort_date'){
                $tutors->orderBy('tutors.created_at', $sort_method);
            }
            if( $sort_value == 'sort_willing_to_travel'){
                $tutors->orderBy('tutors.miles_travel', $sort_method);
            }

        }

        $tutors->groupBy('users.user_id');

        $tutors = $tutors->paginate(100);

        $all_tuto_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $all_tuto_data[$i]['tutor_id'] = $tutor->user_id;
            $all_tuto_data[$i]['first_name'] = $tutor->first_name;
            $all_tuto_data[$i]['last_name'] = $tutor->last_name;
            $all_tuto_data[$i]['user_id'] = $tutor->user_id;
            $all_tuto_data[$i]['email'] = $tutor->email;
            $all_tuto_data[$i]['contact_number'] = $tutor->phone;
            $all_tuto_data[$i]['tutor_type'] = $tutor->tutor_type;
            $all_tuto_data[$i]['street'] = $tutor->street;
            $all_tuto_data[$i]['city'] = $tutor->city;
            $all_tuto_data[$i]['state'] = $tutor->state;
            $all_tuto_data[$i]['is_approved'] = $tutor->is_approved;
            $all_tuto_data[$i]['created_at'] = $tutor->created_at;
            $all_tuto_data[$i]['miles_travel'] = $tutor->miles_travel;


            /**  ----------------------rating---------------------------------- **/

            $rating_total = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("SUM(rating) as sum"));

            $rating_total = $rating_total->first();
            $rating_total = intval($rating_total->sum);

            $tutoring_sessions = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("COUNT(rating) as count"));

            $tutoring_sessions = $tutoring_sessions->first();
            $tutoring_sessions = intval($tutoring_sessions->count);


            if($tutoring_sessions>0){
                $tutor_rating = intval(ceil($rating_total/$tutoring_sessions));
            }else{
                $tutor_rating=0;
            }

            $all_tuto_data[$i]['rating'] = $tutor_rating;
            $all_tuto_data[$i]['rating_count'] =$tutoring_sessions;

            /**  -------------------------------------------------------- **/



            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name','subjects.subject_id')
                ->get();

            $all_tuto_data[$i]['subjects'] = array();

            foreach($tutors_subjects as $tutor_subj) {

                //array_push($all_tuto_data[$i]['subjects'], $tutor_subj->subject_name);
                array_push($all_tuto_data[$i]['subjects'], array('subject_id'=>$tutor_subj->subject_id,'subject_name'=>$tutor_subj->subject_name));

            }

            $i++;

        }

        if(!empty($request->input('sort_method'))) {
            $tutors->setPath('?tutor_name=' . $tutors_name . '&tutor_type=' . $tutors_type . '&subject_name=' . $subject_name . '&location=' . $location . '&status=' . $status . '&sort_method' . $sort_method . '&sort_value' . $sort_value . '&');
        } else {
            $tutors->setPath('?tutor_name='.$tutors_name.'&tutor_type='.$tutors_type.'&subject_name='.$subject_name.'&location='.$location.'&status='.$status.'&');
        }
//        echo "<pre>";
//        print_r($all_tuto_data);exit;
        return view('tutors.all', ['tutors' => $all_tuto_data,'paginations'=>$tutors]);

    }

    public function  new_applications()
    {
        $url_param = $_GET;
        $tutors = DB::table('tutors')
            ->join('users', 'tutors.user_id', '=', 'users.user_id')
            ->join('address', 'tutors.user_id', '=', 'address.user_id')
            ->where('tutors.is_approved', '=',0);



        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if($url_param['tutor_name'] != '') {
                    $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }
                if(!empty($url_param['subject_name'])){
                    $tutors->where('subjects.subject_name', 'like',  '%'.$url_param['subject_name'].'%');
                }
                if(!empty($url_param['tutor_type'])){
                    $tutors->where('tutors.tutor_type','=',  $url_param['tutor_type']);
                }

                if(!empty($url_param['location'])){
                    $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$url_param['location'].'%');
                }
            }
        }

        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');

        $tutors->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"));
        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $tutors = $tutors->paginate(100);
        } else {
            $tutors = $tutors->paginate(100);
        }
        $all_tuto_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $all_tuto_data[$i]['tutor_id'] = $tutor->user_id;
            $all_tuto_data[$i]['first_name'] = $tutor->first_name;
            $all_tuto_data[$i]['last_name'] = $tutor->last_name;
            $all_tuto_data[$i]['user_id'] = $tutor->user_id;
            $all_tuto_data[$i]['email'] = $tutor->email;
            $all_tuto_data[$i]['contact_number'] = $tutor->phone;
            $all_tuto_data[$i]['tutor_type'] = $tutor->tutor_type;
            $all_tuto_data[$i]['street'] = $tutor->street;
            $all_tuto_data[$i]['city'] = $tutor->city;
            $all_tuto_data[$i]['state'] = $tutor->state;
            $all_tuto_data[$i]['is_approved'] = $tutor->is_approved;
            $all_tuto_data[$i]['created_at'] = $tutor->created_at;
            $all_tuto_data[$i]['miles_travel'] = $tutor->miles_travel;





            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name','subjects.subject_id')
                ->get();

            $all_tuto_data[$i]['subjects'] = array();

            foreach($tutors_subjects as $tutor_subj) {

                //array_push($all_tuto_data[$i]['subjects'], $tutor_subj->subject_name);
                array_push($all_tuto_data[$i]['subjects'], array('subject_id'=>$tutor_subj->subject_id,'subject_name'=>$tutor_subj->subject_name));

            }

            $i++;
        }
        if(!empty($url_param) && count($url_param) > 1) {
            $tutors->setPath('?tutor_name='.$url_param['tutor_name'].'&tutor_type='.$url_param['tutor_type'].'&subject_name='.$url_param['subject_name'].'&location='.$url_param['location'].'&');
        } else {
            $tutors->setPath('');
        }
        return view('tutors.new_applications',['tutors' => $all_tuto_data,'paginations'=>$tutors]);
    }

    /**
     * filter all   un-approved applications in Tutors.
     *
     * @param  Request  $request
     * @return Response
     */

    public function  filter_all_new_application_by_category(Request $request)
    {

        $tutors_name    =   $request->input('tutorName');
        $tutors_type    =   $request->input('tutorType');
        $subject_name   =   $request->input('subjectName');
        $location       =   $request->input('location');
        // $status         =   $request->input('status');

        $tutors = DB::table('users')
            ->join('address', 'address.user_id', '=', 'users.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'address.user_id')
        /*
            ->join('tutoring_subjects', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
            ->join('subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id') */
            ->where('tutors.is_approved', '=', 0);
        if($tutors_name != '') {
            //$tutors->where('users.first_name', 'like', '%'.$tutors_name.'%');
            $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$tutors_name.'%');
        }
        if(!empty($subject_name)){
            $tutors->where('subjects.subject_name', 'like',  '%'.$subject_name.'%');
        }
        if(!empty($tutors_type)){
            $tutors->where('tutors.tutor_type','=',  $tutors_type);
        }
//        if($status != ""){
//            $tutors->where('tutors.is_approved','=',  $status);
//        }
        if(!empty($location)){
            $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$location.'%');
        }

        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');

        if(!empty($request->input('sort_method'))){

            $sort_value     =   $request->input('sort_value');
            $sort_method    =   $request->input('sort_method');

            if( $sort_value == 'sort_name'){
                $tutors->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), $sort_method);
            }
            if( $sort_value == 'sort_type'){
                $tutors->orderBy('tutors.tutor_type', $sort_method);
            }
            if( $sort_value == 'sort_location'){
                $tutors->orderBy(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), $sort_method);
            }
            if( $sort_value == 'sort_status'){
                $tutors->orderBy('tutors.is_approved', $sort_method);
            }
            if( $sort_value == 'sort_date'){
                $tutors->orderBy('tutors.created_at', $sort_method);
            }
            if( $sort_value == 'sort_willing_to_travel'){
                $tutors->orderBy('tutors.miles_travel', $sort_method);
            }

        }
        $tutors->groupBy('users.user_id');

        $tutors =$tutors->paginate(100);

        $all_tuto_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $all_tuto_data[$i]['tutor_id'] = $tutor->user_id;
            $all_tuto_data[$i]['first_name'] = $tutor->first_name;
            $all_tuto_data[$i]['last_name'] = $tutor->last_name;
            $all_tuto_data[$i]['user_id'] = $tutor->user_id;
            $all_tuto_data[$i]['email'] = $tutor->email;
            $all_tuto_data[$i]['contact_number'] = $tutor->phone;
            $all_tuto_data[$i]['tutor_type'] = $tutor->tutor_type;
            $all_tuto_data[$i]['street'] = $tutor->street;
            $all_tuto_data[$i]['city'] = $tutor->city;
            $all_tuto_data[$i]['state'] = $tutor->state;
            $all_tuto_data[$i]['is_approved'] = $tutor->is_approved;
            $all_tuto_data[$i]['created_at'] = $tutor->created_at;
            $all_tuto_data[$i]['miles_travel'] = $tutor->miles_travel;

            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name','subjects.subject_id')
                ->get();

            $all_tuto_data[$i]['subjects'] = array();

            foreach($tutors_subjects as $tutor_subj) {

                array_push($all_tuto_data[$i]['subjects'], array('subject_id'=>$tutor_subj->subject_id,'subject_name'=>$tutor_subj->subject_name));

            }
            $i++;
        }
        if(!empty($request->input('sort_method'))) {
            $tutors->setPath('?tutor_name=' . $tutors_name . '&tutor_type=' . $tutors_type . '&subject_name=' . $subject_name . '&location=' . $location . $sort_method . '&sort_value' . $sort_value . '&');
        } else {
            $tutors->setPath('?tutor_name=' . $tutors_name . '&tutor_type=' . $tutors_type . '&subject_name=' . $subject_name . '&location=' . $location . '&');
        }
        return view('tutors.new_applications', ['tutors' => $all_tuto_data,'paginations'=>$tutors]);
    }

    /**
     * List all deactivated tutors.
     * The Deactivated DB status is 2
     *
     * @return Tutors
     */

    public function deactivated_tutors()
    {
        $url_param = $_GET;
        $tutors = DB::table('tutors')
            ->join('users', 'tutors.user_id', '=', 'users.user_id')
            ->join('address', 'tutors.user_id', '=', 'address.user_id')
            ->where('tutors.is_approved', '=',2);


        if(!empty($url_param)) {
            if(count($url_param) > 1) {
                if($url_param['tutor_name'] != '') {
                    $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$url_param['tutor_name'].'%');
                }
                if(!empty($url_param['subject_name'])){
                    $tutors->where('subjects.subject_name', 'like',  '%'.$url_param['subject_name'].'%');
                }
                if(!empty($url_param['tutor_type'])){
                    $tutors->where('tutors.tutor_type','=',  $url_param['tutor_type']);
                }

                if(!empty($url_param['location'])){
                    $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$url_param['location'].'%');
                }
            }
        }
        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');

        $tutors->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"));

        if(isset($url_param['?page'])) {
            $currentPage = $url_param['?page'];

            Paginator::currentPageResolver(function() use ($currentPage) {
                return $currentPage;
            });
            $tutors = $tutors->paginate(100);
        } else {
            $tutors = $tutors->paginate(100);
        }
        $all_tuto_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $all_tuto_data[$i]['tutor_id'] = $tutor->user_id;
            $all_tuto_data[$i]['first_name'] = $tutor->first_name;
            $all_tuto_data[$i]['last_name'] = $tutor->last_name;
            $all_tuto_data[$i]['user_id'] = $tutor->user_id;
            $all_tuto_data[$i]['email'] = $tutor->email;
            $all_tuto_data[$i]['contact_number'] = $tutor->phone;
            $all_tuto_data[$i]['tutor_type'] = $tutor->tutor_type;
            $all_tuto_data[$i]['street'] = $tutor->street;
            $all_tuto_data[$i]['city'] = $tutor->city;
            $all_tuto_data[$i]['state'] = $tutor->state;
            $all_tuto_data[$i]['is_approved'] = $tutor->is_approved;
            $all_tuto_data[$i]['created_at'] = $tutor->created_at;
            $all_tuto_data[$i]['miles_travel'] = $tutor->miles_travel;




            /**  ----------------------rating---------------------------------- **/

            $rating_total = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("SUM(rating) as sum"));

            $rating_total = $rating_total->first();
            $rating_total = intval($rating_total->sum);

            $tutoring_sessions = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("COUNT(rating) as count"));

            $tutoring_sessions = $tutoring_sessions->first();
            $tutoring_sessions = intval($tutoring_sessions->count);


            if($tutoring_sessions>0){
                $tutor_rating = intval(ceil($rating_total/$tutoring_sessions));
            }else{
                $tutor_rating=0;
            }

            $all_tuto_data[$i]['rating'] = $tutor_rating;
            $all_tuto_data[$i]['rating_count'] =$tutoring_sessions;

            /**  -------------------------------------------------------- **/





            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name','subjects.subject_id')
                ->get();

            $all_tuto_data[$i]['subjects'] = array();

            foreach($tutors_subjects as $tutor_subj) {

                array_push($all_tuto_data[$i]['subjects'], array('subject_id'=>$tutor_subj->subject_id,'subject_name'=>$tutor_subj->subject_name));

            }

            $i++;
        }
        if(!empty($url_param) && count($url_param) > 1) {
            $tutors->setPath('?tutor_name='.$url_param['tutor_name'].'&tutor_type='.$url_param['tutor_type'].'&subject_name='.$url_param['subject_name'].'&location='.$url_param['location'].'&');
        } else {
            $tutors->setPath('');
        }
        return view('tutors.deactivated_tutors',['tutors' => $all_tuto_data,'paginations'=>$tutors]);

    }

    /**
     * Filter all   deactivated tutors in Tutors.
     * The Deactivated DB status is 2
     * @param  Request  $request
     * @return Response
     */

    public function  filter_deactivated_tutors_by_category(Request $request)
    {

        $tutors_name    =   $request->input('tutorName');
        $tutors_type    =   $request->input('tutorType');
        $subject_name   =   $request->input('subjectName');
        $location       =   $request->input('location');
        //$status         =   $request->input('status');
        /*
                $tutors = DB::table('tutors')
                    ->leftJoin('users', 'tutors.user_id', '=', 'users.user_id')
                    ->leftJoin('address', 'tutors.user_id', '=', 'address.user_id')
                    ->where('tutors.is_approved', '=',2);*/



        $tutors = DB::table('users')
            ->join('address', 'address.user_id', '=', 'users.user_id')
            ->join('tutors', 'tutors.user_id', '=', 'address.user_id')
         /*   ->join('tutoring_subjects', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
            ->join('subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id') */
            ->where('tutors.is_approved', '=', 2);



        if($tutors_name != '') {
            $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$tutors_name.'%');
        }
        if(!empty($subject_name)){
            $tutors->where('subjects.subject_name', 'like',  '%'.$subject_name.'%');
        }
        if(!empty($tutors_type)){
            $tutors->where('tutors.tutor_type','=',  $tutors_type);
        }
//        if($status != ""){
//            $tutors->where('tutors.is_approved','=',  $status);
//        }
        if(!empty($location)){
            $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$location.'%');
        }

        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');

        if(!empty($request->input('sort_method'))){

            $sort_value     =   $request->input('sort_value');
            $sort_method    =   $request->input('sort_method');

            if( $sort_value == 'sort_name'){
                $tutors->orderBy(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), $sort_method);
            }
            if( $sort_value == 'sort_type'){
                $tutors->orderBy('tutors.tutor_type', $sort_method);
            }
            if( $sort_value == 'sort_location'){
                $tutors->orderBy(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), $sort_method);
            }
            if( $sort_value == 'sort_status'){
                $tutors->orderBy('tutors.is_approved', $sort_method);
            }
            if( $sort_value == 'sort_date'){
                $tutors->orderBy('tutors.created_at', $sort_method);
            }
            if( $sort_value == 'sort_willing_to_travel'){
                $tutors->orderBy('tutors.miles_travel', $sort_method);
            }

        }

        $tutors->groupBy('users.user_id');

        $tutors =$tutors->paginate(100);

        $deactivated_tutor_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $deactivated_tutor_data[$i]['tutor_id'] = $tutor->user_id;
            $deactivated_tutor_data[$i]['first_name'] = $tutor->first_name;
            $deactivated_tutor_data[$i]['last_name'] = $tutor->last_name;
            $deactivated_tutor_data[$i]['user_id'] = $tutor->user_id;
            $deactivated_tutor_data[$i]['email'] = $tutor->email;
            $deactivated_tutor_data[$i]['contact_number'] = $tutor->phone;
            $deactivated_tutor_data[$i]['tutor_type'] = $tutor->tutor_type;
            $deactivated_tutor_data[$i]['street'] = $tutor->street;
            $deactivated_tutor_data[$i]['city'] = $tutor->city;
            $deactivated_tutor_data[$i]['state'] = $tutor->state;
            $deactivated_tutor_data[$i]['is_approved'] = $tutor->is_approved;
            $deactivated_tutor_data[$i]['created_at'] = $tutor->created_at;
            $deactivated_tutor_data[$i]['miles_travel'] = $tutor->miles_travel;




            /**  ----------------------rating---------------------------------- **/

            $rating_total = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("SUM(rating) as sum"));

            $rating_total = $rating_total->first();
            $rating_total = intval($rating_total->sum);

            $tutoring_sessions = DB::table('sessions')
                ->where('tutor_id', '=', $tutor->user_id)
                ->select(DB::raw("COUNT(rating) as count"));

            $tutoring_sessions = $tutoring_sessions->first();
            $tutoring_sessions = intval($tutoring_sessions->count);


            if($tutoring_sessions>0){
                $tutor_rating = intval(ceil($rating_total/$tutoring_sessions));
            }else{
                $tutor_rating=0;
            }

            $deactivated_tutor_data[$i]['rating'] = $tutor_rating;
            $deactivated_tutor_data[$i]['rating_count'] =$tutoring_sessions;

            /**  -------------------------------------------------------- **/







            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name','subjects.subject_id')
                ->get();

            $deactivated_tutor_data[$i]['subjects'] = array();

            foreach($tutors_subjects as $tutor_subj) {
                // array_push($deactivated_tutor_data[$i]['subjects'], $tutor_subj->subject_name);
                array_push($deactivated_tutor_data[$i]['subjects'], array('subject_id'=>$tutor_subj->subject_id,'subject_name'=>$tutor_subj->subject_name));

            }

            $i++;
        }

        if(empty($request->input('sort_method'))) {
            $tutors->setPath('?tutor_name=' . $tutors_name . '&tutor_type=' . $tutors_type . '&subject_name=' . $subject_name . '&location=' . $location . '&');
        } else {
            $tutors->setPath('?tutor_name=' . $tutors_name . '&tutor_type=' . $tutors_type . '&subject_name=' . $subject_name . '&location=' . $location .'&sort_method' . $sort_method . '&sort_value' . $sort_value . '&');
        }
        return view('tutors.deactivated_tutors', ['tutors' => $deactivated_tutor_data,'paginations'=>$tutors]);

    }

    /**
     *
     * Update tutors status in Tutors.
     * Status types are
     *      0= pending,
     *      1=activated,
     *      2=deactivated
     * @param  Request  $request
     * @return Response (status ,user_id)
     *
     */

    public function update_application_status(Request $request){

        $stutus_value =  $request->input('stutus_value');
        $user_id =  $request->input('user_id');

        $is_result =  DB::table('tutors')
            ->where('user_id', $user_id)
            ->update(array('is_approved' => $stutus_value));

        if($stutus_value == 2){

            $arr = array(
                'stutus_value'=>$stutus_value,
                'user_id'=>$user_id
            );
            return $arr;

        }else if($stutus_value == 1){

            $arr = array(
                'stutus_value'=>$stutus_value,
                'user_id'=>$user_id
            );
            $this->application_approved_email($user_id);
            return $arr;

        } else if($stutus_value == 3){

            $arr = array(
                'stutus_value'=>$stutus_value,
                'user_id'=>$user_id
            );
            $this->application_rejected_email($user_id);
            return $arr;

        }else {

            $arr = array(
                'stutus_value'=>$stutus_value,
                'user_id'=>$user_id
            );
            return $arr;
        }

    }


    public function details($id)
    {

        $tutor['details'] = DB::table('users')
            ->join('tutors', 'users.user_id', '=', 'tutors.user_id')
            ->join('address', 'users.user_id', '=', 'address.user_id')
            ->where('users.user_id', '=', $id)
            ->select('users.first_name','users.last_name','users.email','tutors.dob','tutors.gender','tutors.tutor_type','tutors.profile_image','users.phone','address.street','address.city','address.state','address.zip','tutors.experience')
            ->first();
        if(empty($tutor['details'])) {
            return redirect()->action('TutorsController@index');
        }
        $tutor['certificates'] = DB::table('users')
            ->join('certificates', 'users.user_id', '=', 'certificates.user_id')
            ->where('users.user_id', '=', $id)
            ->select('certificates.certificate')
            ->get();

        $tutor['subjects_tested'] = DB::table('users')
            ->join('tutoring_subjects', 'users.user_id', '=', 'tutoring_subjects.user_id')
            ->join('subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
            ->where('users.user_id', '=', $id)
            ->select('subjects.subject_name', 'tutoring_subjects.is_tested', 'tutoring_subjects.subject_id')
            ->get();
//        dd($tutor['subjects_tested']);


        $tutor['earnings'] = DB::table('sessions')
            ->where('sessions.is_completed', "=" ,  '1')
            ->where('sessions.tutor_id', "=" , $id)
            ->join('tutors', 'tutors.user_id', '=', 'sessions.tutor_id')
            ->join('grade_subjects', 'grade_subjects.subject_id', '=', 'sessions.subject_id')
            ->select(DB::raw("SUM(`sessions`.end_time-`sessions`.start_time) AS total_hours"), DB::raw("SUM(IF(`tutors`.tutor_type='PRO',grade_subjects.pro_tutor_amount,grade_subjects.peer_tutor_amount))
        AS totla_earning"))
            ->first();



        $tutor['education'] = DB::table('college')
            ->where('user_id', '=', $id)
            ->select('*')
            ->get();
        //dd($tutor['education']);



        $rating_total = DB::table('sessions')
            ->where('tutor_id', '=', $id)
            ->select(DB::raw("SUM(rating) as sum"));
        $rating_total = $rating_total->first();
        $rating_total = intval($rating_total->sum);

        $tutoring_sessions = DB::table('sessions')
            ->where('tutor_id', '=', $id)
            ->select(DB::raw("COUNT(rating) as count"));

        $tutoring_sessions = $tutoring_sessions->first();
        $tutoring_sessions = intval($tutoring_sessions->count);
        if($tutoring_sessions > 0) {
            $tutor['rating'] = intval(ceil($rating_total/$tutoring_sessions));
        } else {
            $tutor['rating'] = 0;
        }
        $tutor['tutoring_sessions'] = $tutoring_sessions;
        if($tutor['details']->tutor_type == 'PEER') {
        $tutor['high_school'] = DB::table('highschool')
            ->where('user_id', '=', $id)
            ->select('*')
            ->get();
        } else {
            $tutor['high_school'] = "";
        }
        /**
         * Get SUM of total no of hours for Earning Informations
         **/

        $tutor['total_no_of_hours'] = DB::table('sessions')
            ->join('payments', 'payments.session_id', '=', 'sessions.session_id')
            ->where('sessions.tutor_id', '=', $id)
            ->where('sessions.is_completed', '=', 1)
            ->select(
                DB::raw("SUM(payments.hours) as total_hours"),
                DB::raw("SUM(payments.hours * payments.per_hour) as total_earning"))
            ->get();

        /**
         * Get SUM of total paid for Earning Information
         **/

        $tutor['total_paid_earning'] = DB::table('sessions')
            ->join('payments', 'payments.session_id', '=', 'sessions.session_id')
            ->where('sessions.tutor_id', '=', $id)
            ->where('sessions.is_completed', '=', 1)
            ->where('payments.is_credited', '=', 1)
            ->select(
                DB::raw("SUM(payments.hours * payments.per_hour) as total_paid"))
            ->get();

        if(empty($tutor['details'])) {
            return redirect()->action('TutorsController@index');
        } else {
            return view('tutors.tutorProfile', compact('tutor'));
        }

    }

    public function booking(Request $request)
    {
        $request->session()->forget('subject');
        $request->session()->forget('sessionDate');
        $request->session()->forget('studentEmail');
        $request->session()->forget('package_hour_fee');
        $request->session()->forget('peer_hour_fee');
        $request->session()->forget('pro_hour_fee');
        $request->session()->forget('package_selected');
        $request->session()->forget('student_id');
        $request->session()->forget('studentGrade');
        $request->session()->forget('categories');
        $request->session()->forget('begin_time');
        $request->session()->forget('session_duration');
        $request->session()->forget('tutor_id');
        $request->session()->forget('tutor_type');
        $tutor_details['grades'] = DB::table('grades')
            ->orderBy('grade', 'asc')
            ->get();
        $tutor_details['selected_tutors'] = array();

        return view('tutors.find_new_tutor', compact('tutor_details'));
    }

    public function categories_for_grade(Request $request)
    {

        // Getting all post data
        $grade_id =  $request->input('grade_id');

        $details = DB::table('grade_subjects')
            ->join('subjects', 'grade_subjects.subject_id', '=', 'subjects.subject_id')
            ->join('categories', 'subjects.category_id', '=', 'categories.category_id')
            ->where('grade_subjects.grade_id', '=', $grade_id)
            ->distinct('categories.category_id')
            ->select('categories.category_id','categories.category_name')
            ->orderBy('categories.category_name', 'asc')
            ->get();

        return json_encode($details);

    }

    public function categories_for_subject(Request $request)
    {

        // Getting all post data
        $category_id =  $request->input('category_id');

        $details = DB::table('categories')
            ->join('subjects', 'subjects.category_id', '=', 'categories.category_id')
            ->where('categories.category_id', '=', $category_id)
            ->select('subjects.subject_id','subjects.subject_name')
            ->orderBy('subjects.subject_name', 'asc')
            ->get();

        return json_encode($details);


    }

    public function update_booking($id)
    {

        $tutor_details['grades'] = DB::table('grades')
            ->orderBy('grade', 'asc')
            ->get();
        $tutor_details['selected_tutors'] = array();
        return view('tutors.update_booked_tutor', compact('tutor_details'));
    }

    public function update_booking_categories_for_grade(Request $request)
    {

        // Getting all post data
        $grade_id =  $request->input('grade_id');

        $details = DB::table('grade_subjects')
            ->join('subjects', 'grade_subjects.subject_id', '=', 'subjects.subject_id')
            ->join('categories', 'subjects.category_id', '=', 'categories.category_id')
            ->where('grade_subjects.grade_id', '=', $grade_id)
            ->distinct('categories.category_id')
            ->select('categories.category_id','categories.category_name')
            ->orderBy('categories.category_name', 'asc')
            ->get();

        return json_encode($details);

    }

    /**
     * @param Request $request
     * @return string
     */
    public function update_booking_categories_for_subject(Request $request)
    {

        // Getting all post data
        $category_id =  $request->input('category_id');

        $details = DB::table('categories')
            ->join('subjects', 'subjects.category_id', '=', 'categories.category_id')
            ->where('categories.category_id', '=', $category_id)
            ->select('subjects.subject_id','subjects.subject_name')
            ->orderBy('subjects.subject_name', 'asc')
            ->get();

        return json_encode($details);


    }

    public function update_booking_get_session_details_onload(Request $request)
    {

        // Getting all post data
        $session_id =  $request->input('session_id');

        $sessions_selector = DB::table('sessions')
            ->where('sessions.session_id',"=",$session_id)
            ->select('sessions.is_completed')->first();

        if($sessions_selector->is_completed == -1){

            $sessions_details = DB::table('sessions');
            $sessions_details->where('sessions.session_id',"=",$session_id)
                ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
                ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
            $sessions_details->select(DB::raw('student_users.email as student_email'),'sessions.subject_id','sessions.grade_id','sessions.date','sessions.start_time', 'sessions.end_time',DB::raw("CONCAT(sessions.street,' ',sessions.state, ' ', sessions.city) AS location "), 'subjects.category_id');
            $sessions_details = $sessions_details->first();
            return json_encode($sessions_details);

        } else {

            $sessions_details = DB::table('sessions');
            $sessions_details->where('sessions.session_id',"=",$session_id)
                ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
                ->join(DB::raw("tutors"),"sessions.tutor_id","=","tutors.user_id")
                ->join(DB::raw("users as tutor_users"),"sessions.tutor_id","=","tutor_users.user_id")
                ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
            $sessions_details->select(DB::raw("CONCAT(tutor_users.first_name,' ',tutor_users.last_name) AS tutor_name "),'tutors.gender',DB::raw('student_users.email as student_email'),'tutors.tutor_type','sessions.subject_id','sessions.grade_id','sessions.date','sessions.start_time', 'sessions.end_time',DB::raw("CONCAT(sessions.street,' ',sessions.state, ' ', sessions.city) AS location "), 'subjects.category_id');
            $sessions_details = $sessions_details->first();
            return json_encode($sessions_details);

        }

    }

   /* public function find(Request $request)
    {


        $request->session()->put('subject', $request->input('subject'));
        $request->session()->put('categories', $request->input('categories'));
        $request->session()->put('studentGrade', $request->input('studentGrade'));
        $request->session()->put('student_id', $request->input('student_id'));
        $request->session()->put('package_selected', $request->input('package_selected'));
        $request->session()->put('pro_hour_fee', $request->input('pro_hour_fee'));
        $request->session()->put('peer_hour_fee', $request->input('peer_hour_fee'));
        $request->session()->put('package_hour_fee', $request->input('package_hour_fee'));

        $subject = $request->input('subject');
        $type = $request->input('tutor_type');
        $request->session()->put('tutor_type', $request->input('tutor_type'));
        $gender = $request->input('gender');
        $request->session()->put('gender', $request->input('gender'));
        $location = $request->input('location');
        $student_email = $request->input('studentEmail');
        $request->session()->put('studentEmail', $student_email);
        $student_grade = $request->input('studentGrade');
        $category = $request->input('categories');
        $session_date = $request->input('sessionDate');
        $request->session()->put('sessionDate', $request->input('sessionDate'));

        $begin_time = $request->input('beginTime');
        $beginTimeType = $request->input('beginTimeType');
        if($begin_time != '') {
            if($beginTimeType == 'PM') {
                $begin_time = intval($begin_time) + 12;
            }
        }
        $request->session()->put('beginTime', $begin_time);
        $request->session()->put('beginTimeType', $beginTimeType);
        $end_time = $request->input('endTime');
        $endTimeType = $request->input('endTimeType');
        if($end_time != '') {
            if($endTimeType == 'PM') {
                $end_time = intval($end_time) + 12;
            }
        }
        $request->session()->put('endTime', $end_time);
        $request->session()->put('endTimeType', $endTimeType);
        $tutor_name = $request->input('tutorName');
        $this->validate($request, [
            'studentEmail' => 'required|email',
            'studentGrade' => 'required',
            'categories' => 'required',
            'subject' => 'required',
        ]);

        $duration = $end_time-$begin_time;
        $request->session()->put('session_duration', $duration);
        $session_date   =   date('Y-m-d', strtotime($session_date));
        $addr = \App\Tutors::getStudent($student_email);
        $available_tutors_array = array();
        $scheduled_tutors_array = array();
        if($addr != '') {
            $tutor_list = \App\Tutors::getSuitableTutors($addr, $type, $gender, $subject, $location, $tutor_name);

            $tutor_list_array = array();
            if(!empty($tutor_list)) {
                foreach($tutor_list as $each_tutor) {
                    array_push($tutor_list_array,$each_tutor->user_id);
                }
            }
            if(!empty($tutor_list_array)) {
                $not_available_tutors = \App\Tutors::get_availability($begin_time,$duration,$tutor_list_array,$session_date);


                if(!empty($not_available_tutors)) {
                    foreach($not_available_tutors as $each_tutor) {
                        if($each_tutor->is_booked == 0) {
                            array_push($available_tutors_array,$each_tutor->user_id);
                        } else if($each_tutor->is_booked == 1) {
                            array_push($scheduled_tutors_array,$each_tutor->user_id);
                        }
                    }
                }

                //  if(count($matching_tutors) > 0 && $matching_tutors[0] != '') {
                $to_select_tutors = DB::select('select users.first_name, users.last_name,users.user_id, tutors.profile_image, tutors.gender,address.city, address.state,address.street,address.zip,tutors.tutor_type from `users` left join `tutors` on `tutors`.`user_id` = `users`.`user_id` left join address on address.user_id=users.user_id where `tutors`.`user_id` in ('.implode(',',$tutor_list_array).')');
                $tutors_print = array();
                $i = 1;
                foreach($to_select_tutors as $each_tutor) {
                    $tutors_print[$i]['first_name'] = $each_tutor->first_name;
                    $tutors_print[$i]['last_name'] = $each_tutor->last_name;
                    $tutors_print[$i]['city'] = $each_tutor->city;
                    $tutors_print[$i]['state'] = $each_tutor->state;
                    $tutors_print[$i]['street'] = $each_tutor->street;
                    $tutors_print[$i]['zip'] = $each_tutor->zip;
                    $tutors_print[$i]['tutor_id'] = $each_tutor->user_id;
                    $tutors_print[$i]['tutor_type'] = $each_tutor->tutor_type;

                    $tutors_print[$i]['highschool'] = DB::table('users')
                        ->join('highschool', 'users.user_id', '=', 'highschool.user_id')
                        ->where('users.user_id', '=', $each_tutor->user_id)
                        ->select('highschool.school_name')
                        ->take(1)->get();
                    if(!empty($tutors_print[$i]['highschool'])) {
                        $tutors_print[$i]['highschool'] =  $tutors_print[$i]['highschool'][0]->school_name;
                    } else {
                        $tutors_print[$i]['highschool'] = "";
                    }


                    $rating_total = DB::table('sessions')
                        ->where('tutor_id', '=', $each_tutor->user_id)
                        ->select(DB::raw("SUM(rating) as sum"));

                    $rating_total = $rating_total->first();
                    $rating_total = intval($rating_total->sum);

                    $tutoring_sessions = DB::table('sessions')
                        ->where('tutor_id', '=', $each_tutor->user_id)
                        ->select(DB::raw("COUNT(rating) as count"));

                    $tutoring_sessions = $tutoring_sessions->first();
                    $tutoring_sessions = intval($tutoring_sessions->count);


                    if($tutoring_sessions>0){
                        $tutor_rating = intval(ceil($rating_total/$tutoring_sessions));
                    }else{
                        $tutor_rating=0;
                    }

                    $tutors_print[$i]['rating'] = $tutor_rating;
                    $tutors_print[$i]['rating_count'] = $tutoring_sessions;

                    $tutors_print[$i]['session_data']['student_id'] = $addr->user_id;
                    $tutors_print[$i]['session_data']['tutor_id'] = $each_tutor->user_id;
                    $tutors_print[$i]['session_data']['grade_id'] = $student_grade;
                    $tutors_print[$i]['session_data']['tutor_gender'] = $each_tutor->gender;
                    $tutors_print[$i]['session_data']['tutor_type'] = $each_tutor->tutor_type;
                    $tutors_print[$i]['session_data']['date'] = $session_date;
                    $tutors_print[$i]['session_data']['start_time'] = $begin_time;
                    $tutors_print[$i]['session_data']['end_time'] = $end_time;
                    $tutors_print[$i]['session_data']['street'] = $addr->street;
                    $tutors_print[$i]['session_data']['city'] = $addr->city;
                    $tutors_print[$i]['session_data']['state'] = $addr->state;
                    $tutors_print[$i]['session_data']['zip'] = $addr->zip;
                    $tutors_print[$i]['session_data']['lat'] = $addr->lat;
                    $tutors_print[$i]['session_data']['lng'] = $addr->lng;
                    $tutors_print[$i]['session_data']['subject_id'] = $subject;

                    $tutors_print[$i]['session_data']['is_completed'] = 0;
                    $tutors_print[$i]['session_data']['created_at'] = date('Y-m-d H:i:s');

                    $i++;
                }
                $to_select_tutors = $tutors_print;
                // }
            } else {
                $to_select_tutors = array();
            }
        } else {
            $to_select_tutors = array();
        }
        $tutor_details['selected_tutors'] = $to_select_tutors;
        $tutor_details['available_tutors'] = $available_tutors_array;
        $tutor_details['scheduled_tutors'] = $scheduled_tutors_array;

        $tutor_details['grades'] = DB::table('grades')
            ->orderBy('grade', 'asc')
            ->get();

        return view('tutors.find_new_tutor', compact('tutor_details'));

    } */
    public function find(Request $request)
    {


        $request->session()->put('subject', $request->input('subject'));
        $request->session()->put('categories', $request->input('categories'));
        $request->session()->put('studentGrade', $request->input('studentGrade'));
        $request->session()->put('student_id', $request->input('student_id'));
        $request->session()->put('package_selected', $request->input('package_selected'));
        $request->session()->put('pro_hour_fee', $request->input('pro_hour_fee'));
        $request->session()->put('peer_hour_fee', $request->input('peer_hour_fee'));
        $request->session()->put('package_hour_fee', $request->input('package_hour_fee'));

        $subject = $request->input('subject');
        $type = $request->input('tutor_type');

        $request->session()->put('tutor_type', $request->input('tutor_type'));

        $gender = $request->input('gender');

        $request->session()->put('gender', $request->input('gender'));

        $location = $request->input('location');
        $student_email = $request->input('studentEmail');
        $request->session()->put('studentEmail', $student_email);
        $student_grade = $request->input('studentGrade');
        $category = $request->input('categories');
        $session_date = $request->input('sessionDate');
        $request->session()->put('sessionDate', $request->input('sessionDate'));


        $tutor_id = $request->input('search_tutor_id');


        $begin_time = $request->input('begin_time');
        $request->session()->put('begin_time', $begin_time);
        $session_duration = $request->input('session_duration');
        $request->session()->put('session_duration', $session_duration);
        $available_tutors_only = $request->input('available_tutors_only');
        $addr = \App\Tutors::getStudent($student_email);

        $available_tutors_array = array();
        $scheduled_tutors_array = array();
        $no_results_yet = 1;
        if($addr != '') {
            $tutor_list = \App\Tutors::getSuitableTutors($addr, $type, $gender, $subject, $location, $tutor_id);

            $tutor_list_array = array();
            if(!empty($tutor_list)) {
                foreach($tutor_list as $each_tutor) {
                    array_push($tutor_list_array,$each_tutor->user_id);
                }
            }


            if(!empty($tutor_list_array)) {
                $not_available_tutors = \App\Tutors::get_availability($session_duration, $session_date, $begin_time, $tutor_list_array);

                if(!empty($not_available_tutors)) {
                    if($not_available_tutors != 'no_results') {
                        $no_results_yet = 0;
                        if(!empty($not_available_tutors['available_list'])) {
                            foreach($not_available_tutors['available_list'] as $each_tutor) {
                                array_push($available_tutors_array,$each_tutor['user_id']);
                            }
                        }
                        if(!empty($not_available_tutors['booked_list'])) {
                            foreach($not_available_tutors['booked_list'] as $each_tutor) {
                                array_push($scheduled_tutors_array,$each_tutor['user_id']);
                            }
                        }
                    }
                }

                //  if(count($matching_tutors) > 0 && $matching_tutors[0] != '') {
                $to_select_tutors = DB::select('select users.first_name, users.last_name,users.user_id,users.phone, tutors.profile_image, tutors.gender,address.city, address.state,address.street,address.zip,tutors.tutor_type from `users` left join `tutors` on `tutors`.`user_id` = `users`.`user_id` left join address on address.user_id=users.user_id where `tutors`.`user_id` in ('.implode(',',$tutor_list_array).')');
                $tutors_print = array();
                $i = 1;
                foreach($to_select_tutors as $each_tutor) {
                    $tutors_print[$i]['first_name'] = $each_tutor->first_name;
                    $tutors_print[$i]['last_name'] = $each_tutor->last_name;
                    $tutors_print[$i]['city'] = $each_tutor->city;
                    $tutors_print[$i]['state'] = $each_tutor->state;
                    $tutors_print[$i]['street'] = $each_tutor->street;
                    $tutors_print[$i]['zip'] = $each_tutor->zip;
                    $tutors_print[$i]['tutor_id'] = $each_tutor->user_id;
                    $tutors_print[$i]['tutor_type'] = $each_tutor->tutor_type;
                    $tutors_print[$i]['phone'] = $each_tutor->phone;
                    $tutors_print[$i]['gender'] = $each_tutor->gender;
                    $tutors_print[$i]['profile_image'] = $each_tutor->profile_image;
                    if($each_tutor->tutor_type == 'PEER') {
                        $tutors_print[$i]['highschool'] = DB::table('users')
                            ->join('highschool', 'users.user_id', '=', 'highschool.user_id')
                            ->where('users.user_id', '=', $each_tutor->user_id)
                            ->select('highschool.school_name')
                            ->take(1)->get();
                        if(empty($tutors_print[$i]['highschool'])) {
                            $tutors_print[$i]['highschool'] = "";
                        } else {
                        $tutors_print[$i]['highschool'] =  $tutors_print[$i]['highschool'][0]->school_name;
                        }
                    } else {
                        $tutors_print[$i]['highschool'] = DB::table('users')
                            ->join('college', 'users.user_id', '=', 'college.user_id')
                            ->where('users.user_id', '=', $each_tutor->user_id)
                            ->select('college.college_name')
                            ->take(1)->get();
                        if(empty($tutors_print[$i]['highschool'])) {
                            $tutors_print[$i]['highschool'] = "";
                        } else {
                            $tutors_print[$i]['highschool'] =  $tutors_print[$i]['highschool'][0]->college_name;
                        }


                    }

                    if(empty($tutors_print[$i]['highschool'])) {
                        $tutors_print[$i]['highschool'] = "";
                    }


                    $rating_total = DB::table('sessions')
                        ->where('tutor_id', '=', $each_tutor->user_id)
                        ->select(DB::raw("SUM(rating) as sum"));

                    $rating_total = $rating_total->first();
                    $rating_total = intval($rating_total->sum);

                    $tutoring_sessions = DB::table('sessions')
                        ->where('tutor_id', '=', $each_tutor->user_id)
                        ->select(DB::raw("COUNT(rating) as count"));

                    $tutoring_sessions = $tutoring_sessions->first();
                    $tutoring_sessions = intval($tutoring_sessions->count);


                    if($tutoring_sessions>0){
                        $tutor_rating = intval(ceil($rating_total/$tutoring_sessions));
                    }else{
                        $tutor_rating=0;
                    }

                    $tutors_print[$i]['rating'] = $tutor_rating;
                    $tutors_print[$i]['rating_count'] = $tutoring_sessions;



                    $i++;
                }
                $to_select_tutors = $tutors_print;
                // }
            } else {
                $to_select_tutors = array();
            }
        } else {
            $to_select_tutors = array();
        }
        $tutor_details['selected_tutors'] = $to_select_tutors;
        $tutor_details['available_tutors'] = $available_tutors_array;
        $tutor_details['scheduled_tutors'] = $scheduled_tutors_array;
$tutor_html = "";

foreach($tutor_details['selected_tutors'] as $each_tutor) {
    if($available_tutors_only == 'yes') {
        if(!in_array($each_tutor['tutor_id'],$tutor_details['available_tutors'])) {
            continue;
        }
    }
    $active_available_tutor = 0;
    $profile_picture = "";
    if($each_tutor['profile_image'] == '') {
        if($each_tutor['gender'] == 'm') {
            $profile_picture = URL::asset('assets/images/tutor-male.png');
        } else {
            $profile_picture = URL::asset('assets/images/tutor-female.png');
        }
    } else {
        $profile_picture = $each_tutor['profile_image'];
    }

$tutor_html .= '<div class="row-result">
<p>
                    <div class="tutor-bio col-sm-9">
                        <div class="profile-img-holder">
                            <img class="img-circle img-responsive" alt="Tutor Profile Image" src="'.$profile_picture.'">';
                   if($no_results_yet == 0) {
                       if(in_array($each_tutor['tutor_id'],$tutor_details['available_tutors'])) {
                           $active_available_tutor = 1;
                           $tutor_html .= '<img class="tutor-status" src="'.URL::asset('assets/images/tutor-available.png').'" alt="tutor is available">';
                       } else if(in_array($each_tutor['tutor_id'],$tutor_details['scheduled_tutors'])) {
                           $tutor_html .= '<img class="tutor-status" src="'.URL::asset('assets/images/tutor-scheduled.png').'" alt="tutor is available">';
                       } else {
                           $tutor_html .= '<img class="tutor-status" src="'.URL::asset('assets/images/tutor-not-available.png').'" alt="tutor is available">';
                       }
                    }
    $tutor_html .= '</div>';
    $tutor_link = url('tutors/'.$each_tutor['tutor_id']);
$tutor_html .= '<div class="profile-bio-info col-sm-10"><h3> <a href="'.$tutor_link.'" target="_blank">'.$each_tutor['first_name'].' '.$each_tutor['last_name'].'</a></h3>
    <div class="contact-block col-sm-4">
        <p><i class="fa fa-phone fa-fw"></i>'.$each_tutor['phone'].'</p>
        <p><i class="fa fa-map-marker fa-fw"></i>'.$each_tutor['street'].','.$each_tutor['city'].','.$each_tutor['state'].' '.$each_tutor['zip'].'</p>
    </div>
    <div class="education-block col-sm-6">
        <p><i class="fa fa-graduation-cap fa-fw"></i>'.$each_tutor['highschool'].'</p>
    </div>
    <div class="tutor-type-block">
        <p><i class="fa fa-user fa-fw"></i>'.$each_tutor['tutor_type'].' Tutor</p>
    </div>
</div>
</div>
<div class="tutor-rating col-sm-3">
    <div class="star-rating tutor-rating">';
        for( $i=0; $i < $each_tutor['rating']; $i++) {
            $tutor_html .= '<span class="fa fa-star fa-star-active" data-rating="1"></span>';
        }
        for( $i=0; $i < 5-$each_tutor['rating']; $i++) {
            $tutor_html .= '<span class="fa fa-star" data-rating="3"></span>';
        }
    $tutor_html .= '</div>
    <p class="review-stats"><span class="num-of-students">'.$each_tutor['rating_count'].'</span> Reviews</p>';
    if($active_available_tutor == 1 &&  $no_results_yet == 0 ) {
        $disable_assign_btn = "";
    } else {
        $disable_assign_btn = "disable_assign_btn";
    }

    $tutor_html .= '<a href="javascript:void(0);" class="btn btn-default assign '.$disable_assign_btn.'" onclick=tutor_booking_cofirm("'.$each_tutor['tutor_id'].'","'.$active_available_tutor.'")>Assign this Tutor</a>
</div>
</div>';

}
       /*
        foreach($tutor_details['selected_tutors'] as $each_tutor) {
            $tutor_html .= '<div class="row-result">
                    <div class="tutor-bio col-sm-9">';
                        $profile_image = URL::asset('assets/images/tutor-prof-img.png');
            $tutor_html .= '<div class="profile-img-holder"><img class="img-circle" alt="Tutor Profile Image" src="'.$profile_image.'">';

            $tutor_html .= '</div>';
            $tutor_html .= '</div></div>';
        }
       */
        $tutor_details['grades'] = DB::table('grades')
            ->orderBy('grade', 'asc')
            ->get();
        $find_result_array = array();
        if(!empty($tutor_list_array)) {
            $find_result_array['all_tutor_list'] = implode(",",$tutor_list_array);
        } else {
            $find_result_array['all_tutor_list'] = "";
        }

        $find_result_array['result'] = $tutor_html;
        echo json_encode($find_result_array);
       // return view('tutors.find_new_tutor', compact('tutor_details'));

    }

    public function storesession(Request $request) {

        $session_data = $request->input('session_data');
        $session_data = unserialize($session_data);

        DB::table('sessions')->insert($session_data);
        $availability_data = array();
        $availability_data['date'] = $session_data['date'];
        $availability_data['start_time'] = $session_data['start_time'];
        $availability_data['end_time'] = $session_data['end_time'];
        $availability_data['user_id'] = $session_data['tutor_id'];
        $availability_data['is_booked'] = 1;
        $availability_data['created_at'] = date('Y-m-d H:i:s');
        DB::table('availability')->insert($availability_data);
        $request->session()->flash('alert-success', 'Session successfully Saved');
        return redirect()->action('SessionsController@scheduled_listing');

    }

    /**
     *
     * Dalete tutor's subjects in Tutors.

     * @param  Request  $request
     * @return boolian
     *
     */

    public function delete_tutor_subject(Request  $request){

        $subject_id =  $request->input('subject_id');
        $user_id =  $request->input('user_id');

        $query  = DB::table('tutoring_subjects')
            ->where('subject_id', '=', $subject_id)
            ->where('user_id', '=', $user_id)
            ->delete();


        if($query){

            $arr = array(
                'subject_id'=>$subject_id,
                'user_id'=>$user_id
            );
            return $arr;

        }

    }

    public function delete_tutor_subject_from_profile(Request  $request){

        $subject_id =  $request->input('subject_id');
        $tutor_id =  $request->input('tutor_id');

        $result = DB::table('tutoring_subjects')
            ->where('subject_id', '=', $subject_id)
            ->where('user_id', '=', $tutor_id)
            ->delete();

        echo json_encode($result);

    }
    
    public function getStudentEmail(Request  $request) {

        $term = $request->input('term');

        $results = array();

        $queries = DB::table('users')
            ->where('email', 'LIKE', $term.'%')
            ->where('user_role', '=', 2)
            ->take(5)->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->user_id, 'value' => $query->email];
        }
        echo json_encode($results);
    }

    public function getTutorName(Request  $request) {

        $term = $request->input('term');

        $results = array();

        $queries = DB::table('users')
        ->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$term.'%')
            ->where('user_role', '=', 1)
            ->take(5)->get();

        foreach ($queries as $query)
        {
            $results[] = [ 'id' => $query->user_id, 'value' => $query->first_name.' '.$query->last_name];
        }
        echo json_encode($results);
    }

    public function student_summery(Request  $request) {
        $id = $request->input('user_id');
        $students_details = DB::table('students')
            ->join('users', 'students.user_id', '=', 'users.user_id')
            ->join('address', 'users.user_id', '=', 'address.user_id')
            ->where('users.user_id', '=', $id)
            ->select('users.email','users.first_name','users.last_name','users.phone','address.street','address.city','address.state','address.zip')
            ->first();

        echo json_encode($students_details);
    }

    public function get_tutor_type_student_rates(Request  $request) {
        $subject_id = $request->input('subject_id');
        $rates_details = DB::table('grade_subjects')
            ->where('subject_id', '=', $subject_id)
            ->select('peer_student_amount','pro_student_amount','package_10_rate')
            ->first();

        echo json_encode($rates_details);
    }

    public function pick_tutor_add_card($id){

        $tutor = array();
        $tutor['id'] = $id;
        return view('tutors.pick_tutor_add_card',compact('tutor'));
    }

    public function  pick_tutor_add_card_details(Request $request)
    {

        $user_id = $request->session()->get('student_id');
        $tutor_id = $request->input('tutor_id');
        $is_user_exist = DB::table('student_accounts')
            ->where('user_id', '=', $user_id)
            ->select('user_id')
            ->first();

        if(!count($is_user_exist)>0)
        {
            \Stripe\Stripe::setApiKey(env('APP_SK_KEY'));

            $creditCardToken  = $request->input('stripeToken');

            $customer =   \Stripe\Customer::create(array(
                "description" => "Customer for test@example.com",
                "source" => $creditCardToken
            ));

            $is_inserted =    DB::table('student_accounts')->insert(
                [
                    'token' => $customer["id"],
                    'card_no' => $customer['sources']['data'][0]['last4'],
                    'user_id'=>$user_id
                ]
            );

            if($is_inserted){
                return redirect()->action('TutorsController@pick_tutor_payment_info',$tutor_id);
            }else{
                return redirect()->action('TutorsController@pick_tutor_add_card');
            }



        }




    }

    public function  pick_tutor_payment_info(Request $request,$id)
    {

        $student_id = $request->session()->get('student_id');

        $tutor_id = $request->session()->put('tutor_id',$id);
        $pick_tutor_payment = array();
        $pick_tutor_payment['payment_info'] = DB::table('student_accounts')
            ->where('user_id', '=', $student_id)
//            ->select('user_id')
            ->first();

        if(empty($pick_tutor_payment['payment_info'])) {
            $tutor = array();
            $tutor['id'] = $id;
           // return view('tutors.pick_tutor_add_card', compact('tutor'));

            return redirect()->action('TutorsController@pick_tutor_add_card_details',$tutor);

        } else {

            \Stripe\Stripe::setApiKey(env('APP_SK_KEY'));

            $retrieve_customer  = \Stripe\Customer::retrieve($pick_tutor_payment['payment_info']->token);

            $card_type =  $retrieve_customer["sources"]["data"][0]['brand'];
            $pick_tutor_payment['card_type'] = $card_type;
            $tutor_id = $id;

            $pick_tutor_payment['tutor_name'] = \App\Tutors::get_tutor_name($id);
            $student_email = $request->session()->get('studentEmail');
            $pick_tutor_payment['student_details'] =  \App\Tutors::getStudent($student_email);

            $pick_tutor_payment['session_date'] = date('l , F d ,Y',strtotime($request->session()->get('sessionDate')));

            $pick_tutor_payment['session_start'] = $request->session()->get('begin_time');
            if($pick_tutor_payment['session_start'] > 12) {
                $pick_tutor_payment['session_start'] = ($pick_tutor_payment['session_start'] - 12).':00 PM';
            } else if($pick_tutor_payment['session_start'] == 12) {
                $pick_tutor_payment['session_start'] = ($pick_tutor_payment['session_start']).':00 PM';
            } else {
                $pick_tutor_payment['session_start'] = ($pick_tutor_payment['session_start']).':00 AM';
            }
            $pick_tutor_payment['session_end'] = $request->session()->get('begin_time') + $request->session()->get('session_duration');
            if($pick_tutor_payment['session_end'] > 12) {
                $pick_tutor_payment['session_end'] = ($pick_tutor_payment['session_end'] - 12).':00 PM';
            } else if($pick_tutor_payment['session_end'] == 12) {
                $pick_tutor_payment['session_end'] = ($pick_tutor_payment['session_end']).':00 PM';
            } else {
                $pick_tutor_payment['session_end'] = ($pick_tutor_payment['session_end']).':00 AM';
            }
            $pick_tutor_payment['tutor_type'] = $pick_tutor_payment['tutor_name']->tutor_type;

            $request->session()->put('tutor_type', $pick_tutor_payment['tutor_type']);
            $pick_tutor_payment['available_remaining_hours'] = 0;
            /** remove pro package
            if($pick_tutor_payment['tutor_type'] == 'PRO' || $pick_tutor_payment['tutor_type'] == 'ALL') {
             * */
                $grade_subjects_id = DB::table('grade_subjects')
                    ->where('subject_id', '=', $request->session()->get('subject'))
                    ->where('grade_id', '=', $request->session()->get('studentGrade'))
                    ->select("id")
                    ->first();


             $package_count = DB::table('packages')
                    ->where('subject_grade_id', '=', $grade_subjects_id->id)
                    ->where('student_id', '=', $student_id)
                    ->where('package_type', '=', 1)
                    ->select(DB::raw("sum(total_hours) as all_count"))
                    ->get();

            $package_count = $package_count[0]->all_count;
                $package_id = DB::table('packages')
                    ->where('subject_grade_id', '=', $grade_subjects_id->id)
                    ->where('student_id', '=', $student_id)
                    ->where('package_type', '=', 1)
                    ->max('package_id');

                //need to add stubject_grade_id to the packages db.

                $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");


                if($package_count == 0) {
                    $pick_tutor_payment['available_remaining_hours'] = 0;
                    $package_id = 0;
                } else {
                    $pick_tutor_payment['available_remaining_hours'] = ($package_count - $used_session[0]->hours);

                }

        /** remove pro package      } else {
                $package_id = 0;
            }
         */
            $tutor_gender = $pick_tutor_payment['tutor_name']->gender;
            $pick_tutor_payment['peer_hour_fee'] = $request->session()->get('peer_hour_fee');
            $pick_tutor_payment['tutor_id'] = $tutor_id;
            $pick_tutor_payment['pro_hour_fee'] = $request->session()->get('pro_hour_fee');
            /** remove pro package    if($pick_tutor_payment['tutor_type'] == 'PRO' || $pick_tutor_payment['tutor_type'] == 'ALL') { */
                $pick_tutor_payment['package_hour_fee'] = $request->session()->get('package_hour_fee');
         /** remove pro package    } else {
                $pick_tutor_payment['package_hour_fee'] = "";
            }
             if($request->session()->get('package_selected') == 1 && $pick_tutor_payment['tutor_type'] == 'PRO') { */
            if($request->session()->get('package_selected') == 1) {
                $pick_tutor_payment['package_selected'] = 1;

            } else {
                $pick_tutor_payment['package_selected'] = 0;
            }
            $request->session()->put('package_selected', $pick_tutor_payment['package_selected']);
            $pick_tutor_payment['session_duration'] = $request->session()->get('session_duration');
            $pick_tutor_payment['package_id'] = $package_id;
            if($tutor_gender == 'm') {
                $pick_tutor_payment['tutor_gender'] = 'Male';
            } else {
                $pick_tutor_payment['tutor_gender'] = 'Female';
            }

            return view('tutors.pick_tutor_payment_info', compact('pick_tutor_payment'));
        }



    }

    public function  pick_tutors_charge(Request $request)
    {

        \Stripe\Stripe::setApiKey(env('APP_SK_KEY'));

        $student_id = $request->session()->get('student_id');
        $subject_id = $request->session()->get('subject');
        $pick_tutor_payment_info = DB::table('student_accounts')
            ->where('user_id', '=', $student_id)
            ->first();
        $student_details_table = DB::table('students')
            ->where('user_id', '=', $student_id)
            ->first();

        $customer_id =  $pick_tutor_payment_info->token;
        $charge_amount =  $request->input('amount') *100;
        $tutor_id =   $request->input('tutor_id');


        // Charge the Customer instead of the card
        $customer_charge = \Stripe\Charge::create(array(
                "amount" => $charge_amount, # amount in cents, again
                "currency" => "usd",
                "customer" => $customer_id)
        );

        if($customer_charge['status']=="succeeded"){

            $session_entry_data = array();

            $session_entry_data['student_id'] = $student_id;
            $session_entry_data['tutor_id'] = $tutor_id;
            $session_entry_data['grade_id'] = $request->session()->get('studentGrade');

            $session_entry_data['tutor_type'] = $request->session()->get('tutor_type');
            $session_entry_data['date'] = $request->session()->get('sessionDate');
            $session_entry_data['start_time'] = $request->session()->get('begin_time');
            $session_entry_data['end_time'] = $request->session()->get('begin_time') + $request->session()->get('session_duration');
            $student_email = $request->session()->get('studentEmail');
            $student_details = \App\Tutors::getStudent($student_email);
            $session_entry_data['street'] = $student_details->street;
            $session_entry_data['subject_id'] = $subject_id;
            $session_entry_data['city'] = $student_details->street;
            $session_entry_data['state'] = $student_details->state;
            $session_entry_data['zip'] = $student_details->zip;
            $session_entry_data['lat'] = $student_details->lat;
            $session_entry_data['lng'] = $student_details->lng;
            $session_entry_data['is_completed'] = 0;

            if(!empty($student_details_table)) {
                $session_entry_data['pin'] = $student_details_table->pin;
            } else {
                $session_entry_data['pin'] = "";
            }
            $session_entry_data['created_at'] = date('Y-m-d H:i:s');

            DB::table('sessions')->insert($session_entry_data);
            $session_id = DB::getPdo()->lastInsertId();


            $start_update_time = $session_entry_data['start_time'];
            $end_update_time = $session_entry_data['end_time'];
            for( $i=1; $i <= ($session_entry_data['end_time']-$session_entry_data['start_time']); $i++) {

                DB::table('availability')
                    ->where('user_id', $session_entry_data['tutor_id'])
                    ->where('start_time',$start_update_time)
                    ->where('end_time',$end_update_time)
                    ->update(array('updated_at' => date('Y-m-d H:i:s'),'is_booked'=>1));

            }

            $start_update_time = $session_entry_data['start_time'];
            $end_update_time = $session_entry_data['start_time']+1;
            for( $i=1; $i <= ($session_entry_data['end_time']-$session_entry_data['start_time']); $i++) {

                DB::table('availability')
                    ->where('user_id', $session_entry_data['tutor_id'])
                    ->where('start_time',$start_update_time)
                    ->where('end_time',$end_update_time)
                    ->update(array('updated_at' => date('Y-m-d H:i:s'),'is_booked'=>1));



                $start_update_time++;
                $end_update_time++;

            }


            $packages_data = array();
            if($request->session()->get('package_selected') == 0) {
                $packages_data['package_type'] =2;
                $available_hours = 0;
                $previous_package_id = 0;
            } else {
                $packages_data['package_type'] =1;

                $available_hours = $request->input('available_hours');
                $previous_package_id = $request->input('package_id');

            }
            $packages_data['student_id'] = $student_id;
            if($request->session()->get('package_selected') == 0) {
                $packages_data['total_hours'] = $request->session()->get('session_duration');
            } else {
                $packages_data['total_hours'] = 10;
            }

            $packages_data['subject_grade_id'] = $session_entry_data['grade_id'];
            $packages_data['created_at'] =date('Y-m-d H:i:s');
            $packages_data['updated_at'] =date('Y-m-d H:i:s');
            $packages_data['is_charged'] =1;
            $packages_data['charged_at'] =date('Y-m-d H:i:s');;
             // $packages_data['is_completed'] =0; Have to Clarify with Sherine 2015-10-27
            $packages_details = DB::table('packages')->insert($packages_data);
            $new_package_id = DB::getPdo()->lastInsertId();

            $grade_subject_details = DB::table('grade_subjects')
                ->where('subject_id', '=', $subject_id)
                ->where('grade_id', '=', $request->session()->get('studentGrade'))
                ->first();

            if($request->session()->get('package_selected') == 1) {
                if($available_hours > 0) {
                    $payment_data = array();
                    $package_id = $previous_package_id;
                    $payment_data['package_id'] =$package_id;
                    $payment_data['session_id'] =$session_id;
                    $payment_data['hours'] =$available_hours;
                    $payment_data['is_credited'] =1;
                    $payment_data['created_at'] =date('Y-m-d H:i:s');
                    $payment_data['updated_at'] =date('Y-m-d H:i:s');
                    $payment_data['per_hour'] = $grade_subject_details->package_10_rate;
                    /** remove pro package $payment_data['per_hour_tutor'] =$grade_subject_details->pro_tutor_amount; */
                    $payment_data['per_hour_tutor'] =$grade_subject_details->peer_tutor_amount;
                    DB::table('payments')->insert($payment_data);
                }
                $payment_data = array();
                $payment_data['package_id'] =$new_package_id;
                $payment_data['session_id'] =$session_id;

                $payment_data['is_credited'] =1;
                $payment_data['created_at'] =date('Y-m-d H:i:s');
                $payment_data['updated_at'] =date('Y-m-d H:i:s');
                $payment_data['per_hour'] = $grade_subject_details->package_10_rate;
                /** remove pro package $payment_data['per_hour_tutor'] =$grade_subject_details->pro_tutor_amount; */
                $payment_data['per_hour_tutor'] =$grade_subject_details->peer_tutor_amount;
                if($available_hours > 0) {
                    $payment_data['hours'] =($request->session()->get('session_duration')) - $available_hours;
                } else {
                    $payment_data['hours'] = $request->session()->get('session_duration');
                }
                DB::table('payments')->insert($payment_data);
            }



            $subject_name = DB::table('subjects')
                ->where('subject_id', '=', $subject_id)
                ->select('subject_name')
                ->first();

            $booking_success_data  = array();
            $booking_success_data['tutor_type'] = $session_entry_data['tutor_type'];
            $booking_success_data['subject_name'] = $subject_name->subject_name;
            $tutor_details = \App\Tutors::get_tutor_name($tutor_id);
            $booking_success_data['tutor_name'] = $tutor_details->first_name.' '.$tutor_details->last_name;
            $booking_success_data['profile_pic'] = $tutor_details->profile_image;
            if($tutor_details->gender == 'f') {
                $booking_success_data['tutor_gender'] = 'Female';
            } else {
                $booking_success_data['tutor_gender'] = 'Male';
            }

            $this->tutor_assigned_email($session_id);
           return view('tutors.tutor_booking_success', compact('booking_success_data'));

        }else{
           // return view('tutors.pick_tutor_payment_info');
            return redirect()->action('TutorsController@pick_tutor_payment_info',$tutor_id);
        }

        //exit;

       // return view('tutors.pick_tutor_payment_info', compact('pick_tutor_payment'));

    }

    public function get_available_tutors_for_calendar(Request $request) {


        $student_email = $request->input('studentEmail');
        if($student_email == "") {
            $student_email =$request->session()->get('studentEmail');
        }

        $subject_id = $request->input('subject');
        if($subject_id == "") {
            $subject_id = $request->session()->get('subject');
        }
        $tutor_type = $request->input('tutor_type');

        if($tutor_type == "") {
            $tutor_type = $request->session()->get('tutor_type');
        }
        $gender = $request->input('gender');
        if($gender == "") {
            $gender = $request->session()->get('gender');
        }
        $session_duration = $request->input('session_duration');

        if($student_email == '' || $subject_id == '') {
            echo json_encode(array());
        } else {

            $addr = \App\Tutors::getStudent($student_email);

            if($addr != '') {

                $tutor_list = \App\Tutors::getSuitableTutors($addr, $tutor_type, $gender, $subject_id, "", "");

                $tutor_list_array = array();
                if(!empty($tutor_list)) {
                    foreach($tutor_list as $each_tutor) {
                        array_push($tutor_list_array,$each_tutor->user_id);
                    }
                }
                if(!empty($tutor_list_array)) {

                    $all_tutors = \App\Tutors::get_calendar_availability($_REQUEST['year'], $_REQUEST['month'], $tutor_list_array,$session_duration);

                     $dates = array();
                    $k =0;
                    if(!empty($all_tutors)) {

                    foreach($all_tutors as $each_day) {
                        $dates[$k] = array(
                            'date' => $each_day,
                            'badge' => false,
                            'title' => ''
                        );
                        $k++;
                    }
                    }

                    echo json_encode($dates);
                }

            } else {
                echo json_encode(array());
            }
        }


    }

    public function get_available_time_slots(Request $request) {
        $tutor_list = $request->input('tutor_list');
        $session_date = $request->input('session_date');
        $session_duration = $request->input('session_duration');
        $date_times_sql ="select date,start_time,end_time,is_booked,user_id  from availability where
                user_id in (". $tutor_list.")";

        if($session_date != '') {
            $date_times_sql .=" AND date = '".$session_date."' ";
        }

        $date_times_sql .= " AND is_booked=0 AND start_time >= 10 AND end_time <= 21 ORDER by user_id,date,start_time,end_time";
        if($session_duration == '') {
            $session_duration = 1;
        }

        $date_times = DB::select($date_times_sql);
        if(!empty($date_times)) {
            $user_available_list = array();

            if(!empty($date_times)) {
                $available_count = 0;
                for ($x = 1; $x <= 11; $x++) {
                foreach($date_times as $each_time_slot) {
                    if($available_count == 0)  {
                        if($each_time_slot->is_booked == 0) {
                            $available_count++;
                            $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                            $user_available_list[$available_count]['date'] = $each_time_slot->date;
                            $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                            $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                            $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                        }
                    } else {
                        if(($user_available_list[$available_count]['user_id'] == $each_time_slot->user_id) && ($user_available_list[$available_count]['end_time'] == $each_time_slot->start_time) && $each_time_slot->is_booked == 0) {
                            if($session_duration == ($user_available_list[$available_count]['end_time']-$user_available_list[$available_count]['start_time'])) {
                                $available_count++;
                                $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);

                            } else {
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $user_available_list[$available_count]['start_time']);

                            }
                        } else {
                            if($each_time_slot->is_booked == 0) {
                                $available_count++;
                                $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                            }
                        }
                    }
                }
                    unset($date_times[$x-1]);
                }

            }

            $user_available_list = array_map("unserialize", array_unique(array_map("serialize", $user_available_list)));
            foreach($user_available_list as $id=>$each_tutor) {
                if($session_duration >$each_tutor['duration']) {

                    unset($user_available_list[$id]);
                }


            }

            $time_array = array();
            foreach($user_available_list as $each_date) {
                array_push($time_array,$each_date['start_time']);
            }
            echo json_encode($time_array);

        } else {
            echo json_encode(array());
        }


    }

    public function clear_booking_sessions(Request $request){

        $request->session()->forget('subject');
        $request->session()->forget('sessionDate');
        $request->session()->forget('studentEmail');
        $request->session()->forget('package_hour_fee');
        $request->session()->forget('peer_hour_fee');
        $request->session()->forget('pro_hour_fee');
        $request->session()->forget('package_selected');
        $request->session()->forget('student_id');
        $request->session()->forget('studentGrade');
        $request->session()->forget('categories');
        $request->session()->forget('begin_time');
        $request->session()->forget('session_duration');
        $request->session()->forget('tutor_id');
        $request->session()->forget('tutor_type');

        return redirect()->action('TutorsController@booking');
    }

    public function assign_tutor_for_no_tutor(Request $request,$id) {


        $sessions_selector = DB::table('sessions')
            ->where('sessions.session_id',"=",$id)
            ->select('sessions.is_completed')->first();
        if(empty($sessions_selector)) {
            return redirect()->action('SessionsController@listing');
        } else {
        if($sessions_selector->is_completed == -1){

            $sessions_details = DB::table('sessions');
            $sessions_details->where('sessions.session_id',"=",$id)
                ->join('subjects', 'subjects.subject_id', '=', 'sessions.subject_id')
                ->join('grades', 'grades.grade_id', '=', 'sessions.grade_id')
                ->join('categories', 'subjects.category_id', '=', 'categories.category_id')
                ->join(DB::raw("users as student_users"),"sessions.student_id","=","student_users.user_id");
            $sessions_details->select(
                DB::raw('student_users.user_id as student_id'),
                DB::raw('student_users.email as student_email'),
                DB::raw('((sessions.end_time) - (sessions.start_time)) as session_duration'),
                DB::raw('categories.category_name as category_name'),
                'subjects.subject_name','grades.grade','sessions.date','sessions.tutor_gender','sessions.start_time', 'sessions.end_time',
                DB::raw("CONCAT(sessions.street,' ',sessions.state, ' ', sessions.city) AS location "), 'categories.category_name','student_users.first_name','student_users.last_name','student_users.phone','sessions.tutor_type','student_users.email','categories.category_id','sessions.subject_id','sessions.grade_id','sessions.session_id');
            $sessions_details = $sessions_details->first();
            if($sessions_details->tutor_gender == 'a') {
                $sessions_details->tutor_gender = 'any_gender';
            }


            if($sessions_details->start_time > 12) {
                $sessions_details->start_time = ($sessions_details->start_time - 12).':00 PM';
            } else if($sessions_details->start_time == 12) {
                $sessions_details->start_time = ($sessions_details->start_time).':00 PM';
            } else {
                $sessions_details->start_time = ($sessions_details->start_time).':00 AM';
            }

            if($sessions_details->end_time > 12) {
                $sessions_details->end_time = ($sessions_details->end_time - 12).':00 PM';
            } else if($sessions_details->end_time == 12) {
                $sessions_details->end_time = $sessions_details->end_time.':00 PM';
            } else {
                $sessions_details->end_time = $sessions_details->end_time.':00 AM';
            }
            $sessions_details->show_time = $sessions_details->start_time. " - ".$sessions_details->end_time;
            $request->session()->put('studentEmail', $sessions_details->student_email);
            $request->session()->put('subject', $sessions_details->subject_id);
            $request->session()->put('tutor_type', $sessions_details->tutor_type);
            $request->session()->put('gender', $sessions_details->tutor_gender);


            return view('tutors.assign_tutor_for_no_tutor', compact('sessions_details'));

        } else {
            return redirect()->action('SessionsController@listing');
        }
        }
    }
    public function pick_tutors_no_charge(Request $request) {



            $package_id = $request->input('package_id');
            $tutor_id =   $request->input('tutor_id');
            $subject_id = $request->session()->get('subject');
            $grade_id = $request->session()->get('studentGrade');
            $student_id = $request->session()->get('student_id');
            $student_details_table = DB::table('students')
            ->where('user_id', '=', $student_id)
            ->first();

            $session_entry_data = array();
            $session_entry_data['student_id'] = $request->session()->get('student_id');
            $session_entry_data['tutor_id'] = $tutor_id;
            $session_entry_data['grade_id'] = $grade_id;
            $session_entry_data['tutor_type'] = $request->session()->get('tutor_type');
            $session_entry_data['date'] = $request->session()->get('sessionDate');
            $session_entry_data['start_time'] = $request->session()->get('begin_time');
            $session_entry_data['end_time'] = $request->session()->get('begin_time') + $request->session()->get('session_duration');
            $student_email = $request->session()->get('studentEmail');
            $session_entry_data['subject_id'] =  $subject_id;
            $student_details = \App\Tutors::getStudent($student_email);
            $session_entry_data['street'] = $student_details->street;
            $session_entry_data['city'] = $student_details->street;
            $session_entry_data['state'] = $student_details->state;
            $session_entry_data['zip'] = $student_details->zip;
            $session_entry_data['lat'] = $student_details->lat;
            $session_entry_data['lng'] = $student_details->lng;
            if(!empty($student_details_table)) {
                $session_entry_data['pin'] = $student_details_table->pin;
            } else {
                $session_entry_data['pin'] = "";
            }
            $session_entry_data['is_completed'] = 0;
            $session_entry_data['created_at'] = date('Y-m-d H:i:s');

            DB::table('sessions')->insert($session_entry_data);
           $session_id = DB::getPdo()->lastInsertId();


            $start_update_time = $session_entry_data['start_time'];
            $end_update_time = $session_entry_data['start_time']+1;
            for( $i=1; $i <= ($session_entry_data['end_time']-$session_entry_data['start_time']); $i++) {

              DB::table('availability')
                    ->where('user_id', $session_entry_data['tutor_id'])
                    ->where('start_time',$start_update_time)
                    ->where('end_time',$end_update_time)
                   ->update(array('updated_at' => date('Y-m-d H:i:s'),'is_booked'=>1));



                $start_update_time++;
                $end_update_time++;

            }


            $grade_subject_details = DB::table('grade_subjects')
                ->where('subject_id', '=', $subject_id)
                ->where('grade_id', '=', $grade_id)
                ->first();

            $payment_data = array();
            $payment_data['package_id'] =$package_id;
            $payment_data['session_id'] =$session_id;
            $payment_data['hours'] =$request->session()->get('session_duration');
            $payment_data['is_credited'] =1;
            $payment_data['created_at'] =date('Y-m-d H:i:s');
            $payment_data['updated_at'] =date('Y-m-d H:i:s');
            $payment_data['per_hour'] = $grade_subject_details->package_10_rate;
           /** remove pro package $payment_data['per_hour_tutor'] =$grade_subject_details->pro_tutor_amount; */
            $payment_data['per_hour_tutor'] =$grade_subject_details->peer_tutor_amount;
            DB::table('payments')->insert($payment_data);
            $subject_name = DB::table('subjects')
            ->where('subject_id', '=', $subject_id)
            ->select('subject_name')
            ->first();

            $booking_success_data  = array();
            $booking_success_data['tutor_type'] = $session_entry_data['tutor_type'];
            $booking_success_data['subject_name'] = $subject_name->subject_name;
            $tutor_details = \App\Tutors::get_tutor_name($tutor_id);
            $booking_success_data['tutor_name'] = $tutor_details->first_name.' '.$tutor_details->last_name;
            $booking_success_data['profile_pic'] = $tutor_details->profile_image;
            if($tutor_details->gender == 'f') {
                $booking_success_data['tutor_gender'] = 'Female';
            } else {
                $booking_success_data['tutor_gender'] = 'Male';
            }

            $this->tutor_assigned_email($session_id);
            return view('tutors.tutor_booking_success', compact('booking_success_data'));


    }
    public function get_tutor_remaining_hours(Request $request) {
        $student_id = $request->input('student_id');
        $grade_id = $request->input('grade_id');
        $subject_id = $request->input('subject_id');
        $grade_subjects_id = DB::table('grade_subjects')
            ->where('subject_id', '=', $subject_id)
            ->where('grade_id', '=', $grade_id)
            ->select("id")
            ->first();
        if($student_id != '' && $grade_subjects_id != '') {
            $package_count = DB::table('packages')
                ->where('subject_grade_id', '=', $grade_subjects_id->id)
                ->where('student_id', '=', $student_id)
                ->where('package_type', '=', 1)
                ->select(DB::raw("sum(total_hours) as all_count"))
                ->get();

                $package_count = $package_count[0]->all_count;


                $used_session = DB::select("select sum(hours) as hours from payments, packages where packages.subject_grade_id =".$grade_subjects_id->id." and packages.package_type=1 and packages.package_id = payments.package_id and packages.student_id = $student_id");


                if($package_count == 0) {
                    echo 'null';

                } else {
                    if(($package_count - $used_session[0]->hours) > 0) {
                        echo ($package_count - $used_session[0]->hours);
                    } else {
                        echo 'null';
                    }

                }


        } else {
            echo 'null';
        }
    }



    public function tutor_booking_update(Request $request){

        $session_id =    $request->input('session_assign_info_session_id');
        $session_date =    $request->input('session_assign_info_session_date');
        $session_duration =    $request->input('session_assign_info_session_duration');
        $begin_time =    $request->input('session_assign_info_begin_time');
        $tutor_id =    $request->input('session_assign_info_tutor_id');

        $is_session_value =  DB::table('sessions')
            ->join('grades', 'sessions.grade_id', '=', 'grades.grade_id')
            ->join('subjects', 'sessions.subject_id', '=', 'subjects.subject_id')
            ->where('sessions.session_id', '=', $session_id)
            ->select('subjects.subject_name', 'grades.grade','sessions.tutor_type','sessions.tutor_gender')
            ->get();

        if($tutor_id){
            $is_tutor_details = DB::table('users')
                ->where('user_id', '=', $tutor_id)
                ->first();
        }

        $is_tutor_profile = DB::table('tutors')
            ->where('user_id', '=', $tutor_id)
            ->first();

        $booking_success_data  = array();
        $booking_success_data['tutor_type'] = $is_session_value[0]->tutor_type;
        $booking_success_data['subject_name'] = $is_session_value[0]->subject_name;
        $booking_success_data['tutor_name'] = $is_tutor_details->first_name.' '.$is_tutor_details->last_name;
        $booking_success_data['profile_pic'] = $is_tutor_profile->profile_image;
        if($is_session_value[0]->tutor_gender == 'f') {
            $booking_success_data['tutor_gender'] = 'Female';
        } else if($is_session_value[0]->tutor_gender == 'm') {
            $booking_success_data['tutor_gender'] = 'Male';
        }else{
            $booking_success_data['tutor_gender'] = 'Any';
        }


        $session_availability = DB::table('sessions')
            ->where('session_id', '=',$session_id)
            ->update(array(
                'date' => $session_date,
                'start_time' => $begin_time,
                'end_time'=>$session_duration+$begin_time,
                'tutor_id'=>$tutor_id,
                'is_completed'=>0,
                'updated_at'=> date('Y-m-d H:i:s'),
            ));

        if($session_duration > 1){


            $start_update_time = $begin_time;
            $end_update_time = $begin_time+1;
            for( $i=1; $i <= $session_duration; $i++) {

                DB::table('availability')
                    ->where('user_id', $tutor_id)
                    ->where('start_time', '=', $start_update_time)
                    ->where('end_time', '=', $end_update_time)
                    ->where('date', '=', $session_date)
                    ->update(array('updated_at' => date('Y-m-d H:i:s'),'is_booked'=>1));

                $start_update_time++;
                $end_update_time++;

            }

            //  echo 'done';

        }else{

            DB::table('availability')
                ->where('user_id', $tutor_id)
                ->where('start_time', '=', $begin_time)
                ->where('end_time', '=', $session_duration+$begin_time)
                ->where('date', '=', $session_date)
                ->update(array('updated_at' => date('Y-m-d H:i:s'),'is_booked'=>1));

            // echo 'done';
        }

        return view('tutors.tutor_booking_success', compact('booking_success_data'));


    }

    public function delete_tutor__by_tutor_id(Request $request){

        $tutor_id =  $request->input('tutor_id');

        $is_tutor = DB::table('tutors')
            ->where('user_id', '=', $tutor_id)
            ->first();

        if(count($is_tutor) > 0 ){

            $is_tutor_in_session =   DB::table('sessions')
                ->where('tutor_id', '=', $tutor_id)
                ->first();

            // var_dump(count($is_tutor_in_session));

            if(count($is_tutor_in_session) > 0 ){

                echo  $message = "This tutor has scheduled sessions for him";

            }else{

                $is_tutor_in_address =   DB::table('address')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_address) > 0 ){

                    $result = DB::table('address')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_bank_account =   DB::table('bank_accounts')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_bank_account) > 0 ){

                    $result = DB::table('bank_accounts')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_college =   DB::table('college')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_college) > 0 ){

                    $result = DB::table('college')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_highschool =   DB::table('highschool')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_highschool) > 0 ){

                    $result = DB::table('highschool')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_exam_answers =   DB::table('tutor_exam_answers')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_exam_answers) > 0 ){

                    $result = DB::table('tutor_exam_answers')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_exam_results =   DB::table('tutor_exam_results')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_exam_results) > 0 ){

                    $result = DB::table('tutor_exam_results')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_tutoring_subjects=   DB::table('tutoring_subjects')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_tutoring_subjects) > 0 ){

                    $result = DB::table('tutoring_subjects')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_certificates =   DB::table('certificates')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_certificates) > 0 ){

                    $result = DB::table('certificates')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                $is_tutor_in_tutors=   DB::table('tutors')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_tutors) > 0 ){

                    $result = DB::table('tutors')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }


                $is_tutor_in_users =   DB::table('users')
                    ->where('user_id', '=', $tutor_id)
                    ->first();

                if(count($is_tutor_in_users) > 0 ){

                    $result = DB::table('users')
                        ->where('user_id', '=', $tutor_id)
                        ->delete();

                }

                echo  $message = "Tutor deleted has been success";

            }



        }else{

        }
        // $user_id =  $request->input('user_id');

//        $query  = DB::table('tutoring_subjects')
//            ->where('subject_id', '=', $subject_id)
//            ->where('user_id', '=', $user_id)
//            ->delete();
    }

    public function to_excel(Request $request)
    {

        $tutors = DB::table('tutors')
            ->join('users', 'tutors.user_id', '=', 'users.user_id')
            ->join('address', 'tutors.user_id', '=', 'address.user_id');

        if($request->input('tutorNameEx') != '') {
            $tutors->where(DB::raw("CONCAT(users.first_name, ' ', users.last_name)"), "like" ,  '%'.$request->input('tutorNameEx').'%');
        }
        if($request->input('subjectNameEx') != ''){
            $tutors->where('subjects.subject_name', 'like',  '%'.$request->input('subjectNameEx').'%');
        }
        if($request->input('tutorTypeEx') != ''){
            $tutors->where('tutors.tutor_type','=',  $request->input('tutorTypeEx'));
        }
        if($request->input('statusEx') != ""){
            $tutors->where('tutors.is_approved','=',  $request->input('statusEx'));
        }
        if($request->input('locationEx') != ''){
            $tutors->where(DB::raw("CONCAT(address.city, ' ', address.street, ' ',address.state)"), 'like',  '%'.$request->input('locationEx').'%');
        }

        //Paginator
        $tutors->select('users.first_name','users.user_id','users.last_name', 'address.city','address.street','address.zip','address.state','tutors.is_approved','tutors.tutor_type','tutors.created_at','tutors.miles_travel','users.email','users.phone');

        $tutors = $tutors->get();

        $all_tuto_data = array();
        $i = 1;
        foreach($tutors as $tutor){
            $all_tuto_data[$i]['first_name'] = $tutor->first_name;
            $all_tuto_data[$i]['last_name'] = $tutor->last_name;
            $all_tuto_data[$i]['email'] = $tutor->email;
            $all_tuto_data[$i]['contact_number'] = $tutor->phone;

            $all_tuto_data[$i]['tutor_type'] = $tutor->tutor_type;
            $all_tuto_data[$i]['address'] = $tutor->street.' '.$tutor->city.' '.$tutor->state;
            $all_tuto_data[$i]['is_approved'] = $tutor->is_approved;
            $all_tuto_data[$i]['created_at'] = $tutor->created_at;
            $all_tuto_data[$i]['miles_travel'] = $tutor->miles_travel;

            $tutors_subjects = DB::table('subjects')
                ->join('tutoring_subjects', 'subjects.subject_id', '=', 'tutoring_subjects.subject_id')
                ->join('tutors', 'tutors.user_id', '=', 'tutoring_subjects.user_id')
                ->where('tutors.user_id', '=',$tutor->user_id)
                ->select('subjects.subject_name')
                ->get();

            $new_arr = array();
            foreach($tutors_subjects as $object)
            {
                $new_arr[] = $object->subject_name;
            }

            $all_tuto_data[$i]['subjects'] = implode(",", $new_arr);

            $i++;
        }

        if($request->input('statusEx') != ""){
            if($request->input('statusEx') == 0) {
                $excel_file_name = 'Pending tutors';
            } else if($request->input('statusEx') == 2) {
                $excel_file_name = 'Deactivated tutors';
            } else {
                $excel_file_name = 'All tutor list';
            }
        } else {
            $excel_file_name = 'All tutor list';
        }

        Excel::create($excel_file_name, function($excel) use($all_tuto_data)  {

            $excel->sheet('details list', function($sheet) use($all_tuto_data)  {

                $sheet->fromArray($all_tuto_data);

            });

        })->download('xls');

        return Redirect::back();

    }




    public function session_assign_info(Request $request){

        $session_id =    $request->input('session_assign_info_session_id');
        $session_date =    $request->input('session_assign_info_session_date');
        $session_duration =    $request->input('session_assign_info_session_duration');
        $begin_time =    $request->input('session_assign_info_begin_time');
        $end_time = $begin_time + $session_duration;
        $tutor_id =    $request->input('session_assign_info_tutor_id');

        $is_sessions = DB::table('sessions')
            ->where('session_id', '=', $session_id)
            ->first();


        if($is_sessions->student_id){
            $is_student_details = DB::table('users')
                ->where('user_id', '=', $is_sessions->student_id)
                ->first();
        }

        if($is_student_details->user_id){
            $is_student_address = DB::table('address')
                ->where('user_id', '=', $is_student_details->user_id)
                ->first();
        }

        $is_session_value =  DB::table('sessions')
            ->join('grades', 'sessions.grade_id', '=', 'grades.grade_id')
            ->join('subjects', 'sessions.subject_id', '=', 'subjects.subject_id')
            ->where('sessions.session_id', '=', $session_id)
            ->select('subjects.subject_name', 'grades.grade')
            ->get();

        if($tutor_id){
            $is_tutor_details = DB::table('users')
                ->where('user_id', '=', $tutor_id)
                ->first();
        }

        $session_assign_info = array();
        $session_assign_info['session_assign_info_passed_session_date'] = $session_date;
        $session_assign_info['session_assign_info_begin_time'] = $begin_time;
        $session_assign_info['session_assign_info_session_id'] = $session_id;
        $session_assign_info['session_assign_info_tutor_id'] = $tutor_id;
        $session_assign_info['session_assign_info_session_duration'] = $session_duration;
        $session_assign_info['session_assign_info_tutor_first_name'] = $is_tutor_details->first_name;
        $session_assign_info['session_assign_info_tutor_last_name'] = $is_tutor_details->last_name;
        $session_assign_info['session_assign_info_student_grade'] = $is_session_value[0]->grade;
        $session_assign_info['session_assign_info_student_subject_name'] = $is_session_value[0]->subject_name;
        $session_assign_info['session_assign_info_student_first_name'] = $is_student_details->first_name;
        $session_assign_info['session_assign_info_student_last_name'] = $is_student_details->last_name;
        $session_assign_info['session_assign_info_student_email'] = $is_student_details->email;
        $session_assign_info['session_assign_info_tutor_type'] = $is_sessions->tutor_type;
        $session_assign_info['session_assign_info_student_location'] = $is_student_address->street.",".$is_student_address->city.",".$is_student_address->state.",".$is_student_address->zip;

        if($begin_time > 12) {
            $session_assign_info['session_assign_info_session_start_time'] = ($begin_time - 12).':00 PM';
        } else if($begin_time == 12) {
            $session_assign_info['session_assign_info_session_start_time'] = ($begin_time).':00 PM';
        } else {
            $session_assign_info['session_assign_info_session_start_time'] = ($begin_time).':00 AM';
        }

        if($end_time > 12) {
            $session_assign_info['session_assign_info_session_end_time'] = ($end_time - 12).':00 PM';
        } else if($end_time == 12) {
            $session_assign_info['session_assign_info_session_end_time'] = ($end_time).':00 PM';
        } else {
            $session_assign_info['session_assign_info_session_end_time'] = ($end_time).':00 AM';
        }

        $session_assign_info['session_assign_info_session_date']= date('l , F d ,Y',strtotime($session_date));

        return view('tutors.session_assign_info', compact('session_assign_info'));


    }


}
