<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;
use DB;

class Student_account extends Model implements BillableContract
{
    use Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'student_account';


    protected $dates = ['trial_ends_at', 'subscription_ends_at'];


}
