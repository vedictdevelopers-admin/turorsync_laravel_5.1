<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;
use DB;



class Tutors extends Model implements BillableContract
{

    use Billable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tutors';


    //protected $dates = ['trial_ends_at', 'subscription_ends_at'];


    public function tutors(){
        return $this->hasMany('App\Users');
    }
    public static function getStudent($student_email)
    {
        $address = DB::table('users')
            ->where('users.email', '=', $student_email)
            ->join('address', 'address.user_id', '=', 'users.user_id')
            ->select('users.first_name','users.last_name','address.lat', 'address.lng','users.user_id','address.street','address.city','address.state','address.zip')
            ->first();

        return $address;
    }
    public static function getSuitableTutors($addr, $type, $gender, $subject, $location, $tutor_id)
    {
        $gender_sql ='';
        if($gender != '' && $gender != 'any_gender')
        {
            $gender_sql = "and t.gender='".$gender."'";
        }
        $location_sql = "";
        if($location != "") {
            $location_sql = " and CONCAT(a.state, ' ', a.city,' ',a.street) LIKE '%".$location."%' ";
        }
        $tutor_id_sql = "";
        if($tutor_id != "") {
            $tutor_id_sql = " and t.user_id=".$tutor_id;
        }
        if($type != '' && $type != 'ALL') {

            $type_sql = " and t.tutor_type ='".$type."'";
        } else {
            $type_sql = " ";
        }

      $sql= "SELECT t.user_id
                     FROM tutors t
                     JOIN users u on t.user_id = u.user_id
                     JOIN address a on t.user_id = a.user_id
                     JOIN tutoring_subjects ts on ts.user_id=t.user_id
                    WHERE ROUND((3959 * acos(cos(radians($addr->lat)) * cos(radians(a.lat)) * cos(radians(a.lng) - radians($addr->lng)) + sin(radians($addr->lat)) * sin(radians(a.lat)))),2) <= t.miles_travel
                      AND ts.is_pass=1
                      AND t.is_approved =1 ".$type_sql.$gender_sql."
                      AND ts.subject_id=".$subject.$location_sql.$tutor_id_sql;


        /* $sql="select user_id from (
                     select t.*,a.address_id, ts.subject_id,
                     distance(".$addr->lat.",".$addr->lng.",a.lat,a.lng,'MI') as distance
                     from tutors t, users u, address a, tutoring_subjects  ts
                     where t.user_id = u.user_id and u.user_id =a.user_id
                     and ts.user_id = u.user_id and ts.is_tested=1 and
                     t.is_approved =1 ".$type_sql.$gender_sql."
                     and ts.subject_id=".$subject.$location_sql.$tutor_name_sql." ) final_table where
                     distance <= miles_travel order by distance";*/


        $results = DB::select($sql);


        return $results;
    }

   /* public static function get_availability($duration, $session_date, $begin_time, $tutors)
    {
       $date_times_sql ="SELECT user_id,is_booked
                            FROM availability
                           WHERE user_id in (".implode(",",$tutors).") ";
        if($session_date != '') {
            $date_times_sql .= "AND date = '".$session_date."' ";
            if($duration != '' && $begin_time == '') {
                $begin_time = "8";
                $date_times_sql .= "AND (".$duration."<= (end_time-start_time) and start_time >=".$begin_time.") ";

            } else if($duration != '' && $begin_time != '') {
                $date_times_sql .= "AND (".$duration."<= (end_time-start_time) and start_time >=".$begin_time.")";
            }
        }

echo $date_times_sql;
        exit;
        $date_times = DB::select($date_times_sql);

        return $date_times;
    }
   */
    public static function get_tutor_name($tutor_id) {
        $tutor_name = DB::table('users')
            ->join('tutors', 'users.user_id', '=', 'tutors.user_id')
            ->where('users.user_id', '=', $tutor_id)
            ->select('users.user_id','users.first_name', 'users.last_name','tutors.gender','tutors.tutor_type','tutors.profile_image')
            ->first();
        return $tutor_name;

    }


public static function get_availability($duration,$session_date,$begin_time,$tutors)
{
    if($duration != '' && $session_date != '' && $begin_time != '' && !empty($tutors)) {
   $date_times_sql ="SELECT date,
                            start_time,
                            end_time,
                            is_booked,
                            user_id
                       FROM availability
                      WHERE user_id in (". implode(",",$tutors).")
                        AND date = '".$session_date."'
                        AND start_time >= ".$begin_time. "
                        AND end_time <= 21
                   ORDER by user_id,
                            date,
                            start_time,
                            end_time";

    $date_times = DB::select($date_times_sql);
    $date_times_avil = $date_times;
    if(!empty($date_times)) {
        $user_available_list = array();
        $user_booked_list = array();
        if(!empty($date_times)) {
            $available_count = 0;
            for ($x = 1; $x <= 11; $x++) {

                foreach($date_times_avil as $each_time_slot) {
                    if($available_count == 0)  {
                        if($each_time_slot->is_booked == 0) {
                            $available_count++;
                            $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                            $user_available_list[$available_count]['date'] = $each_time_slot->date;
                            $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                            $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                            $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                        }
                    } else {


                        if(($user_available_list[$available_count]['user_id'] == $each_time_slot->user_id) && ($user_available_list[$available_count]['end_time'] == $each_time_slot->start_time) && $each_time_slot->is_booked == 0) {


                            if($duration == ($user_available_list[$available_count]['end_time']-$user_available_list[$available_count]['start_time'])) {

                                $available_count++;
                                $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);

                            } else {
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $user_available_list[$available_count]['start_time']);


                            }

                        } else {
                            if($each_time_slot->is_booked == 0) {
                                $available_count++;
                                $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                            }
                        }
                    }
                }


                unset($date_times_avil[$x-1]);

                //$date_times_avil = array_shift($date_times_avil);
            }

            $date_times_blocked = $date_times;
            $not_available_count = 0;
            for ($x = 1; $x <= 11; $x++) {
            foreach($date_times_blocked as $each_time_slot) {
                if($not_available_count == 0)  {
                    if($each_time_slot->is_booked == 1) {
                        $not_available_count++;
                        $user_booked_list[$not_available_count]['user_id'] = $each_time_slot->user_id;
                        $user_booked_list[$not_available_count]['date'] = $each_time_slot->date;
                        $user_booked_list[$not_available_count]['start_time'] = $each_time_slot->start_time;
                        $user_booked_list[$not_available_count]['end_time'] = $each_time_slot->end_time;
                        $user_booked_list[$not_available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                    }
                } else {
                    if(($user_booked_list[$not_available_count]['user_id'] == $each_time_slot->user_id) && ($user_booked_list[$not_available_count]['end_time'] == $each_time_slot->start_time) && $each_time_slot->is_booked == 1) {
                        if($duration == ($user_booked_list[$not_available_count]['end_time']-$user_booked_list[$not_available_count]['start_time'])) {
                            $not_available_count++;
                            $user_booked_list[$not_available_count]['user_id'] = $each_time_slot->user_id;
                            $user_booked_list[$not_available_count]['date'] = $each_time_slot->date;
                            $user_booked_list[$not_available_count]['start_time'] = $each_time_slot->start_time;
                            $user_booked_list[$not_available_count]['end_time'] = $each_time_slot->end_time;
                            $user_booked_list[$not_available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);

                        } else {
                            $user_booked_list[$not_available_count]['end_time'] = $each_time_slot->end_time;
                            $user_booked_list[$not_available_count]['duration'] = ($each_time_slot->end_time - $user_booked_list[$not_available_count]['start_time']);
                            $not_available_count++;
                            $user_booked_list[$not_available_count]['user_id'] = $each_time_slot->user_id;
                            $user_booked_list[$not_available_count]['date'] = $each_time_slot->date;
                            $user_booked_list[$not_available_count]['start_time'] = $each_time_slot->start_time;
                            $user_booked_list[$not_available_count]['end_time'] = $each_time_slot->end_time;
                            $user_booked_list[$not_available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                        }

                    } else {
                        if($each_time_slot->is_booked == 1) {
                            $not_available_count++;
                            $user_booked_list[$not_available_count]['user_id'] = $each_time_slot->user_id;
                            $user_booked_list[$not_available_count]['date'] = $each_time_slot->date;
                            $user_booked_list[$not_available_count]['start_time'] = $each_time_slot->start_time;
                            $user_booked_list[$not_available_count]['end_time'] = $each_time_slot->end_time;
                            $user_booked_list[$not_available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                        }
                    }
                }
            }
                unset($date_times_blocked[$x-1]);
            }
        }
        $user_available_list = array_map("unserialize", array_unique(array_map("serialize", $user_available_list)));
        $user_booked_list = array_map("unserialize", array_unique(array_map("serialize", $user_booked_list)));
        $user_full_list = array();

        foreach($user_available_list as $id=>$each_tutor) {
                if($each_tutor['start_time'] != $begin_time) {
                    unset($user_available_list[$id]);
                } else if($duration >$each_tutor['duration']) {


                    unset($user_available_list[$id]);
                }


        }


            $user_full_list['available_list'] = $user_available_list;
            $user_full_list['booked_list'] = $user_booked_list;
    } else {
        $user_full_list = array();
        $user_full_list['available_list'] = "";
        $user_full_list['booked_list'] = "";
    }

    return $user_full_list;
    } else {
        return 'no_results';
    }
    }



    public static function get_calendar_availability($session_year,$session_month,$tutors,$duration)
    {
        $date_times_sql ="select date,
                            start_time,
                            end_time,
                            is_booked,
                            user_id from availability where
                user_id in (". implode(",",$tutors).")";


         $date_times_sql .=" AND YEAR(`date`) = '".$session_year."' AND MONTH(`date`) = '".$session_month."' AND is_booked=0 and `date` >='".date('Y-m-d')."' AND start_time >= 10 AND end_time <= 21";

        $date_times = DB::select($date_times_sql);

        if($duration != '') {
            $result_array = array();
        if(!empty($date_times)) {
            $user_available_list = array();

            if(!empty($date_times)) {
                $available_count = 0;
                for ($x = 1; $x <= 11; $x++) {
                    foreach($date_times as $each_time_slot) {
                        if($available_count == 0)  {
                            if($each_time_slot->is_booked == 0) {
                                $available_count++;
                                $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                            }
                        } else {
                            if(($user_available_list[$available_count]['user_id'] == $each_time_slot->user_id) && ($user_available_list[$available_count]['end_time'] == $each_time_slot->start_time) && $each_time_slot->is_booked == 0) {
                                if($duration == ($user_available_list[$available_count]['end_time']-$user_available_list[$available_count]['start_time'])) {
                                    $available_count++;
                                    $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                    $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                    $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                    $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                    $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);

                                } else {
                                    $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                    $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $user_available_list[$available_count]['start_time']);

                                }
                            } else {
                                if($each_time_slot->is_booked == 0) {
                                    $available_count++;
                                    $user_available_list[$available_count]['user_id'] = $each_time_slot->user_id;
                                    $user_available_list[$available_count]['date'] = $each_time_slot->date;
                                    $user_available_list[$available_count]['start_time'] = $each_time_slot->start_time;
                                    $user_available_list[$available_count]['end_time'] = $each_time_slot->end_time;
                                    $user_available_list[$available_count]['duration'] = ($each_time_slot->end_time - $each_time_slot->start_time);
                                }
                            }
                        }
                    }
                    unset($date_times[$x-1]);
                }

            }

            $user_available_list = array_map("unserialize", array_unique(array_map("serialize", $user_available_list)));


            foreach($user_available_list as $id=>$each_tutor) {
                if($duration >$each_tutor['duration']) {

                    unset($user_available_list[$id]);
                }
            }


            $k = 0;
            foreach($user_available_list as $each_tutor) {
                $result_array[$k] = $each_tutor['date'];
                $k++;
            }

        }
            return $result_array;
        } else {

            $k = 0;
            $result_array = array();
            foreach($date_times as $each_tutor) {
                $result_array[$k] = $each_tutor->date;
                $k++;
            }
            return $result_array;
        }

    }
    //need to complete






}

