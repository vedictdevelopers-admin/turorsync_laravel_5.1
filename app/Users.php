<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    public function users(){
        return $this->belongsTo('App\Tutors');
    }

}
